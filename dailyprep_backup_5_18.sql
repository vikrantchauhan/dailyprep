-- MySQL dump 10.13  Distrib 5.7.13, for Linux (x86_64)
--
-- Host: localhost    Database: dailyprep
-- ------------------------------------------------------
-- Server version	5.7.13-0ubuntu0.16.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `chapter_master`
--

DROP TABLE IF EXISTS `chapter_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chapter_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `abbreviation` varchar(100) DEFAULT NULL,
  `subject_id` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` varchar(100) DEFAULT NULL,
  `modified_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `id` (`id`),
  KEY `sub_id` (`subject_id`),
  CONSTRAINT `sub_id` FOREIGN KEY (`subject_id`) REFERENCES `subject_master` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chapter_master`
--

LOCK TABLES `chapter_master` WRITE;
/*!40000 ALTER TABLE `chapter_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `chapter_master` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER chapter_master_update_entry BEFORE UPDATE ON chapter_master FOR EACH ROW SET NEW.modify_date = CURRENT_TIMESTAMP */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `exam_chapter`
--

DROP TABLE IF EXISTS `exam_chapter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exam_chapter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_id` int(11) NOT NULL,
  `chapter_id` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` varchar(100) DEFAULT NULL,
  `modified_by` varchar(100) DEFAULT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `ex_ec_id` (`exam_id`),
  KEY `chap_ec_id` (`chapter_id`),
  CONSTRAINT `chap_ec_id` FOREIGN KEY (`chapter_id`) REFERENCES `chapter_master` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ex_ec_id` FOREIGN KEY (`exam_id`) REFERENCES `exam_master` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exam_chapter`
--

LOCK TABLES `exam_chapter` WRITE;
/*!40000 ALTER TABLE `exam_chapter` DISABLE KEYS */;
/*!40000 ALTER TABLE `exam_chapter` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER exam_chapter_update_entry BEFORE UPDATE ON exam_chapter FOR EACH ROW SET NEW.modify_date = CURRENT_TIMESTAMP */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `exam_master`
--

DROP TABLE IF EXISTS `exam_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exam_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `abbreviation` varchar(100) DEFAULT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` varchar(100) DEFAULT NULL,
  `modified_by` varchar(100) DEFAULT NULL,
  `color_code` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exam_master`
--

LOCK TABLES `exam_master` WRITE;
/*!40000 ALTER TABLE `exam_master` DISABLE KEYS */;
INSERT INTO `exam_master` VALUES (1,'GATE-CSE','GATE Computer Science and Engineering Exam','cse','2016-09-07 11:39:38','2016-09-07 11:43:30',NULL,NULL,'#fe8664'),(2,'GATE-ECE','GATE Electronics and Communication Engineering Exam','ece','2016-09-07 11:39:38','2016-09-07 11:43:42',NULL,NULL,'#56bdde'),(3,'GATE-EE','GATE Electrical Engineering Exam','ee','2016-09-07 11:40:33','2016-09-07 11:43:53',NULL,NULL,'#b198dc'),(4,'GATE-ME','GATE Mechanical Engineering Exam','me','2016-09-07 11:40:33','2016-09-07 11:44:04',NULL,NULL,'#6dc7be');
/*!40000 ALTER TABLE `exam_master` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER exam_master_update_entry BEFORE UPDATE ON exam_master FOR EACH ROW SET NEW.modify_date = CURRENT_TIMESTAMP */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `exam_subject`
--

DROP TABLE IF EXISTS `exam_subject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exam_subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` varchar(100) DEFAULT NULL,
  `modified_by` varchar(100) DEFAULT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `ex_id` (`exam_id`),
  KEY `sub_es_id` (`subject_id`),
  CONSTRAINT `ex_id` FOREIGN KEY (`exam_id`) REFERENCES `exam_master` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sub_es_id` FOREIGN KEY (`subject_id`) REFERENCES `subject_master` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exam_subject`
--

LOCK TABLES `exam_subject` WRITE;
/*!40000 ALTER TABLE `exam_subject` DISABLE KEYS */;
/*!40000 ALTER TABLE `exam_subject` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER exam_subject_update_entry BEFORE UPDATE ON exam_subject FOR EACH ROW SET NEW.modify_date = CURRENT_TIMESTAMP */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `exam_topic`
--

DROP TABLE IF EXISTS `exam_topic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exam_topic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` varchar(100) DEFAULT NULL,
  `modified_by` varchar(100) DEFAULT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `ex_et_id` (`exam_id`),
  KEY `top_et_id` (`topic_id`),
  CONSTRAINT `ex_et_id` FOREIGN KEY (`exam_id`) REFERENCES `exam_master` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `top_et_id` FOREIGN KEY (`topic_id`) REFERENCES `topic_master` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exam_topic`
--

LOCK TABLES `exam_topic` WRITE;
/*!40000 ALTER TABLE `exam_topic` DISABLE KEYS */;
/*!40000 ALTER TABLE `exam_topic` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER exam_topic_update_entry BEFORE UPDATE ON exam_topic FOR EACH ROW SET NEW.modify_date = CURRENT_TIMESTAMP */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `learning_outcome`
--

DROP TABLE IF EXISTS `learning_outcome`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_outcome` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` varchar(100) DEFAULT NULL,
  `modified_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_outcome`
--

LOCK TABLES `learning_outcome` WRITE;
/*!40000 ALTER TABLE `learning_outcome` DISABLE KEYS */;
INSERT INTO `learning_outcome` VALUES (4,'Analysis','2016-09-08 10:30:51','0000-00-00 00:00:00',NULL,NULL),(3,'Application','2016-09-08 10:30:51','0000-00-00 00:00:00',NULL,NULL),(2,'Comprehension','2016-09-08 10:30:51','0000-00-00 00:00:00',NULL,NULL),(5,'Evaluation','2016-09-08 10:30:51','0000-00-00 00:00:00',NULL,NULL),(1,'Knowledge','2016-09-08 10:30:51','0000-00-00 00:00:00',NULL,NULL),(6,'Synthesis','2016-09-08 10:30:51','0000-00-00 00:00:00',NULL,NULL);
/*!40000 ALTER TABLE `learning_outcome` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER learning_outcome_update_entry BEFORE UPDATE ON learning_outcome FOR EACH ROW SET NEW.modify_date = CURRENT_TIMESTAMP */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `logs`
--

DROP TABLE IF EXISTS `logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_email` varchar(255) DEFAULT NULL,
  `platform` varchar(255) DEFAULT NULL,
  `device_info` text,
  `api_request` text,
  `api_response` text,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` varchar(100) DEFAULT NULL,
  `modified_by` varchar(100) DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logs`
--

LOCK TABLES `logs` WRITE;
/*!40000 ALTER TABLE `logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `logs` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER logs_update_entry BEFORE UPDATE ON logs FOR EACH ROW SET NEW.modify_date = CURRENT_TIMESTAMP */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `newsletter`
--

DROP TABLE IF EXISTS `newsletter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter` (
  `news_id` int(11) NOT NULL AUTO_INCREMENT,
  `news_email` varchar(100) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`news_email`),
  UNIQUE KEY `news_id` (`news_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter`
--

LOCK TABLES `newsletter` WRITE;
/*!40000 ALTER TABLE `newsletter` DISABLE KEYS */;
INSERT INTO `newsletter` VALUES (2,'','2016-09-06 10:04:55'),(1,'vikrant.chauhan@numetriclabz.com','2016-09-01 13:10:38');
/*!40000 ALTER TABLE `newsletter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subject_master`
--

DROP TABLE IF EXISTS `subject_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subject_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `abbreviation` varchar(100) DEFAULT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` varchar(100) DEFAULT NULL,
  `modified_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subject_master`
--

LOCK TABLES `subject_master` WRITE;
/*!40000 ALTER TABLE `subject_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `subject_master` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER subject_master_update_entry BEFORE UPDATE ON subject_master FOR EACH ROW SET NEW.modify_date = CURRENT_TIMESTAMP */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `test_info`
--

DROP TABLE IF EXISTS `test_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `test_id` int(11) NOT NULL,
  `question` text,
  `answer_choices` text,
  `correct_answer` varchar(255) DEFAULT NULL,
  `explanation` text,
  `lo_id` int(11) DEFAULT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` varchar(100) DEFAULT NULL,
  `modified_by` varchar(100) DEFAULT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `lo_ti_id` (`lo_id`),
  CONSTRAINT `lo_ti_id` FOREIGN KEY (`lo_id`) REFERENCES `learning_outcome` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test_info`
--

LOCK TABLES `test_info` WRITE;
/*!40000 ALTER TABLE `test_info` DISABLE KEYS */;
INSERT INTO `test_info` VALUES (1,1,'Which of the following problems are decidable?<br/>1) Does a given program ever produce an output? <br/> 2) If L is context-free language, then, is L also context-free?<br/>  3) If L is regular language, then, is L also regular? <br/> 4) If L is recursive language, then, is L also recursive? ','[\"1,2,3,4\", \"1,2\", \"2,3,4\", \"3,4\"]','3','CFL’s are not closed under complementation. Regular and recursive languages are closed under complementation. ',NULL,'2016-09-12 04:24:39','0000-00-00 00:00:00',NULL,NULL),(2,1,'Given the language L-{ab, aa, baa}, which of the following strings are in L*? <br/> 1) abaabaaabaa <br/>  2) aaaabaaaa <br/>  3) baaaaabaaaab <br/>  4) baaaaabaa','[\"1,2 and 3 \", \"2,3 and 4 \", \"1,2 and 4 \", \"1,3 and 4\"]','2','L ={ab, aa, baa } <br/> Let S1 = ab , S2 = aa and S3 =baa <br/> abaabaaabaa can be written as S1S2S3S1S2 <br/> aaaabaaaa can be written as S1S1S3S1 <br/> baaaaabaa can be written as S3S2S1S2 ',NULL,'2016-09-12 04:29:42','0000-00-00 00:00:00',NULL,NULL),(3,1,'In the IPv4 addressing format, the number of networks allowed under Class C addresses is ','[\"2<sup>14 </sup> \", \" 2<sup>7</sup> \", \"2<sup>21</sup> \", \"2<sup>24</sup>\"]','2','For class C address, size of network field is 24 bits. But first 3 bits are fixed as 110. <br/>Hence total number of networks possible is 2<sup>21</sup>.',NULL,'2016-09-12 04:33:58','2016-09-12 04:36:27',NULL,NULL),(4,1,'Which of the following transport layer protocols is used to support electronic mail? ','[\"SMTP \", \" IP \", \" TCP \", \" UDP\"]','2','                    E-mail uses SMTP, application layer protocol which intern uses TCP transport layer protocol.   \r\n          \r\n        ',NULL,'2016-09-12 04:40:41','2016-09-12 09:35:37',NULL,NULL),(5,1,'Consider a random variable X that takes values + 1 and -1 with probability 0.5 each. The values of the cumulative distribution function F(x) at x = -1 and +1 are ','[\" 0 and 0.5 \", \" 0 and 1  \", \" 0.5 and 1 \", \" 0.25 and 0.75 \"]','2','The cumulative distribution function F( x ) = P( X <= x) <br/>F (-1)= P (X <=-1) P (X =-1 ) = 0.5 <br/>F(+1) = P(X<=+1) =P(X=-1)+P(X=-1) = 0.5 + 0.5 = 1',NULL,'2016-09-12 04:50:14','0000-00-00 00:00:00',NULL,NULL),(6,1,'Register renaming is done is pipelined processors ','[\" as an alternative to register allocation at compile time \", \" for efficient access to function parameters and local variables  \", \"to handle certain kinds of hazards  \", \"  as part of address translation \"]','2','Register renaming is done to eliminate WAR/WAW hazards. ',NULL,'2016-09-12 04:51:51','0000-00-00 00:00:00',NULL,NULL),(7,1,'The amount of ROM needed to implement a 4 bit multiplier is ','[\"64 bits\", \" 128 bits \", \"1 Kbits\", \"  2 Kbits\"]','3','For a 4 bit multiplier there are  256 combinations.  Output will contain 8 bits.  So the amount of ROM needed is 2^8 * 8 bits = 2Kbits.',NULL,'2016-09-12 04:54:53','0000-00-00 00:00:00',NULL,NULL),(9,1,'Let W(n) and A(n) denote respectively, the worst case and average case running time of an algorithm executed on an input of size n. Which of the following is ALWAYS TRUE? ','[\"A(n)=&#x3A9;(W(n)) \", \" A(n)=&#x398;(W(n)) \", \"A(n)=O(W(n)) \", \"A(n)=o(W(n)) \"]','2','The average case time can be lesser than or even equal to the worst case. So A(n) would be\r\nupper bounded by W(n) and it will not be strict upper bound as it can even be same (e.g.\r\nBubble Sort and merge sort).<br/>? A (n) = O (W( n)).',NULL,'2016-09-12 05:34:30','0000-00-00 00:00:00',NULL,NULL),(10,1,'Let G be a simple undirected planar graph on 10 vertices with 15edges. If G is a connected graph, then the number of bounded faces in any embedding of G on the plane is equal to ','[\"3 \", \" 4\", \"5\", \"6\"]','3','We have the relation V-E+F=2, by this we will get the total number of faces, F = 7. Out of 7 faces one is an unbounded face, so total 6 bounded faces. ',NULL,'2016-09-12 05:39:07','0000-00-00 00:00:00',NULL,NULL),(11,1,'The recurrence relation capturing the optimal execution time of the Towers of Hanoi problem with n discs is ','[\"T(n)= 2T( n -2)+ 2 \", \"T(n)= 2T( n -1)+ n\", \"T(n)= 2T( n /2)+ 1\", \"T(n)= 2T( n -1)+ 1\"]','3','Let the three pegs be A,B and C, the goal is to move n pegs from A to C using peg B <br/>\r\n The following sequence of steps are executed recursively<br/>\r\n 1.move n?1 discs from A to B. This leaves disc n alone on peg A --- T(n-1)<br/>\r\n 2.move disc n from A to C---------1<br/>\r\n 3.move n?1 discs from B to C so they sit on disc n----- T(n-1)<br/>\r\n So, T(n) = 2T(n-1) +1 ',NULL,'2016-09-12 05:45:25','0000-00-00 00:00:00',NULL,NULL),(12,1,'Which of the following statements are TRUE about an SQL query?<br/>\r\n P : An SQL query can contain a HAVING clause even if it does not have a\r\n GROUP BY clause<br/>\r\n Q : An SQL query can contain a HAVING clause only if it has GROUP BY\r\n clause<br/>\r\n R : All attributes used in the GROUP BY clause must appear in the SELECT\r\n clause<br/>\r\n S : Not all attributes used in the GROUP BY clause need to appear in the\r\n SELECT clause ','[\" P and R  \", \"  P and S \", \"Q and R  \", \"Q and S \"]','1','If we use a HAVING clause without a GROUP BY clause, the HAVING condition applies to\r\nall rows that satisfy the search condition. In other words, all rows that satisfy the search\r\ncondition make up a single group.<br/> So, option P is true and Q is false. \r\n        ',NULL,'2016-09-12 05:51:39','0000-00-00 00:00:00',NULL,NULL),(13,1,'Given the basic ER and relational models, which of the following is INCORRECT?','[\" An attribute of an entity can have more than one value \", \" An attribute of an entity can be composite \", \"In a row of a relational table, an attribute can have more than one value \", \"In a row of a relational table, an attribute can have exactly one value or a NULL value  \"]','2','The term ‘entity’ belongs to ER model and the term ‘relational table’ belongs to relational\r\nmodel.<br/>\r\n Options A and B both are true since ER model supports both multivalued and composite\r\nattributes.<br/>\r\n As multivalued attributes are not allowed in relational databases, in a row of a relational\r\n(table), an attribute cannot have more than one value.\r\n        ',NULL,'2016-09-12 05:53:50','0000-00-00 00:00:00',NULL,NULL),(16,1,'What is the correct translation of the following statement into mathematical logic? <br/>\r\n“Some real numbers are rational” ','[\"?x (real (x) v rational (x)) \", \"?x (real (x) ? rational (x))\",\"?x (real (x) ? rational (x))\",\"?x (real (x) ? rational (x))\"]','2',' Option A: There exists x which is either real or rational and can be both.<br/>\r\n Option B: All real numbers are rational.<br/>\r\n Option C: There exists a real number which is rational.<br/>\r\n Option D: There exists some number which is not rational or which is real. \r\n        ',NULL,'2016-09-12 06:11:29','0000-00-00 00:00:00',NULL,NULL),(17,1,'Let A be the 2 x 2 matrix with elements a<sub>11</sub> = a<sub>12</sub> = a<sub>21</sub>= + 1 and a<sub>22</sub> =-1. Then the eigen values\r\nof the matrix A<sup>19</sup> are ','[\"1024 and -1024 \", \"1024 &radic; 2 and -1024 &radic; 2 \",\"4 &radic 2 and -4 &radic 2 \",\"512 &radic;  2 and -512 &radic;  2 \"]','3','Characteristic equation of A is |A I | 0 where is the eigen value <br/>\r\n<table class=\"matrix\">\r\n        <tr>\r\n            <td>1-?</td>\r\n            <td>1</td>\r\n            \r\n        </tr>\r\n        <tr>\r\n            <td>1</td>\r\n            <td>-1-?</td>\r\n        </tr>\r\n    </table>\r\n        =0 ? ?<sup>2</sup> ?2 = 0? ? <sup>2</sup>=±&radic; 2 <br/>\r\nEvery matrix satisfies its characteristic equation Therefore  A<sup>2</sup> - 2I = 0 ? A<sup>2</sup>= 2I .<br/>\r\nA<sup>19</sup> = A<sup>18</sup>X A =( A<sup>2</sup>)<sup>9</sup> X A = ( 2I)<sup>9</sup> X A = 512 X A <br/>\r\nHence eigen values of A<sup>19</sup> are ±512 &radic;  2.',NULL,'2016-09-12 06:34:43','0000-00-00 00:00:00',NULL,NULL),(18,1,'Consider a random variable X that takes values + 1 and -1 with probability 0.5 each. The values of the cumulative distribution function F(x) at x = -1 and +1 are  12','[\" 0 and 0.5 \", \" 0 and 1  \", \" 0.5 and 1 \", \" 0.25 and 0.75 \"]','2','          The cumulative distribution function F( x ) = P( X <= x) <br/>F (-1)= P (X <=-1) P (X =-1 ) = 0.5 <br/>F(+1) = P(X<=+1) =P(X=-1)+P(X=-1) = 0.5 + 0.5 = 1  \r\n        ',NULL,'2016-09-12 07:28:51','0000-00-00 00:00:00',NULL,NULL),(20,1,'The protocol data unit (PDU) for the application layer in the Internet stack is ',' [\"Segment\",\"Datagram\",\"Message\",\"Frame \"]','2','The PDU for Datalink layer, Network layer , Transport layer and Application layer are frame,\r\ndatagram, segment and message respectively.\r\n        ',NULL,'2016-09-12 07:37:18','0000-00-00 00:00:00',NULL,NULL),(21,1,'Consider the function f(x) = sin(x) in the interval x &#x2208;[&#x220F;/ 4,7 &#x220F;/ 4 ]. The number and location (s) of the local minima of this function are ',' [\"One, at &#x220F;/ 2\",\"One, at 3 &#x220F;/ 2\",\"Two, at &#x220F; / 2 and 3&#x220F; / 2 \",\"Two, at &#x220F;/ 4and3 &#x220F;/ 2 \"]','1','Sin x has a maximum value of 1 at &#x220F; / 2  and a minimum value of –1 at 3 &#x220F; / 2  and at all angles conterminal with them.\r\n Therefore,it has one local minimum at 3&#x220F; / 2. \r\n\r\n        ',NULL,'2016-09-12 07:44:56','0000-00-00 00:00:00',NULL,NULL),(22,1,'A process executes the code<br/> fork ();<br/>fork ();<br/>fork ();<br/>\r\n The total number of child processes created is ',' [\"3\",\"4\",\"7\",\"8\"]','2','If fork is called n times, there will be total 2<sup>n</sup>running processes including the parent process. <br/>\r\nSo, there will be 2<sup>n</sup>-1 child processes. \r\n\r\n        ',NULL,'2016-09-12 07:47:06','0000-00-00 00:00:00',NULL,NULL),(23,1,'The decimal value 0.5 in IEEE single precision floating point representation has ',' [\" fraction bits of 000…000 and exponent value of 0 \",\" fraction bits of 000…000 and exponent value of -1 \",\"fraction bits of 100…000 and exponent value of 0 \",\"no exact representation \"]','1','(0.5)<sub>10</sub>= (1.0)<sub>2</sub> X 2 <sup>-1</sup>.\r\n So, exponent = -1 and fraction is 000 - - - 000 \r\n\r\n        ',NULL,'2016-09-12 07:51:34','0000-00-00 00:00:00',NULL,NULL),(24,1,'Consider the set of strings on {0,1} in which, every substring of 3 symbols has at most two\r\nzeros. For example, 001110 and 011001 are in the language, but 100010 is not. All strings of length less than 3 are also in the language. A partially completed DFA that accepts this\r\nlanguage is shown below. <br/>\r\n<img src=\"http://mydailyprep.com/question-images/2012-26.png\" /><br/>\r\nThe missing arcs in the DFA are : <br/>\r\n<img src=\"http://mydailyprep.com/question-images/2012-26-1.png\" />',' [\" X \",\"X + Y \",\"X ? Y \",\"Y\"]','0','          XY\' + XY =  X (Y\'+ Y) = X\r\n\r\n          \r\n        ',NULL,'2016-09-12 07:57:36','2016-09-12 10:25:37',NULL,NULL),(25,1,'The worst case running time to search for an element in a balanced binary search tree with\r\nn2<sup>n </sup> elements is :',' [\"&#x398;(n log n)  \",\"&#x398;(n 2<sup> n</sup>) \",\"&#x398;(n)  \",\"&#x398;(log n)\"]','2','The worst case search time in a balanced BST on ‘x’ nodes is logx. So, if x =n2<sup>2</sup>\r\n, then log(n2<sup>n</sup>) = log n + log(2<sup>n</sup>) = log n + n = &#x398;(n) \r\n\r\n        ',NULL,'2016-09-12 08:09:44','0000-00-00 00:00:00',NULL,NULL),(26,1,'Assuming P &#x2260; NP,  which of the following is TRUE? ',' [\" NP-complete = NP\",\" NP-complete &#x2229; = &#x3C6;\",\" NP-hard = NP\",\"P = NP-complete \"]','1','If P!=NP, then it implies that no NP-Complete problem can be solved in polynomialtime\r\nwhich implies that the set P and the set NPC are disjoint. \r\n\r\n        ',NULL,'2016-09-12 08:13:32','0000-00-00 00:00:00',NULL,NULL),(28,1,'Which of the following is TRUE?','[\" Every relation is 3NF is also in BCNF \",\"A relation R is in 3NF if every non-prime attribute of R is fully functionally dependent on\r\nevery key of R \",\"Every relation in BCNF is also in 3NF \",\"No relation can be in both BCNF and 3NF \"]','2','Option A is false since BCNF is stricter than 3NF (it needs LHS of all FDs should be candidate\r\nkey for 3NF condition)\r\n Option B is false since the definition given here is of 2NF\r\n Option C is true, since for a relation to be in BCNF it needs to be in 3NF, every relation in\r\nBCNF satisfies all the properties of 3NF.\r\n Option D is false, since if a relation is in BCNF it will always be in 3NF.             \r\n        ',NULL,'2016-09-12 08:28:53','0000-00-00 00:00:00',NULL,NULL),(29,1,'Consider the following logical inferences. <br/>\r\nI1 : If it rains then the cricket match will not be played.\r\n The cricket match was played.<br/>\r\n Inference: There was no rain. <br/>\r\nI2 : If it rains then the cricket match will not be played.\r\n It did not rain.<br/>\r\n Inference: The cricket match was played. <br/>\r\nWhich of the following is TRUE?','[\"Both I1 and I2 are correct inferences\",\r\n \"I1 is correct but I2 is not a correct inference\",\r\n \"I1 is not correct but I2 is a correct inference\",\r\n \"Both I1 and I2 are not correct inferences\"] ','1','\r\n            \r\n        ',NULL,'2016-09-12 09:57:28','0000-00-00 00:00:00',NULL,NULL),(30,1,'Consider the set of strings on {0,1} in which, every substring of 3 symbols has at most two\r\nzeros. For example, 001110 and 011001 are in the language, but 100010 is not. All strings of length less than 3 are also in the language. A partially completed DFA that accepts this\r\nlanguage is shown below. <br/>\r\n<img src=\"http://mydailyprep.com/question-images/2012-26.png\" /><br/>\r\nThe missing arcs in the DFA are : <br/>\r\n<img src=\"http://mydailyprep.com/question-images/2012-26-1.png\" />\r\n','[\"A\",\"B\",\"C\",\"D\"]','3','            \r\n        ',NULL,'2016-09-12 10:06:13','0000-00-00 00:00:00',NULL,NULL),(31,1,'Consider an instance of TCP’s Additive Increase Multiplicative decrease (AIMD) algorithm\r\nwhere the window size at the start of the slow start phase is 2 MSS and the threshold at the\r\nstart of the first transmission is 8 MSS. Assume that a timeout occurs during the fifth\r\ntransmission. Find the congestion window size at the end of the tenth transmission.  ','[\" 8MSS\",\" 14MSS\",\" 7MSS\",\" 12MSS\"]','2','          Given, initial threshold = 8 <br/>\r\n Time = 1, during 1st transmission,Congestion window size = 2 (slow start phase)<br/>\r\n Time = 2, congestion window size = 4 (double the no. of acknowledgments)<br/>\r\n Time = 3, congestion window size = 8 (Threshold meet)<br/>\r\n Time = 4, congestion window size = 9, after threshold (increase by one Additive increase)<br/>\r\n Time = 5, transmits 10 MSS, but time out occurs congestion window size = 10      <br/>\r\nHence threshold = (Congestion window size) /2= 10/2 =  5       <br/>\r\nTime = 6, transmits 2 <br/>\r\n Time = 7, transmits 4 <br/>\r\n Time = 8, transmits 5 (threshold is 5) <br/>\r\n Time = 9, transmits 6, after threshold (increase by one Additive increase) <br/>\r\n Time = 10, transmits 7 <br/>\r\nDuring 10th transmission, it transmits 7 segments hence at the end of the tenth transmission the size of congestion window is 7 MSS. \r\n          \r\n        ',NULL,'2016-09-12 11:15:39','2016-09-12 11:17:22',NULL,NULL),(32,1,'Consider a source computer (S) transmitting a file of size 106\r\n bits to a destination computer\r\n(D) over a network of two routers (R1 and R2) and three links (L1, L2, and L3). L1 connects S\r\nto R1; L2 connects R1 to R2; and L3 connects R2 to D. Let each link be of length 100km.\r\nAssume signals travel over each line at a speed of 108\r\n meters per second. Assume that the\r\nlink bandwidth on each link is 1Mbps. Let the file be broken down into 1000 packets each of\r\nsize 1000 bits. Find the total sum of transmission and propagation delays in transmitting the\r\nfile from S to D? ','[\"1005ms\",\"1010ms\",\"3000ms \",\"3003ms\"]','0','Transmission delay for 1 packet from each of S, R1 and R2 will take 1ms <br/>\r\n Propagation delay on each link L1, L2 and L3 for one packet is 1ms <br/>\r\n Therefore the sum of transmission delay and propagation delay on each link for one packet is 2ms.<br/>\r\n The first packet reaches the destination at 6<sub>th</sub>ms <br/>\r\n The second packet reaches the destination at 7<sub>th</sub>ms <br/>\r\n So inductively we can say that 1000th packet reaches the destination at 1005<sub>th</sub> ms   <br/>          \r\n        ',NULL,'2016-09-12 11:28:20','0000-00-00 00:00:00',NULL,NULL);
/*!40000 ALTER TABLE `test_info` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER test_info_update_entry BEFORE UPDATE ON test_info FOR EACH ROW SET NEW.modify_date = CURRENT_TIMESTAMP */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `test_types`
--

DROP TABLE IF EXISTS `test_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` varchar(100) DEFAULT NULL,
  `modified_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test_types`
--

LOCK TABLES `test_types` WRITE;
/*!40000 ALTER TABLE `test_types` DISABLE KEYS */;
INSERT INTO `test_types` VALUES (2,'Mock Test','2016-09-08 10:25:40','0000-00-00 00:00:00',NULL,NULL),(3,'Practice Test','2016-09-08 10:25:40','0000-00-00 00:00:00',NULL,NULL),(1,'previous','2016-09-08 10:25:40','2016-09-08 13:06:59',NULL,NULL);
/*!40000 ALTER TABLE `test_types` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER test_types_update_entry BEFORE UPDATE ON test_types FOR EACH ROW SET NEW.modify_date = CURRENT_TIMESTAMP */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `tests`
--

DROP TABLE IF EXISTS `tests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `total_count` int(11) DEFAULT NULL,
  `type_id` int(11) NOT NULL,
  `exam_level` varchar(100) DEFAULT NULL,
  `exam_level_id` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` varchar(100) DEFAULT NULL,
  `modified_by` varchar(100) DEFAULT NULL,
  `instructions` text,
  PRIMARY KEY (`name`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tests`
--

LOCK TABLES `tests` WRITE;
/*!40000 ALTER TABLE `tests` DISABLE KEYS */;
INSERT INTO `tests` VALUES (1,'GATE CSE-2012 Paper',20,1,'exam',1,'2016-09-08 10:50:45','2016-09-12 11:47:16',NULL,NULL,'1. asdjfhadjsf asdkfj lasdfj lasdfjlas dfkhfjaskhf dsjf asdjhf lasdfaf asdfkjhas dlflasdjf hladskjfhladfjshladfjs h.<br>1. asdjfhadjsf asdkfj lasdfj lasdfjlas dfkhfjaskhf dsjf asdjhf lasdfaf asdfkjhas dlflasdjf hladskjfhladfjshladfjs h.<br>1. asdjfhadjsf asdkfj lasdfj lasdfjlas dfkhfjaskhf dsjf asdjhf lasdfaf asdfkjhas dlflasdjf hladskjfhladfjshladfjs h.<br>1. asdjfhadjsf asdkfj lasdfj lasdfjlas dfkhfjaskhf dsjf asdjhf lasdfaf asdfkjhas dlflasdjf hladskjfhladfjshladfjs h.<br>1. asdjfhadjsf asdkfj lasdfj lasdfjlas dfkhfjaskhf dsjf asdjhf lasdfaf asdfkjhas dlflasdjf hladskjfhladfjshladfjs h.<br>1. asdjfhadjsf asdkfj lasdfj lasdfjlas dfkhfjaskhf dsjf asdjhf lasdfaf asdfkjhas dlflasdjf hladskjfhladfjshladfjs h.'),(2,'GATE CSE-2013 Paper',20,1,'exam',1,'2016-09-08 10:50:55','2016-09-12 11:47:16',NULL,NULL,'1. asdjfhadjsf asdkfj lasdfj lasdfjlas dfkhfjaskhf dsjf asdjhf lasdfaf asdfkjhas dlflasdjf hladskjfhladfjshladfjs h.<br>1. asdjfhadjsf asdkfj lasdfj lasdfjlas dfkhfjaskhf dsjf asdjhf lasdfaf asdfkjhas dlflasdjf hladskjfhladfjshladfjs h.<br>1. asdjfhadjsf asdkfj lasdfj lasdfjlas dfkhfjaskhf dsjf asdjhf lasdfaf asdfkjhas dlflasdjf hladskjfhladfjshladfjs h.<br>1. asdjfhadjsf asdkfj lasdfj lasdfjlas dfkhfjaskhf dsjf asdjhf lasdfaf asdfkjhas dlflasdjf hladskjfhladfjshladfjs h.<br>1. asdjfhadjsf asdkfj lasdfj lasdfjlas dfkhfjaskhf dsjf asdjhf lasdfaf asdfkjhas dlflasdjf hladskjfhladfjshladfjs h.<br>1. asdjfhadjsf asdkfj lasdfj lasdfjlas dfkhfjaskhf dsjf asdjhf lasdfaf asdfkjhas dlflasdjf hladskjfhladfjshladfjs h.');
/*!40000 ALTER TABLE `tests` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER test_update_entry BEFORE UPDATE ON tests FOR EACH ROW SET NEW.modify_date = CURRENT_TIMESTAMP */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `topic_master`
--

DROP TABLE IF EXISTS `topic_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `topic_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `abbreviation` varchar(100) DEFAULT NULL,
  `chapter_id` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` varchar(100) DEFAULT NULL,
  `modified_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `id` (`id`),
  KEY `chap_id` (`chapter_id`),
  CONSTRAINT `chap_id` FOREIGN KEY (`chapter_id`) REFERENCES `chapter_master` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `topic_master`
--

LOCK TABLES `topic_master` WRITE;
/*!40000 ALTER TABLE `topic_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `topic_master` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER topic_master_update_entry BEFORE UPDATE ON topic_master FOR EACH ROW SET NEW.modify_date = CURRENT_TIMESTAMP */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(10) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `salt` varchar(250) DEFAULT NULL,
  `saltid` varchar(250) DEFAULT NULL,
  `start_time` varchar(250) DEFAULT NULL,
  `reg_source` varchar(100) DEFAULT NULL,
  `reg_platform` varchar(100) DEFAULT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` varchar(100) DEFAULT NULL,
  `modified_by` varchar(100) DEFAULT NULL,
  `exam_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`email`),
  UNIQUE KEY `user_id` (`id`),
  UNIQUE KEY `phone` (`phone`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (3,'dinesh','dinesh.dogra@numetriclabz.com','1232232323','efe6398127928f1b2e9ef3207fb82663','0','514779957d3f3964d9925.13094712','a532eb1cb2014a192dd73d39fcb5ffed','1473508246',NULL,NULL,'2016-09-10 11:50:46','2016-09-12 11:40:37',NULL,NULL,1),(1,'Udit','udit@numetriclabz.com','1231231230','d8578edf8458ce06fbc5bb76a58c5ca4','1','250219857cff96a7e4b92.44728558','4b049aee7740349a3b245c4c13fdc7c3','1473247594',NULL,NULL,'2016-09-07 11:26:34','2016-09-12 11:40:37',NULL,NULL,1),(2,'pankaj','vikrant.chauhan@numetriclabz.com','1231231231','d8578edf8458ce06fbc5bb76a58c5ca4','1','384543957d24930017488.46148080','bc8ed85d07ce3f3af80106c23f9949d3','1473399088',NULL,NULL,'2016-09-09 05:31:28','2016-09-12 11:40:37',NULL,NULL,1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER user_update_entry BEFORE UPDATE ON user FOR EACH ROW SET NEW.modify_date = CURRENT_TIMESTAMP */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `user_test_details`
--

DROP TABLE IF EXISTS `user_test_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_test_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `utr_id` int(11) NOT NULL,
  `state` varchar(255) DEFAULT NULL,
  `time_taken` int(11) DEFAULT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` varchar(100) DEFAULT NULL,
  `modified_by` varchar(100) DEFAULT NULL,
  `question_id` int(11) DEFAULT NULL,
  `answer_id` int(11) DEFAULT NULL,
  `is_Correct` tinyint(1) DEFAULT NULL,
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `utr_ques` (`utr_id`,`question_id`),
  CONSTRAINT `utr_utd_id` FOREIGN KEY (`utr_id`) REFERENCES `user_test_report` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_test_details`
--

LOCK TABLES `user_test_details` WRITE;
/*!40000 ALTER TABLE `user_test_details` DISABLE KEYS */;
INSERT INTO `user_test_details` VALUES (1,1,NULL,NULL,'2016-09-12 10:05:26','2016-09-12 10:06:44',NULL,NULL,1,1,1),(4,1,NULL,NULL,'2016-09-12 10:07:11','0000-00-00 00:00:00',NULL,NULL,2,2,0),(5,1,NULL,NULL,'2016-09-12 10:07:22','0000-00-00 00:00:00',NULL,NULL,4,1,1);
/*!40000 ALTER TABLE `user_test_details` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER utd_update_entry BEFORE UPDATE ON user_test_details FOR EACH ROW SET NEW.modify_date = CURRENT_TIMESTAMP */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `user_test_report`
--

DROP TABLE IF EXISTS `user_test_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_test_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `test_id` int(11) NOT NULL,
  `test_status` varchar(100) DEFAULT NULL,
  `ques_attempted` int(11) DEFAULT NULL,
  `time_taken` int(11) DEFAULT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` varchar(100) DEFAULT NULL,
  `modified_by` varchar(100) DEFAULT NULL,
  `total_count` int(11) DEFAULT NULL,
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `composite_test_user` (`user_id`,`test_id`),
  KEY `test_utr_id` (`test_id`),
  CONSTRAINT `test_utr_id` FOREIGN KEY (`test_id`) REFERENCES `tests` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_utr_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=286 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_test_report`
--

LOCK TABLES `user_test_report` WRITE;
/*!40000 ALTER TABLE `user_test_report` DISABLE KEYS */;
INSERT INTO `user_test_report` VALUES (1,1,1,'completed',7,0,'2016-09-12 09:01:01','2016-09-12 11:47:33',NULL,NULL,NULL);
/*!40000 ALTER TABLE `user_test_report` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER utr_update_entry BEFORE UPDATE ON user_test_report FOR EACH ROW SET NEW.modify_date = CURRENT_TIMESTAMP */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-09-12 17:18:38
