<?php

class Esport_model extends CI_Model{
	
	public function __construct(){
		
		parent::__construct();
	}
	
    function insertUser($data){
    
        return $this->db->insert('Users', $data);
    }
    
    function check_user($email, $password){
    
        $sql = "select userID,status,name from Users where email = ? AND password = ?";
        $data = $this->db->query($sql, array($email,$password));
        return ($data->result_array()) ;
    }
    
        function confirm_link($saltid){
    	
        $sql = "select userID, email, start_time from Users where salt_id = ?";
	$data = $this->db->query($sql, array($saltid));
        return $data->result_array() ;
    }
    
    function update_time($saltid){
        $sql = "UPDATE Users SET status = 1 where salt_id = '$saltid';";
        $data = $this->db->query($sql);
    }
    
    function updateConfirmationTime($id, $start_time){
    	
    	$sql = "update Users set start_time = ? where userID = ?";
    	$data = $this->db->query($sql, array($start_time, $id));
    	$this->db->affected_rows();
    	
    	$sql = "select email, salt_id from Users where userID = $id";
    	$val = $this->db->query($sql);
    	$email = $val->result_array();
    	return $email;
    }
}
