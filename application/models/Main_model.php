<?php
class Main_model extends CI_Model {

	public function __construct(){     
		parent::__construct();
	}
	
	function checkSubscriber($email){
        //return $this->db->insert('newsletter', $email);

        $sql = "select news_id from newsletter where news_email = ?";
        $query = $this->db->query($sql , array($email));
        // return $query->result_array() ;
        return $this->db->affected_rows();
    }

    function newsemail($email){
        $this->db->insert('newsletter', $email);
    }
        
    function saveQuestion($data){
        $this->db->insert('test_info', $data);   
    }
    
    function getQuestion($id){
        $sql = "select id,question,answer_choices,correct_answer,explanation from test_info where test_id = ?";
        $data = $this->db->query($sql, array($id));
        return $data->result_array() ;
    }
        
    function editQuestion($question,$op1,$answer,$explain,$id){
        $sql = "update test_info set question = ?, answer_choices = ? , correct_answer = ?,explanation = ? where id = ?";
        $data = $this->db->query($sql,array($question,$op1,$answer,$explain,$id));
    }

    function contact($data){
        $this->db->insert('contact', $data); 
         return $this->db->affected_rows();

    }
}
