<?php

class User_model extends CI_Model{
	
	public function __construct(){
		
		parent::__construct();
	}
	
	//insert into user table
    function insertUser($data){
    
        return $this->db->insert('user', $data);
    }
    
    //login details
    function check_user($email, $password){
    
        $sql = "select a.status, 
                       a.name, 
                       a.id,
                      
                       b.abbreviation 
                       from user as a
                left join
                exam_master as b
                on a.exam_id = b.id
        		where email = ? AND password = ?";
		$data = $this->db->query($sql, array($email,$password));
        return ($data->result_array()) ;
    }
    
    function confirm_link($saltid){
    	
        $sql = "select id, email, start_time from user where saltid = ?";
		$data = $this->db->query($sql, array($saltid));
        return $data->result_array() ;
    }
    
    
    function socialuser($data){
        $this->db->insert('user', $data);
        //return $this->db->insert_id();
        return $this->ifAlreadyExists($data['email']);
    }

    function ifAlreadyExists($user_email){
        $sql = "select a.id, a.email, a.exam_id, b.abbreviation from user as a left join exam_master as b on a.exam_id = b.id where email = ?";
        $query = $this->db->query($sql, array($user_email));
        return $query->result_array();
    }

    function updateExamID($exam_id, $user_id){
        $sql = 'update user set exam_id = ? where id = ?';
        $data = $this->db->query($sql, array($exam_id, $user_id));
    }

    function examData($exam_id){
        $sql = 'select id, abbreviation from exam_master where id = ?';
        $query = $this->db->query($sql, array($exam_id));
        return $query->result_array();
    }
    
    function updateConfirmationTime($id, $start_time){
    	
    	$sql = "update user set start_time = ? where id = ?";
    	$data = $this->db->query($sql, array($start_time, $id));
    	$this->db->affected_rows();
    	
    	$sql = "select email, saltid from user where id = $id";
    	$val = $this->db->query($sql);
    	$email = $val->result_array();
    	return $email;
    }
    
    /**description* collects user information needed at dahsboard
                    and returns it too controller.
      *@param Int (User Id)
      *@return Array (User Information)
      */
	function get_user_info($id){

            $sql = "select a.id as user_id,
                           a.name as user_name, 
                           a.email as user_email, 
                           b.id as exam_id,
                           b.abbreviation as exam
                    from user as a 
                    left join 
                    exam_master as b 
                    on a.exam_id = b.id 
                    where a.id = ?;";

            $query = $this->db->query($sql, $id);
            return $query->result_array();
    }

	function get_previous_paper($test_type, $exam_level, $exam_id ){
    	$user_id = $this->session->userdata('userId');
        
        $sql = "select a.id,
                       a.name ,
                       a.total_count
                    from tests as a 
                    left join 
                    test_types as b 
                    on a.type_id = b.id 
                    where b.name=? and exam_level = ? and exam_level_id = ?";
        $query = $this->db->query($sql, array($test_type, $exam_level, $exam_id));
        $results = $query->result_array();
        foreach($results as $index => $result) {
        	$results[$index]['test_info'] = $this->check_if_test_attempted($result['id'],$user_id);
        }
        return $results;
    }
    
	function get_previous_paper_by_status($test_type, $exam_level, $exam_id, $status ){
    	$user_id = $this->session->userdata('userId');
        
        $sql = "SELECT a.id, a.name, a.total_count,c.name as exam_category
				FROM tests AS a
				LEFT JOIN test_types AS b ON a.type_id = b.id
				LEFT JOIN exam_master AS c ON c.id = a.exam_level_id
				LEFT JOIN user_test_report AS utr ON utr.test_id = a.id
				WHERE b.name =  ?
				AND a.exam_level =  ?
				AND a.exam_level_id =  ?
				AND utr.user_id = ?
				AND utr.test_status =  ?";
        
        $query = $this->db->query($sql, array($test_type, $exam_level, $exam_id,$user_id,$status));
        $results = $query->result_array();
        $results = $query->result_array();
        foreach($results as $index => $result) {
        	 $test_info = $this->check_if_test_attempted($result['id'],$user_id);
        	 $report = $this->getMarks($user_id, $result['id']);
        	 $results[$index]['test_info'] =  array_merge($test_info,$report);
        }
        return $results;
    }
    
	function get_previous_paper_not_attempted($test_type, $exam_level, $exam_id){
    	$user_id = $this->session->userdata('userId');
        
        $sql = "SELECT a.id, a.name, a.total_count,c.name as exam_category
				FROM tests AS a
				LEFT JOIN test_types AS b ON a.type_id = b.id
				LEFT JOIN exam_master AS c ON c.id = a.exam_level_id
				WHERE b.name =  ?
				AND a.exam_level =  ?
				AND a.exam_level_id =  ?
				AND a.id NOT 
				IN (
					SELECT test_id
					FROM user_test_report
					WHERE user_id =?
				)";
        
        $query = $this->db->query($sql, array($test_type, $exam_level, $exam_id,$user_id));
        $results = $query->result_array();
        return $results;
    }
    
    function check_if_test_attempted($test_id,$user_id) {
    	$sql = "SELECT `id` as attempt_id, `user_id`, `test_id`, `current_question_id`, `test_status`, `ques_attempted`, `time_taken`, `create_date`, `created_by`, `modified_by` FROM `user_test_report` 
    	WHERE user_id=? and test_id=?";
    	$query = $this->db->query($sql, array($user_id,$test_id));
    	return $query->result_array();
    }
    
	function getMarks($user_id, $test_id) {
    	$result = $this->get_user_report($user_id, $test_id);
    	$reportArray = array();
    	if($result) {
    		$reportArray['pos_marks'] = $result[0]['correct'] * 1;
    		$reportArray['neg_marks'] = $result[0]['wrong'] * 0.33;
    		$reportArray['total_marks'] = $result[0]['total_count'];
    		$reportArray['obtained_marks'] = $reportArray['pos_marks'] - $reportArray['neg_marks'];
    		$reportArray['attempted'] = $result[0]['attempted'];
    		$reportArray['correct'] = $result[0]['correct'];
    		$reportArray['incorrect'] = $result[0]['wrong'];
    		$reportArray['skipped'] = $result[0]['attempted'] - ($result[0]['wrong'] + $result[0]['correct']);
    		$reportArray['tques'] = $result[0]['total_count'];
    	}
    	return $reportArray;
    }
    
	public function get_user_report($user_id, $test_id){
		$sql = "select a.total_count, 
					   b.user_id, 
					   b.test_id,
					   b.test_status, 
					   b.attempted, 
					   b.correct, 
					   b.wrong 
				from tests as a 
				left join (select a.test_id as test_id, 
								  a.user_id, 
								  a.test_status as test_status,
								  count(b.id) as attempted, 
								  sum(b.is_Correct =  1) as correct, 
								  sum(b.is_Correct = 0) as wrong 
							from user_test_report as a 
							left join user_test_details as b 
							on a.id = b.utr_id 
							where a.test_id = $test_id and a.user_id = $user_id) as b 
				on a.id = b.test_id 
				where a.id = $test_id;";
		//echo $sql;die();
		$query = $this->db->query($sql);
		return $query->result_array();
	}

    function get_paper_info($id){
        $sql = "select id as test_id, name as test_name, total_count,total_count as q_count, instructions from tests where id = ?";
        $query = $this->db->query($sql, array($id));
        return $query->result_array();
    }
    
    function getTestDetails($test_id,$user_id) {
		$sql = "select 
				tests.id as test_id, 
				tests.name as test_name, 
				tests.total_count,
				tests.total_count as q_count, 
				tests.instructions,
				(tests.total_time_duration - user_test_report.time_taken) AS remaining_time,				
				user_test_report.id as utr_id,
				user_test_report.user_id,
				user_test_report.test_status,
				user_test_report.ques_attempted,
				user_test_report.time_taken,
				user_test_report.current_question_id
				from tests 
				Left join user_test_report on user_test_report.test_id = tests.id 
				where tests.id = ? 
				and user_test_report.user_id=?";    	
    	$query = $this->db->query($sql, array($test_id,$user_id));
    	$result = $query->result_array();
    	if($result && $result[0]['current_question_id'] > 0) {
	    	$offset = $this->getOffset($result[0]['current_question_id'],$result[0]['test_id']);
	    	if($offset >= 0 ) {
	    		$result[0]['current_question_offset'] = $offset-1;
	    	}
    	} else if ($result && $result[0]['current_question_id'] == 0){
    		$result[0]['current_question_offset'] = 0;
    	}
    	$result[0]['question_paper'] = $this->getAllQuestions($test_id);
    	return $result;
    }
    
    public function getAllQuestions($test_id) {
    	$sql = "SELECT test_info.id as question_id, 
		    	test_info.test_id as test_id, 
		    	test_info.question as question, 
		    	test_info.answer_choices as choices, 
		    	test_info.correct_answer as correct_answer,
		    	test_info.explanation as explanation
		    	FROM test_info 
		    	WHERE test_info.test_id = ?";
    	$query = $this->db->query($sql, array($test_id));
    	$result = $query->result_array();
        return $result;
    }
    
    public function getOffset($question_id,$test_id) {
    	$sql = "SELECT `id`,
		       (SELECT COUNT(*) FROM `test_info` WHERE `id` <= '$question_id' and `test_id`='$test_id') AS `position`
				FROM `test_info`
				WHERE `id` = '$question_id'
				and `test_id`='$test_id'";
    	$query = $this->db->query($sql);
    	if($query->num_rows() > 0) {
    		return $query->row()->position;
    	} else {
    		return 1;
    	}
    }
    
	public function start_test($user_id, $test_id){

  		$sql = "insert into user_test_report (user_id, 
                                                    test_id, 
                                                    test_status, 
                                                    ques_attempted, 
                                                    time_taken)
  				values ($user_id, $test_id, 'started', 0, 0) 
  				ON DUPLICATE KEY UPDATE test_status='resumed';";
  		$query = $this->db->query($sql);
  		
                $idSql = "select id from user_test_report where user_id = $user_id and test_id=$test_id";
                $data = $this->db->query($idSql);
                $array = $data->result_array();
                return $array[0]['id'];
	}
    
    /* +++++++++++++++++++ forget password ++++++++++++++++++++++++++++++++*/
      function mail_exists($key)
    {

        $this->db->where('email',$key);
        $query = $this->db->get('user');
        
       if ($query->num_rows() == 0){           
           return FALSE;
       }
       else{
           
        return TRUE;
       }
    }
    
    function getId($email){
        $sql = "select id from user where email = ?";
	$data = $this->db->query($sql, array($email));
        $a = $data->result_array() ;
        $id = $a[0]['id'];
        return $id;
    }
    
    function getEmail($saltid){
        $sql = "select id from user where saltid = ?";
	$data = $this->db->query($sql, array($saltid));
        if ($data->num_rows() == 0){           
            return false;
       }
       else{
        $a = $data->result_array() ;
        $id = $a[0]['id'];
        return $id;
    }
    }
    function update_password($password,$id){
        $sql = "update user set password = ? where id = ?";
    	$data = $this->db->query($sql, array($password, $id));
    }
    
    function updateTime($id, $start_time,$saltid){
    	
    	$sql = "update user set start_time = ?, saltid = ? where id = ?";
    	$data = $this->db->query($sql, array($start_time,$saltid, $id));
        $sql = "select email, saltid from user where id = $id";
    	$val = $this->db->query($sql);
    	$email = $val->result_array();
    	return $email;

    }
}
