<?php

class Api_model extends CI_Model{
	
	public function __construct(){
		
		parent::__construct();
	}
	
	public function getExams(){
		
		$sql = "select id, name, full_name, abbreviation, color_code from exam_master";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_question($test_id, $offset){

		$sql = "select id, 
					   question, 
					   answer_choices, 
					   correct_answer, 
					   explanation, 
					   lo_id
				from test_info 
				where test_id = $test_id 
				order by id 
				limit 1 
				offset $offset; ";

		$query = $this->db->query($sql);
		return $query->result_array();
	}

	/*public function get_total($test_id){
		$sql = "select total_count from tests where id = $test_id";
		$query = $this->db->query($sql);
		$data = $query->result_array();
		return $data[0]['total_count'];
	}*/
	
	public function get_total($test_id){
		$sql = "select count(*) as total_count from test_info where test_id = $test_id";
		$query = $this->db->query($sql);
		$data = $query->result_array();
		return $data[0]['total_count'];
	}

	public function start_test($user_id, $test_id){

  		$sql = "insert into user_test_report (user_id, 
                                                    test_id, 
                                                    test_status, 
                                                    ques_attempted, 
                                                    time_taken)
  				values ($user_id, $test_id, 'started', 0, 0) 
  				ON DUPLICATE KEY UPDATE test_status='resumed';";
  		$query = $this->db->query($sql);
  		
                $idSql = "select id from user_test_report where user_id = $user_id and test_id=$test_id";
                $data = $this->db->query($idSql);
                $array = $data->result_array();
                return $array[0]['id'];
	}

	public function submit_answer($user_id, $test_id, $question_id, $answer, $isCorrect, $utr_id,$time_taken){

		$sql = "update user_test_report set test_status = 'resumed', ques_attempted = ques_attempted+1,time_taken= time_taken+$time_taken  where test_id = $test_id and user_id = $user_id";
		$sqlDetails = "insert into user_test_details (utr_id, question_id, answer_id, is_Correct) values ($utr_id, $question_id, $answer, $isCorrect) on duplicate key update is_Correct = $isCorrect , answer_id = $answer";

		$this->db->trans_begin();

		$this->db->query($sql);
		$this->db->query($sqlDetails);
		$last_insert_id = $this->db->insert_id();
		
		$sqlUpdate = "Update user_test_details set time_taken= time_taken+$time_taken WHERE id=$last_insert_id";
	    $this->db->query($sqlUpdate);

		if ($this->db->trans_status() === FALSE){
		    $this->db->trans_rollback();
			return false;
		}
		else{
	        $this->db->trans_commit();
	        return true;
		} 
	}
	
	public function chekIfAnswered($question_id, $utr_id) {
		$sql = "SELECT time_taken as time_taken, answer_id as markedOption,is_Correct as isCorrect, is_marked as isMarked  FROM user_test_details WHERE utr_id=? and question_id=?";
		$query = $this->db->query($sql, array($utr_id,$question_id));
		return $query->result_array();
	}
	
	/*function insert_null_response($question_id,$utr_id) {
		$sqlDetails = "insert into user_test_details (utr_id, question_id, answer_id, is_Correct) values ($utr_id, $question_id, NULL, NULL) on duplicate key update is_Correct = NULL , answer_id = NULL ";
		$this->db->query($sqlDetails);
	}
	
	function insert_bookmarked_response($question_id,$utr_id) {
		$sqlDetails = "insert into user_test_details (utr_id, question_id, answer_id, is_Correct, is_marked) values ($utr_id, $question_id, NULL, NULL,1) on duplicate key update is_Correct = NULL , answer_id = NULL,is_marked = 1";
		$this->db->query($sqlDetails);
	}*/
	
	function insert_null_response($question_id,$utr_id,$timeTaken,$answer_id=null,$is_Correct=null) {
		if($answer_id && $is_Correct) {
			$sqlDetails = "insert into user_test_details (utr_id, question_id, answer_id, is_Correct) values ($utr_id, $question_id, $answer_id, $is_Correct) on duplicate key update is_Correct = $is_Correct, answer_id = $answer_id";
		} else {
			$sqlDetails = "insert into user_test_details (utr_id, question_id) values ($utr_id, $question_id) on duplicate key update utr_id = $utr_id, question_id = $question_id";
		}
		$this->db->query($sqlDetails);
		$last_insert_id = $this->db->insert_id();
		$sqlUpdate = "Update user_test_details set time_taken= time_taken+$timeTaken WHERE id=$last_insert_id";
	    $this->db->query($sqlUpdate);
	    
	    $sqlUtrUpdate = "update user_test_report set time_taken= time_taken+$timeTaken  where id = $utr_id";
		$this->db->query($sqlUtrUpdate);
	}
	
	function insert_bookmarked_response($question_id,$data,$timeTaken) {
		//print_r($data);
		$utr_id = $data['utr_id'];
		if(!isset($data['answer_id'])) {
			$sqlDetails = "insert into user_test_details (utr_id, question_id, is_marked) 
							values ($utr_id, $question_id,1) 
							on duplicate key update utr_id = $utr_id , question_id = $question_id,is_marked = 1";
		} else {
			$answer_id = $data['is_Correct'];
			$is_Correct = $data['is_Correct'];
			$sqlDetails = "insert into user_test_details (utr_id,answer_id, is_Correct, question_id, is_marked) 
							values ($utr_id,$answer_id, $is_Correct, $question_id,1) on
							duplicate key update utr_id = $utr_id , answer_id= $answer_id,is_Correct = $is_Correct, question_id = $question_id,is_marked = 1";
		}
		$this->db->query($sqlDetails);
		$last_insert_id = $this->db->insert_id();
		$sqlUpdate = "Update user_test_details set time_taken= time_taken+$timeTaken WHERE id=$last_insert_id";
	    $this->db->query($sqlUpdate);
	    $sqlUtrUpdate = "update user_test_report set time_taken= time_taken+$timeTaken  where id = $utr_id";
		$this->db->query($sqlUtrUpdate);
	}
	
	function getPreviousResponses($utr_id,$test_id,$user_id) {
		/*$sql = "SELECT ti.id as question_id,
				ti.test_id as test_id,
				ti.question as question, 
				ti.answer_choices as answer_choices,
				ti.correct_answer as correct_answer,
				ti.explanation as explanation,
				utr.id as utr_id,
				utr.user_id as user_id,
				utd.question_id as attempted,
				utd.is_Correct as is_Correct,
				utd.answer_id as markedOption,
				utd.is_marked as isMarked,
				utd.time_taken as time
				FROM test_info as ti 
				LEFT JOIN user_test_report as utr ON utr.test_id = ti.test_id
				LEFT JOIN user_test_details as utd ON utd.question_id = ti.id 
				WHERE utr.id = ?
				AND utr.test_id = ? 
				AND utr.user_id = ?";*/
		
		
		$sql =' SELECT ti.id AS question_id, 
				ti.test_id AS test_id, 
				ti.question AS question, 
				ti.answer_choices AS answer_choices, 
				ti.correct_answer AS correct_answer, 
				ti.explanation AS explanation, 
				utr.id AS utr_id, utr.user_id AS user_id, 
				utd.question_id AS attempted, 
				utd.is_correct AS is_Correct, 
				utd.answer_id AS markedOption, 
				utd.is_marked AS isMarked, 
				utd.time_taken AS TIME
				FROM test_info AS ti
				LEFT JOIN user_test_report AS utr ON utr.test_id = ti.test_id
				LEFT JOIN user_test_details AS utd ON utd.utr_id = utr.id AND utd.question_id = ti.id
				WHERE utr.id = ?
				AND utr.test_id = ?
				AND utr.user_id = ?';
				
				
		$query = $this->db->query($sql, array($utr_id,$test_id,$user_id));
		$results = $query->result_array();
		if($results) {
			foreach($results as $index => $result) {
				if($result['attempted'] > 0) {
					$results[$index]['attempted'] = 'Yes';
				}
			}
		}
		return $results;
	}
	
	function getMrkedCount($utr_id,$test_id,$user_id) {
		$sql = "SELECT ti.id as markedCount
				FROM test_info as ti 
				LEFT JOIN user_test_report as utr ON utr.test_id = ti.test_id
				LEFT JOIN user_test_details AS utd ON utd.utr_id = utr.id AND utd.question_id = ti.id
				WHERE utr.id = ?
				AND utr.test_id = ? 
				AND utr.user_id = ?
				AND utd.is_marked = 1";
		$query = $this->db->query($sql, array($utr_id,$test_id,$user_id));
		return $query->num_rows();	
	}
	
	function getAttemptedCount($utr_id,$test_id,$user_id) {
		$sql = "SELECT ti.id as attemptedCount
				FROM test_info as ti 
				LEFT JOIN user_test_report as utr ON utr.test_id = ti.test_id
				LEFT JOIN user_test_details AS utd ON utd.utr_id = utr.id AND utd.question_id = ti.id 
				WHERE utr.id = ?
				AND utr.test_id = ? 
				AND utr.user_id = ?
				AND utd.is_Correct is not null 
				AND utd.question_id is not null";
		$query = $this->db->query($sql, array($utr_id,$test_id,$user_id));
		return $query->num_rows();	
	}
	
	function getSkippedCount($utr_id,$test_id,$user_id) {
		$sql = "SELECT ti.id as attemptedCount
				FROM test_info as ti 
				LEFT JOIN user_test_report as utr ON utr.test_id = ti.test_id
				LEFT JOIN user_test_details AS utd ON utd.utr_id = utr.id AND utd.question_id = ti.id
				WHERE utr.id = ?
				AND utr.test_id = ? 
				AND utr.user_id = ?
				AND utd.is_Correct is null 
				AND utd.question_id is not null
				AND utd.is_marked = 0";
		$query = $this->db->query($sql, array($utr_id,$test_id,$user_id));
		return $query->num_rows();	
	}
	
	function getNotVisitedCount($utr_id,$test_id,$user_id) {
		$sql = "SELECT ti.id as attemptedCount
				FROM test_info as ti 
				LEFT JOIN user_test_report as utr ON utr.test_id = ti.test_id
				LEFT JOIN user_test_details AS utd ON utd.utr_id = utr.id AND utd.question_id = ti.id
				WHERE utr.id = ?
				AND utr.test_id = ? 
				AND utr.user_id = ?
				AND utd.is_Correct is null 
				AND utd.question_id is null";
		$query = $this->db->query($sql, array($utr_id,$test_id,$user_id));
		return $query->num_rows();	
	}
	
	
	function getMarkedAndAnsweredCount($utr_id,$test_id,$user_id) {
		$sql = "SELECT ti.id as attemptedCount
				FROM test_info as ti 
				LEFT JOIN user_test_report as utr ON utr.test_id = ti.test_id
				LEFT JOIN user_test_details AS utd ON utd.utr_id = utr.id AND utd.question_id = ti.id
				WHERE utr.id = ?
				AND utr.test_id = ? 
				AND utr.user_id = ?
				AND utd.is_Correct is not null 
				AND utd.question_id is not null
				AND utd.is_marked = 1";
		$query = $this->db->query($sql, array($utr_id,$test_id,$user_id));
		return $query->num_rows();	
	}
	
	public function update_current_question($utr_id,$user_id,$test_id,$question_id) {
		$sql = "UPDATE `user_test_report` SET `current_question_id`=? WHERE `user_id`=? and `test_id`=? and `id`=?";
		$query = $this->db->query($sql, array($question_id,$user_id,$test_id,$utr_id));
		//$this->output->enable_profiler(TRUE);
	}
	
	public function submit_paper($user_id, $test_id){

		$sql = "update user_test_report set test_status ='completed' where test_id = $test_id and user_id = $user_id";
		$query = $this->db->query($sql);
		return $this->db->affected_rows();
	}

	public function get_user_report($user_id, $test_id){
		$sql = "select a.total_count, 
					   b.user_id, 
					   b.test_id,
					   b.test_status, 
					   b.attempted, 
					   b.correct, 
					   b.wrong 
				from tests as a 
				left join (select a.test_id as test_id, 
								  a.user_id, 
								  a.test_status as test_status,
								  count(b.id) as attempted, 
								  sum(b.is_Correct =  1) as correct, 
								  sum(b.is_Correct = 0) as wrong 
							from user_test_report as a 
							left join user_test_details as b 
							on a.id = b.utr_id 
							where a.test_id = $test_id and a.user_id = $user_id) as b 
				on a.id = b.test_id 
				where a.id = $test_id;";
		//echo $sql;die();
		$query = $this->db->query($sql);
		return $query->result_array();
	}
}