<?php defined('BASEPATH') or exit("No direct scripts allowed");


class Analytics extends CI_Controller{

	public function __Construct(){
		
		parent::__Construct();
        $this->load->database();
        $this->load->dbutil();
        $this->load->helper('url');
        $this->load->helper('csv');
        $this->load->helper('download');
	}
	
	public function createmail(){
		
		$this->updateUser();
		$this->updateNewsletter();
		$this->daywiseuser();
		$attempted = $this->totalQuestionAttempted();
		$val = $attempted[0]['count'];
		
		$text = "<h2>Total Questions Attempted : $val</h2>";
		$this->sendEmail('sj@numetriclabz.com', $text, 'udit@numetriclabz.com', 'dev@numetriclabz.com');
	}
	
	public function updateUser(){
		
		$sql = "select name, email, reg_source as source, status, date(create_date) as date from user order by id";
    	$query = $this->db->query($sql);

		$delimiter = ",";
		$newline = "\r\n";
		$data =  $this->dbutil->csv_from_result($query, $delimiter, $newline);
		
		$file = fopen(FCPATH.'user.csv', "w");
		echo fwrite($file, $data);
		fclose($file);
	}
	
	public function updateNewsletter(){
	
		$sql = "select news_email as email, date(create_date) as date from newsletter order by news_id";
		$query = $this->db->query($sql);

		$delimiter = ",";
		$newline = "\r\n";
		$data =  $this->dbutil->csv_from_result($query, $delimiter, $newline);
		
		$file = fopen(FCPATH.'newsletter.csv', "w");
		echo fwrite($file, $data);
		fclose($file);
	}
	
	public function daywiseuser(){
	
		$sql = "select date(create_date) as date, count(id) as '# of users' from user group by date(create_date) order by date(create_date);";
		$query = $this->db->query($sql);

		$delimiter = ",";
		$newline = "\r\n";
		$data =  $this->dbutil->csv_from_result($query, $delimiter, $newline);
		
		$file = fopen(FCPATH.'day-wise.csv', "w");
		echo fwrite($file, $data);
		fclose($file);
	}
	
	public function totalQuestionAttempted(){
		
		$sql = "select count(*) as count from user_test_details;";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	
	/** @desc send email to activate user account.
	  * @param salt id and email of client
      */
    //mailing function
    function sendEmail($email, $text, $cc, $bcc){

    	$this->load->library('email');
 		$this->email->from('admin@dailyprep', 'DailyPrep');
		$this->email->to($email);	
		$this->email->cc($cc);	
		$this->email->bcc($bcc);
		$this->email->subject('DailyPrep Statistics');
		$this->email->attach(FCPATH.'user.csv');
		$this->email->attach(FCPATH.'newsletter.csv');
		$this->email->attach(FCPATH.'day-wise.csv');
		$message = "<html><head><head></head><body>$text</body></html>";
		$this->email->message($message);
		return $this->email->send();
    }
    
    /*
	select a.id, a.user_id, b.email, b.name, a.test_id, date(a.create_date) as date from user_test_report as a left join user as b on a.user_id = b.id order by a.create_date;

	select b.user_id, b.email, a.utr_id, sum(a.attempted) as attempted from (select count(id) as attempted, utr_id from user_test_details group by utr_id order by id) as a left join (select a.id, a.user_id, b.email, a.test_id, date(a.create_date) as date from user_test_report as a left join user as b on a.user_id = b.id order by a.create_date) as b on a.utr_id = b.id group by b.user_id order by a.utr_id;
	*/
}

?>
