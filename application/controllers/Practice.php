<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Practice extends CI_Controller{

	public function __Construct(){
		parent::__Construct();
		$this->load->helper(array('form', 'url'));
		$this->load->library(array('session', 'form_validation', 'email')); 
		$this->load->model('user_model');
	}
	
	public function stream($param){
	
		if($this->checkSession()){

			$user_id = $this->session->userdata('userId');
			
			$info = $this->user_model->get_user_info($user_id);
			$data['user_info'] = $info[0];
			$data['page'] = "practice";
			
			$this->load->view("student/header", $data);
			$this->load->view("student/practice");
			$this->load->view("student/footer");
		}
		else{
			redirect(base_url().'user/login', 'refresh');   
		}
	}


	public function previousYear($exam, $id = null){
		if($this->checkSession()){
			$user_id = $this->session->userdata('userId');
			$info = $this->user_model->get_user_info($user_id);
			$data['user_info'] = $info[0];
			$data['page'] = "previous";
			$exam_id = $info[0]['exam_id'];
			if($id != null){
				$data['test'] = $this->user_model->get_paper_info($id);
				$this->load->view("student/header-exam", $data);
				$this->load->view("student/test", $data);
				$this->load->view("student/footer");
			}
			else{

				//$data['paper'] = $this->user_model->get_previous_paper("previous", "exam", $exam_id );
				$data['completed'] = $this->user_model->get_previous_paper_by_status("previous", "exam", $exam_id, "completed" );
				$data['resumable'] = $this->user_model->get_previous_paper_by_status("previous", "exam", $exam_id, "resumed" );
				$started = $this->user_model->get_previous_paper_by_status("previous", "exam", $exam_id, "started" );
				$remaining = $this->user_model->get_previous_paper_not_attempted("previous", "exam", $exam_id);
			    $data['remaining'] = array_merge($remaining,$started);
				//echo "<pre>",print_r($data);die();
				$this->load->view("student/header-exam", $data);
				$this->load->view("student/previous_year", $data);
				$this->load->view("student/footer");
			}
		}
		else{
			redirect(base_url().'user/login', 'refresh');   
		}
	}
	
	function gettestestinstructions() {
		$json = array();
		if($_GET && $this->session->userdata('userId')) {
			$test_id = $this->input->get('test_id');
			$user_id = $this->session->userdata('userId');
			$test_instructions = $this->user_model->get_paper_info($test_id);
			$test_instructions[0]['user_id'] = $user_id;
			$json['success'] = 'true';
			$json['data'] = $test_instructions;
			echo json_encode($json);
		} else {
			$json['success'] = 'false';
			$json['message'] = 'invalid request';
			echo json_encode($json);
		}
	}
	
	function gettestdetails() {
		if($_POST) {
			$test_id = $this->input->post('test_id');
			if($this->input->post('user_id')) {
				$user_id = $this->input->post('user_id');
			} else {
				$user_id = $this->session->userdata('userId');
			}
			$test_details = $this->user_model->getTestDetails($test_id,$user_id);
			$user_info = $this->user_model->get_user_info($user_id);
			//print_r($test_details);die();
			if(isset($test_details[0]['utr_id'])) {
				
				$json['success'] = 'true';
				$json['data'] = $test_details;
				$json['user_details'] = $user_info[0];
				echo json_encode($json);
			} else {
				$utr = $this->user_model->start_test($user_id, $test_id);
				if($utr) {
					$test_details = $this->user_model->getTestDetails($test_id,$user_id);
					$json['success'] = 'true';
					$json['data'] = $test_details;
					$json['user_details'] = $user_info[0];
					echo json_encode($json);
				}
			}
		} else {
			$json['success'] = 'false';
			$json['message'] = 'invalid request';
			echo json_encode($json);
		}
	}
	
	/**
     *  @desc : This function will check if user is logged in or not
     *  @return : redirects to login page
     */
    function checkSession() {
    
    	$session = $this->session->all_userdata();

        if(!(array_key_exists('loggedIn', $session) && $session['loggedIn']))  {
            return false;
        }
        else{
			$last=$this->session->userdata('lastActivity');
			$sess_out=$this->session->userdata('sess_expiration');
			
			if($sess_out + $last < $this->input->server("REQUEST_TIME")){
				
				$this->session->sess_destroy();
				return false;
			}
			else{
				
				$this->session->set_userdata('lastActivity', $this->input->server("REQUEST_TIME"));
				return true;
			}
        }
    }
}
