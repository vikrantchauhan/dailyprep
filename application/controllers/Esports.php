<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Esports extends CI_Controller {
    
    function __Construct(){
        parent::__Construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('esport_model');
    }
    
    function index(){

        $this->load->view('esports/register');

    }
    
    function register(){
       
            $salt      = uniqid(rand(10,10000000),true);
            $json     = $this->input->post('json');
            $data = json_decode($json, TRUE);
            print_r($data['name']);
            
            //{"title":"Title", "name":"Doe","sur_name":"John","email":"vikrant.chauhan@numetriclabz.com","phone":"1234567890","region":"fgjf","zone":"fhgjty","church":"sdtg","password":"12345"}
            $title     = $data['title'];
            $name      = $data['name'];
            $sur_name  = $data['sur_name'];
            $email     = $data['email'];
            $phone     = $data['phone'];
            $region    = $data['region'];
            $zone      = $data['zone'];
            $church    = $data['church'];
            $pass      = $data['password'];
            $saltid     = md5($email."/".$salt."/".$pass);
            $start_time= $this->input->server("REQUEST_TIME");
            $password  = md5($pass);

            $registerData=array(
                'title'     => $title,
                'name'      =>$name,
                'sur_name'  =>$sur_name,
                'email'     => $email,
                'phone'     =>$phone,
                'region'    => $region,
                'zone'      => $zone,
                'church'    =>$church,
                'password'  =>$password,
                'status'    => '0',
                'salt_id'   => $saltid,
                'start_time'=> $start_time
                    );

	    $user_id = $this->esport_model->insertUser($registerData);
            
	     	$this->sendEmail($email,$saltid);

            
            
      
    }
    
    function log(){
                    
            $this->load->view('esports/login');
    }
            
    function sendEmail($email,$saltid){

    	$this->load->library('email');
        $url = base_url()."esports/confirmation/".$saltid;
 		$this->email->from('admin@dailyprep', 'DailyPrep');
		$this->email->to($email);	
		$this->email->subject('[DailyPrep] Almost there! Please Verify Your Email Address');
		$message = "<html><head><head></head><body><p>Hi,</p><p>Thanks for signing up for DailyPrep.</p><p>Please click below link to verify your email.</p>".$url."<br/><p>Sincerely,</p><p>DailyPrep Team</p></body></html>";
		$this->email->message($message);
		return $this->email->send();
    }
    
    public function confirmation($saltid){
		
        
        $start_time1 = $this->input->server("REQUEST_TIME");
	$a = $this->esport_model->confirm_link($saltid);
        $id = $a[0]['userID'];
        
        if(!empty($a)){
		    $recordTime = $a[0]['start_time'];
		    
		    if(($start_time1 - $recordTime) <= '1800'){
		        
		        $this->esport_model->update_time($saltid);
		        echo 'Activated';
		    }
		    else{
                        echo 'Link Expired';
                        echo "<a href='".base_url()."esports/resendConfirmationLink/".$id."'>Resend Link</a>";
		    }
        }
        else{
            
            echo 'Error Occured';
        }
    }
    
    public function resendConfirmationLink($id){
    	
    	$start_time = $this->input->server("REQUEST_TIME");
    	$data = $this->esport_model->updateConfirmationTime($id, $start_time);
    	
    	$email = $data[0]['email'];
    	$saltid = $data[0]['salt_id'];
    	
    	if ($this->sendEmail($email, $saltid)){
		echo 'Link send to your email';		

		}
	else{
                echo 'Error occured';
		}
    }
    
    
    public function login(){
        $json = $this->input->post('json');
        $data = json_decode($json, TRUE);        
        $email = $data['email'];
        $password= $data['password'];        
        $passcode=hash('md5', $password);
            // check for user credentials
        $result = $this->esport_model->check_user($email, $passcode);
            
            if(!empty($result)){
            
            	if ($result[0]['status'] == 1){
                        //If account is confirmed.
                        echo 'account activated';
		        }
                        
		else if ($result[0]['status'] == 0){
		        
		       //If account is not confirmed.
                    
                       echo 'not activated';

		        }
            }
        
    }
    
        function resend(){
        $email = $this->input->post("email");
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_rolekey_exists');
        
        if ($this->form_validation->run() == FALSE){
            
            $array['page'] = 'login';
            $array['footer'] = "login";

            $this->load->view('user/header', $array);
            $this->load->view('user/forget_password');
            $this->load->view('footer');
        }
        else {
            
            $id=$this->user_model->getId($email);
            $data = $this->resendPassword($id,$email);
            $email = $data[0]['email'];
            $saltid = $data[0]['saltid'];
            
            if ($this->sendPassword($email, $saltid)){
                
                
            }
            
            $array['page'] = 'login';
            $array['footer'] = "login";

            $this->load->view('user/header', $array);
            $this->load->view('user/success_forget');
            $this->load->view('footer');
        }
    }
    
    public function resendPassword($id,$email){
        
    	$salt       = uniqid(rand(10,10000000),true);
        $saltid     = md5($email."/".$salt);
    	$start_time = $this->input->server("REQUEST_TIME");
    	$data = $this->user_model->updateTime($id, $start_time,$saltid);
    	return $data;
    	
//    	if ($this->sendEmail($email, $saltid)){
//            
//        }
    }
    
    function sendPassword($email,$saltid){

    	$this->load->library('email');
        $url = base_url()."user/passwordReset/".$saltid;
 		$this->email->from('admin@dailyprep', 'DailyPrep');
		$this->email->to($email);	
		$this->email->subject('[DailyPrep] Reset Password');
		$message = "<html><head><head></head><body><p>Hi,</p><p>Thanks for using DailyPrep.</p><p>Please follow given link to Reset your Password.</p>".$url."<br/><p>Sincerely,</p><p>DailyPrep Team</p></body></html>";
		$this->email->message($message);
		return $this->email->send();
    }
    
    function passwordReset($saltid){
        
        $email=$this->user_model->getEmail($saltid);
        if($email == false){
        $array['page'] = 'login';
        $array['footer'] = "login";
        $this->load->view('user/header', $array);
        $this->load->view('badlink');
        $this->load->view('footer');
        }
        else{
        $array['page'] = 'login';
        $array['footer'] = "login";
        $array['user'] = $email;

        $this->load->view('user/header', $array);
        $this->load->view('user/passwordChange');
        $this->load->view('footer');
        }
    }
    
    function updatePassword(){
        if($_POST){
        $id      = $this->input->post('email');
        $passcode   = $this->input->post('password');
        $password=hash('md5', $passcode);          
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|max_length[15]');
        $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');
        if ($this->form_validation->run() == FALSE){
            
            $array['page'] = 'login';
            $array['footer'] = "login";
            $array['user'] = $id;
            $this->load->view('user/header', $array);
            $this->load->view('user/passwordChange');
            $this->load->view('footer');
        }
        else {
            $this->user_model->update_password($password,$id);
             $array['page'] = 'login';
            $array['footer'] = "login";
            $this->load->view('user/header', $array);
            $this->load->view('user/passwordChangeSuccess');
            $this->load->view('footer');
        }       
        }
        
    }
}
