<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registration extends CI_Controller {

	/**
     * @description :load automatically and initiates the instances
     */
    function __Construct(){
        parent::__Construct();
        $this->load->library('user_agent');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->model('user_model');
        $this->load->driver('session');
    }
    
	
    public function index() {
        $this->load->view('user/registration'); 
    }
    
    public function register(){
    
    	if($_POST){
            $fname      = $this->input->post('fname');
            $phone      = $this->input->post('phone');
            $email      = $this->input->post('email');
            $password   = $this->input->post('password');
            $salt       = uniqid(rand(10,10000000),true);
            $saltid     = md5($email."/".$salt."/".$password);
            $start_time = $this->input->server("REQUEST_TIME");
            $status     = 0;
            $password   = md5($password);

            $insertRegisterData=array($fname, $phone, $email, $password, $salt, $saltid, $start_time, $status);

	    $user_id = $this->user_model->insertRegisterData($insertRegisterData);
	     	
            $insertRegisterData['id'] = $user_id;
            
	     	$this->email($saltid, $email);
            
            $userInfo['userInfo'] = $insertRegisterData;
            $this->load->view('welcome_message',$userInfo); 
            $this->load->view('thankyou');
            $this->load->view('footer');
            print_r($insertRegisterData);
        }
    }
    
        /** @desc send email to activate user account.
	  * @param salt id and email of client
      */
  	function email($saltid,$emailid) {
  	
        $this->load->library('email');
  	    $time = $this->input->server("REQUEST_TIME");
  	    $this->employer_model->updateStartTime($time, $saltid);
	    $emailid = urldecode($emailid);
	    
		$url = base_url()."registration/activateAccount/".$saltid;
		//$this->initializeEmail();
 		$this->email->from('admin@crowdhr.in', 'CrowdHR Support');
		$this->email->to($emailid);
	
		$this->email->subject('[CrowdHR] Almost there! Please Verify Your Email Address');
		$message = "<html><head><head></head><body><p>Hi,</p><p>Thanks for signing up for Crowdhr.</p><p>Please click <a href=$url>here</a> to verify your email.</p><p>If you did not initiate this request, please contact <a href='mailto:admin@crowdhr.in'>admin@crowdhr.in</a>.</p><p>Sincerely,</p><p>The Crowd HR Team</p></body></html>";
		$this->email->message($message);
		$this->email->send();            
 	}

}
