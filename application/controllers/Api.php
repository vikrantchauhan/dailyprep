<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Api extends CI_Controller
{
    
    public function __Construct()
    {
        parent::__Construct();
        $this->load->helper(array(
            'form',
            'url'
        ));
        $this->load->library(array(
            'session',
            'form_validation',
            'email'
        ));
        $this->load->model('api_model');
    }
    
    public function exams()
    {
        
        $data = $this->api_model->getExams();
    }
    
    
    public function previousPapers($examId)
    {
        
    }
    
    public function check()
    {
        
        if ($this->checkSession())
        {
            
            $user_id = $this->input->get('user_id');
            $test_id = $this->input->get('test_id');
            $offset  = $this->input->get('offset');
            
            $response            = $this->api_model->start_test($user_id, $test_id);
            $set                 = $this->api_model->get_question($test_id, $offset);
            $data                = $set[0];
            $data['prev_offset'] = $offset - 1;
            $data['next_offset'] = $offset + 1;
            
            $data['total']  = $this->api_model->get_total($test_id);
            $data['utr_id'] = $response;
            //$this->api_model->update_user_report();
            $chekIfAnswered = $this->api_model->chekIfAnswered($data['id'], $data['utr_id']);
            if($chekIfAnswered) {
            	$data['markedOption'] = $chekIfAnswered[0]['markedOption'];
            	$data['isCorrect'] = $chekIfAnswered[0]['isCorrect'];
            	$data['isMarked'] = $chekIfAnswered[0]['isMarked'];
            	$data['time_taken'] = intval($chekIfAnswered[0]['time_taken']);
            } else {
            	$data['time_taken'] = intval(0);
            }
            
            $utr_id = $this->input->get('utr_id');
            $ans    = $this->input->get('answer_id');
            $isMarked = $this->input->get('isMarked');
            $is_Correct = $this->input->get('is_Correct');
                        
            if ($utr_id != null && $ans != null && $isMarked == false)
            {
            	$question_array = $this->api_model->get_question($test_id, $offset);
            	$question_id = $question_array[0]['id'];
                $this->submit_answer($_GET);
                $this->api_model->update_current_question($utr_id, $user_id, $test_id, $question_id);
                $this->api_model->insert_null_response($question_id,$data['utr_id'],$_GET['time_taken']);
            }
            else
            {
            	$question_array = $this->api_model->get_question($test_id, $offset);
            	$question_id = $question_array[0]['id'];
                $this->api_model->update_current_question($data['utr_id'], $user_id, $test_id, $question_id);
                if(isset($isMarked)) {
                	$question_array = $this->api_model->get_question($test_id, $offset-1);
            		$question_id = $question_array[0]['id'];
                	$this->api_model->insert_bookmarked_response($question_id,$_GET,$_GET['time_taken']);
                	$this->api_model->insert_null_response($data['id'],$data['utr_id'],$_GET['time_taken']);
                } else {
                	//print_r($_GET);
                	$this->api_model->insert_null_response($question_id,$data['utr_id'],$_GET['time_taken']);
                }
            }
            print_r(json_encode($data, true));
        }
    }
    
    
    function submit_answer($data)
    {

        if ($this->checkSession())
        {
            $user_id     = $data['user_id'];
            $test_id     = $data['test_id'];
            $question_id = $data['question_id'];
            $answer      = $data['answer_id'];
            $isCorrect   = $data['is_Correct'];
            $utr_id      = $data['utr_id'];
            $timeTaken   = $data['time_taken'];
            $response    = $this->api_model->submit_answer($user_id, $test_id, $question_id, $answer, $isCorrect, $utr_id,$timeTaken);
        }
    }
    
    public function response() {
    	$utr_id = $this->input->get('utr_id');
    	$test_id = $this->input->get('test_id');
    	$user_id = $this->input->get('user_id');
    	$json = array();
    	$previous_response = $this->api_model->getPreviousResponses($utr_id,$test_id,$user_id);
    	$json['success'] = true;
    	$json['data'] = $previous_response;
    	$json['counts']['markedCounts'] = $this->api_model->getMrkedCount($utr_id,$test_id,$user_id);
    	$json['counts']['attemptedCount'] = $this->api_model->getAttemptedCount($utr_id,$test_id,$user_id);
    	$json['counts']['skippedCount'] = $this->api_model->getSkippedCount($utr_id,$test_id,$user_id);
    	$json['counts']['notVisitedCount'] = $this->api_model->getNotVisitedCount($utr_id,$test_id,$user_id);
    	$json['counts']['markedAndAnsweredCount'] = $this->api_model->getMarkedAndAnsweredCount($utr_id,$test_id,$user_id);
    	$json['report'] = $this->getMarks($user_id, $test_id);
    	echo json_encode($json);
    }
    
    function getMarks($user_id, $test_id) {
    	$result = $this->api_model->get_user_report($user_id, $test_id);
    	$reportArray = array();
    	if($result) {
    		$reportArray['pos_marks'] = $result[0]['correct'] * 1;
    		$reportArray['neg_marks'] = $result[0]['wrong'] * 0.33;
    		$reportArray['total_marks'] = $result[0]['total_count'];
    		$reportArray['obtained_marks'] = $reportArray['pos_marks'] - $reportArray['neg_marks'];
    		$reportArray['attempted'] = $result[0]['attempted'];
    		$reportArray['correct'] = $result[0]['correct'];
    		$reportArray['incorrect'] = $result[0]['wrong'];
    		$reportArray['skipped'] = $result[0]['attempted'] - ($result[0]['wrong'] + $result[0]['correct']);
    		$reportArray['tques'] = $result[0]['total_count'];
    	}
    	return $reportArray;
    }
    
    public function submit_paper()
    {
        
        if ($this->checkSession())
        {
            $user_id        = $this->input->get('user_id');
            $test_id        = $this->input->get('test_id');
            $response       = $this->api_model->submit_paper($user_id, $test_id);
            $result         = $this->api_model->get_user_report($user_id, $test_id);
            $data['report'] = $result[0];
            print_r(json_encode($data, true));
        }
    }
    
    public function getTotalQuetions()
    {
        $test_id  = $this->input->get('test_id');
        $response = $this->api_model->get_total($test_id);
        $arr      = array();
        for ($i = 0; $i < $response; $i++)
        {
            array_push($arr, $i);
        }
        $data['totalQ'] = $arr;
        print_r(json_encode($data, true));
        
    }
    
    /**
     *  @desc : This function will check if user is logged in or not
     *  @return : redirects to login page
     */
    function checkSession()
    {
        
        $session = $this->session->all_userdata();
        
        if (!(array_key_exists('loggedIn', $session) && $session['loggedIn']))
        {
            return false;
        }
        else
        {
            $last     = $this->session->userdata('lastActivity');
            $sess_out = $this->session->userdata('sess_expiration');
            
            if ($sess_out + $last < $this->input->server("REQUEST_TIME"))
            {
                
                $this->session->sess_destroy();
                return false;
            }
            else
            {
                
                $this->session->set_userdata('lastActivity', $this->input->server("REQUEST_TIME"));
                return true;
            }
        }
    }
}