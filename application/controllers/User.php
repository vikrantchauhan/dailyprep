<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller{

    function __Construct(){
		parent::__Construct();
		$this->load->helper(array('form', 'url'));
		$this->load->library(array('session', 'form_validation', 'email')); 
		$this->load->model('user_model');
    }	
    
    function index(){
    	$array['page'] = '';
    	$this->load->view('user/header');
		$this->load->view('user/register_success');
		$this->load->view('footer');
    }

    // function check(){
    //     echo upload_url;
    // }
    
    function socialuserData($reg) {

        // Fetching Values From URL
        $user_ID = $_POST['user_ID'];
        $user_name = $_POST['user_name'];
        $user_email = $_POST['user_email'];
        $user_img_url = $_POST['img_url'];
        
        switch(@exif_imagetype($user_img_url)){
            case IMAGETYPE_JPEG:
                $extensions = array('jpg', 'jpeg');
                break;
            case IMAGETYPE_PNG:
                $extensions = array('png');
                break;
            case IMAGETYPE_GIF:
                $extensions = array('gif');
                break;
        }
        $image_name = strtolower(preg_replace('/\s+/', '', $user_name.$user_ID));

        $image_name = $image_name.'.'.$extensions[0];
        $img_result = $this->uploadImage($user_img_url, $image_name);
        
        //print_r($extensions);
        $result = $this->user_model->ifAlreadyExists($user_email);


        if(!empty($result)){

            $this->setSession($result[0]['id'],$result[0]['email'] , $image_name);
            //redirect(base_url().'previous-years/cse', 'refresh');
            print_r(json_encode($result[0]));
            
        }
        else{
            
            $data = array(
                          'name' => $user_name ,
                          'email' => $user_email,
                          'reg_source' => $reg,
                          'status'=>1,
                          'image' => $image_name
                          );

            // $image_name =  $user_name.time();

            

            $id = $this->user_model->socialuser($data);
        
            $this->setSession($id[0]['id'],$user_name ,$image_name);
            //redirect(base_url().'previous-years/cse', 'refresh');
            print_r(json_encode($id[0]));
        }
    }

    // function facebookuser(){
    //     // Fetching Values From URL
    //     $user_name = $_POST['user_name'];
    //     $user_email = $_POST['user_email'];

    //     $result = $this->user_model->ifAlreadyExists($user_email);

    //     if(!empty($result)){

    //         $this->setSession($result[0]['id'],$result[0]['email']);
    //         //redirect(base_url().'practice/cse', 'refresh');
    //         print_r(json_encode($result[0]));
    //     }
    //     else{

    //         $data = array(
    //                       'name' => $user_name ,
    //                       'email' => $user_email,
    //                       'reg_source' => 'facebook',
    //                       'status'=>1
    //                       );

    //         $id = $this->user_model->socialuser($data);
    //         $this->setSession($id[0 ]['id'],$user_name);

    //         print_r(json_encode($id[0]));
    //     }

    // }
    
    function selectSocialUserPaper($id){
        $array['user'] = $id;
        //print_r($u);
        //$data = $email;
        //print_r($data);
        $this->load->view('user/selectpaper_header');
        $this->load->view('user/selectPaper', $array);
    }

    function showdashboard(){
        $exam_id = $_POST['exam_id'];
        $userId = $_POST['userId'];

        $this->user_model->updateExamID($exam_id, $userId);

        $examData = $this->user_model->examData($exam_id);

        print_r(json_encode($examData[0]));
        //print_r($userId);
        //redirect(base_url().'previous-years/cse', 'refresh');
    }

    function uploadImage($thumbnail, $name){
    
        //$thumbnail = str_replace(" ","%20",$thumbnail);
        
        if(file_exists(upload_url.$name)) {
            unlink(upload_url.$name);
        }
        
        //gets file from network server
        $content = file_get_contents($thumbnail);
        
        //Store in the filesystem.
        $save = file_put_contents(upload_url.$name,$content);
        if($save){
            return true;
        }
        else{
            return false;
        }
    }

    
    function register(){

    	//form validation
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('phone', 'Phone Number', 'required|numeric|min_length[10]|max_length[10]|is_unique[user.phone]');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|max_length[15]');
        $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[user.email]');

        $this->form_validation->set_message('is_unique', 'This %s is already exits');
        
        $array = array();
        
		if ($this->form_validation->run() == FALSE){
		
			$array['page'] = 'register';
		
			$this->load->view('user/header', $array);
			$this->load->view('user/register');
			$this->load->view('footer');
		}
		else{
			$user = $this->input->post('name');
			$email = $this->input->post('email');
			$pass= $this->input->post('password');
			$passcode=hash('md5', $pass);

			//encript the code
			$salt       = uniqid(rand(10,10000000),true);
			$saltid     = md5($email."/".$salt."/".$pass);
			$start_time = $this->input->server("REQUEST_TIME");
			$status     = 0;

			//form array to be stored in database.
			$data = array(
							'name' => $user,
							'email'=> $email,
							'phone'=> $this->input->post('phone'),
							'password'=>$passcode,
							'salt' => $salt,
							'saltid'=> $saltid,
							'start_time'=>$start_time,
							'status'=>$status,
                            'reg_source' => 'website'
						);
			
			// insert form data into database
			if ($this->user_model->insertUser($data)){

				//mailing 
				if ($this->sendEmail($this->input->post('email'),$saltid)){
				
					$array['page'] = "";
					$array['footer'] = true;
					// successfully sent mail	
					$this->load->view('user/header', $array);
					$this->load->view('user/register_success');
					$this->load->view('footer');
				}
				else{
					$array['page'] = "";	
					$this->load->view('user/header', $array);
					$this->load->view('user/register');
					$this->load->view('footer');
				}
			}
			else{	
				$this->load->view('user/header', $array);
				$this->load->view('user/register');
				$this->load->view('footer');
			}
		}
    }
    

    /** @desc send email to activate user account.
	  * @param salt id and email of client
      */
    //mailing function
    function sendEmail($email,$saltid){

    	$this->load->library('email');
        $url = base_url()."user/confirmation/".$saltid;
 		$this->email->from('admin@dailyprep', 'DailyPrep');
		$this->email->to($email);	
		$this->email->subject('[DailyPrep] Almost there! Please Verify Your Email Address');
		$message = "<html><head><head></head><body><p>Hi,</p><p>Thanks for signing up for DailyPrep.</p><p>Please click below link to verify your email.</p>".$url."<br/><p>Sincerely,</p><p>DailyPrep Team</p></body></html>";
		$this->email->message($message);
		return $this->email->send();
    }
 	
    
    public function resendConfirmationLink($id){
    	
    	$start_time = $this->input->server("REQUEST_TIME");
    	$data = $this->user_model->updateConfirmationTime($id, $start_time);
    	
    	$email = $data[0]['user_email'];
    	$saltid = $data[0]['saltid'];
    	
    	if ($this->sendEmail($email, $saltid)){
				
			$array['page'] = "";
			$array['footer'] = true;
			// successfully sent mail	
			$this->load->view('user/header', $array);
			$this->load->view('user/register_success');
			$this->load->view('footer');
		}
		else{
			$array['page'] = "";	
			$this->load->view('user/header', $array);
			$this->load->view('user/register');
			$this->load->view('footer');
		}
    }
    
    
    /**description* Called when user clicks on the confirmation link sent to
    				his account after registration. This function confirms
    				that the user has registered with site. Also, validate
    				whether the link is valid and not yet expired
      *@param String (Confimation Token)
      *@return Result
      */
	public function confirmation($saltid){
		
		$array['page'] = "";
		$array['footer'] = true;
        
        $start_time1 = $this->input->server("REQUEST_TIME");
		$a = $this->user_model->confirm_link($saltid);
        
        if(!empty($a)){
		    $recordTime = $a[0]['start_time'];
		    
		    if(($start_time1 - $recordTime) <= '1800'){
		        
		        $sql = "UPDATE user SET status = 1 where saltid = '$saltid';";
		        $data = $this->db->query($sql);
		        
		        $this->load->view('user/header', $array);
				$this->load->view('user/confirmation_success');
				$this->load->view('footer', $array);
		    }
		    else{
		    	$array['email'] = $a[0]['user_id'];
		        $this->load->view('user/header', $array);
				$this->load->view('user/confirmation_error', $array);
				$this->load->view('footer', $array);
		    }
        }
        else{
            
            $this->load->view('user/header', $array);
			$this->load->view('user/register', $array);
			$this->load->view('footer', $array);
        }
    }
    
    
    public function login(){
    	
    	// get form input
        $email = $this->input->post("email");
        $password= $this->input->post('password');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_rolekey_exists');
       
        $this->form_validation->set_rules('password', 'Password', 'required');
        
        $array['page'] = 'login';
		$array['footer'] = "login";
        $data['error'] = 0;
        
		if ($this->form_validation->run() == FALSE){

			$this->load->view('user/header', $array);
            $this->load->view('user/login',$data);
            $this->load->view('footer');
        }
        else{
            
            $passcode=hash('md5', $password);
            // check for user credentials
            $result = $this->user_model->check_user($email, $passcode);
            
            if(!empty($result)){
            
            	if ($result[0]['status'] == 1){

                
                    $abr = $result[0]['abbreviation'];
                    $this->setSession($result[0]['id'],$result[0]['name'] ,$result[0]['image'] );

                    if ($abr != null) {
                        redirect(base_url().'previous-years/'.$abr, 'refresh'); 
                    }
                    else{
                        $array['user'] = $result[0]['id'];
                        $this->load->view('user/selectpaper_header');
                        $this->load->view('user/selectPaper', $array);
                    }

		                       
		        }
		        else if ($result[0]['status'] == 0){
		        
		        	//If account is not confirmed
		            $this->load->view('user/header', $array);
				    $this->load->view('user/login-error-1');
				    $this->load->view('footer');
		        }
            }	
            else{
            	//When user is not yet registered
                $data['error'] = 1;
            	$this->load->view('user/header', $array);
            	$this->load->view('user/login',$data);
                $this->load->view('footer');
            }
        }
    }
    
    
    /**description* Creates a new user session
      *@param String (User Id)
      *@param String (User Email)
      */
    function setSession($userId,$userName, $userImage) {
	    
    	$userSession = array('userId'=>$userId,
                     		 'userName'=>$userName,
                     		 'lastActivity' => $_SERVER["REQUEST_TIME"],
                     		 'sess_expiration'=>3600,
                     		 'loggedIn'=>TRUE ,
                             'userImage' => $userImage);
		$this->session->set_userdata($userSession);
 	}

 	public function questions(){
        $this->load->view("student/header");
        $this->load->view("student/questions");
        $this->load->view("student/footer");
    }
    
    /*+++++++++++++++++++++++++ forget password +++++++++++++++++++++++++++++++++++*/
    
    function forget(){
        $array['page'] = 'login';
	$array['footer'] = "login";
        $this->load->view("user/header", $array);
        $this->load->view("user/forget_password");
        $this->load->view("footer");
        
    }
    
    function resend(){
        $email = $this->input->post("email");
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_rolekey_exists');
        
        if ($this->form_validation->run() == FALSE){
            
            $array['page'] = 'login';
            $array['footer'] = "login";

            $this->load->view('user/header', $array);
            $this->load->view('user/forget_password');
            $this->load->view('footer');
        }
        else {
            
            $id=$this->user_model->getId($email);
            $data = $this->resendPassword($id,$email);
            $email = $data[0]['email'];
            $saltid = $data[0]['saltid'];
            
            if ($this->sendPassword($email, $saltid)){
                
                
            }
            
            $array['page'] = 'login';
            $array['footer'] = "login";

            $this->load->view('user/header', $array);
            $this->load->view('user/success_forget');
            $this->load->view('footer');
        }
    }
    
    public function resendPassword($id,$email){
        
    	$salt       = uniqid(rand(10,10000000),true);
        $saltid     = md5($email."/".$salt);
    	$start_time = $this->input->server("REQUEST_TIME");
    	$data = $this->user_model->updateTime($id, $start_time,$saltid);
    	return $data;
    	
//    	if ($this->sendEmail($email, $saltid)){
//            
//        }
    }
    
    function sendPassword($email,$saltid){

    	$this->load->library('email');
        $url = base_url()."user/passwordReset/".$saltid;
 		$this->email->from('admin@dailyprep', 'DailyPrep');
		$this->email->to($email);	
		$this->email->subject('[DailyPrep] Reset Password');
		$message = "<html><head><head></head><body><p>Hi,</p><p>Thanks for using DailyPrep.</p><p>Please follow given link to Reset your Password.</p>".$url."<br/><p>Sincerely,</p><p>DailyPrep Team</p></body></html>";
		$this->email->message($message);
		return $this->email->send();
    }
    
    function passwordReset($saltid){
        
        $email=$this->user_model->getEmail($saltid);
        if($email == false){
        $array['page'] = 'login';
        $array['footer'] = "login";
        $this->load->view('user/header', $array);
        $this->load->view('badlink');
        $this->load->view('footer');
        }
        else{
        $array['page'] = 'login';
        $array['footer'] = "login";
        $array['user'] = $email;

        $this->load->view('user/header', $array);
        $this->load->view('user/passwordChange');
        $this->load->view('footer');
        }
    }
    
    function updatePassword(){
        if($_POST){
        $id      = $this->input->post('email');
        $passcode   = $this->input->post('password');
        $password=hash('md5', $passcode);          
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|max_length[15]');
        $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');
        if ($this->form_validation->run() == FALSE){
            
            $array['page'] = 'login';
            $array['footer'] = "login";
            $array['user'] = $id;
            $this->load->view('user/header', $array);
            $this->load->view('user/passwordChange');
            $this->load->view('footer');
        }
        else {
            $this->user_model->update_password($password,$id);
             $array['page'] = 'login';
            $array['footer'] = "login";
            $this->load->view('user/header', $array);
            $this->load->view('user/passwordChangeSuccess');
            $this->load->view('footer');
        }       
        }
        
    }
            
    function rolekey_exists($key) {
        $ret = $this->user_model->mail_exists($key);
        
        if($ret == FALSE)
        {
           $this->form_validation->set_message('rolekey_exists', 'You are not Registerd Yet.'); 
           return FALSE;
        }
        else {
             return TRUE;
         }
        
    }
	
    function logout(){
        $this->session->sess_destroy();
    	//echo "<script>window.location.href = '".base_url()."' ;</script>";
        redirect(base_url().'user/login', 'refresh');
    }
    
    function resendMail(){
        $array['page'] = 'login';
	$array['footer'] = "login";
        $this->load->view("user/header", $array);
        $this->load->view("user/forget_link");
        $this->load->view("footer");
    }
    
    function resendEmail(){
        $email = $this->input->post("email");
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_rolekey_exists');
        
        if ($this->form_validation->run() == FALSE){
            
            $array['page'] = '';
            $array['footer'] = "login";

            $this->load->view('user/header', $array);
            $this->load->view('user/forget_link');
            $this->load->view('footer');
        }
        else {
            
            $id=$this->user_model->getId($email);
            $data = $this->resendPassword($id,$email);
            $email = $data[0]['email'];
            $saltid = $data[0]['saltid'];
            
            if ($this->sendEmail($email, $saltid)){
                $array['page'] = "";
			$array['footer'] = true;
			// successfully sent mail	
			$this->load->view('user/header', $array);
			$this->load->view('user/register_success');
			$this->load->view('footer');
                
            }
            
            else{
			$array['page'] = "";	
			$this->load->view('user/header', $array);
			$this->load->view('user/register');
			$this->load->view('footer');
		}
        }
    }
}
