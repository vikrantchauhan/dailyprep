<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Landing extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('main_model');
		$this->load->helper('url_helper');
        $this->load->library('session'); 
        $this->load->library(array('form_validation' , 'email'));

	}

	public function index(){
 
         $this->load->model('user_model');
         if($this->checkSession()){

            /*$user_id = $this->session->userdata('userId');
            
            $info = $this->user_model->get_user_info($user_id);
            $data['user_info'] = $info[0];
            $data['page'] = "practice";
            $exam_id = $info[0]['exam_id'];
            $data_p['paper'] = $this->user_model->get_previous_paper("previous", "exam", $exam_id , $user_id);
            
            $this->load->view("student/header", $data);
            $this->load->view("student/previous_year" , $data_p);
            $this->load->view("student/footer");*/
         	redirect(base_url().'previous-years/cse');
        }
         else{
            
             $this->load->view('landing/header');
             $this->load->view('landing/page');
             $this->load->view('landing/footer');   

         }
	}
	
    function subscribe(){

        $subscriber_email = $_POST['email'];

        if (!filter_var($subscriber_email, FILTER_VALIDATE_EMAIL) === false) {
          
          //echo("$email is a valid email address");

            $iSEmailExit = $this->checkEmail($subscriber_email);

            if($iSEmailExit == true)
            {
                echo "exit";
            }

            else{

                $insertEmail = $this->newsletter($subscriber_email);

                if($insertEmail == true){
                    echo "success";
                }
                else{
                    echo "failed";
                }
            }
        } 

        else {
         
          echo("not valid email");
        }
    }


	public function checkEmail($email){
            
         

  //        $this->form_validation->set_message('is_unique', 'This %s is already exits');
		
		// $email = $this->input->post("email");
  //       $data = array(            
		//             'news_email'=> $email,
		//          );
       
  //       $this->main_model->newsemail($data);
  //       $this->load->view('landing/header');
  //       $this->load->view('landing/page');
	 //   $this->load->view('landing/footer');

        //$subscriber_email = $_POST['email'];
        //echo ("email = ". $subscriber_email);

        // $data = array( 

        //             'news_email'=> $subscriber_email,
        //          );
       
        $valid = $this->main_model->checkSubscriber($email);

        // print_r(json_encode($valid[0]));
        if($valid > 0 ){
            //print_r($valid)
            //echo "exit";

            return true;
        }
        else{
            //echo "notexit";

            return false;
        }

    }


    
    /**@desc Stores the subscriptions emails to the database
      *@param String (Email)
      */ 

    function newsletter($email){
        //$email = $this->input->post("email");
        $data = array(            
                    'news_email'=> $email,
                 );

        $this->main_model->newsemail($data);
        //redirect(base_url());

        // echo $email;

        return true;
    }
        
        public function question(){
            $this->load->view('header');
            $this->load->view('questions');
            
        }
        
        public function updatequestion(){
        $this->load->view('header');
        $this->load->view('update');
            
        }
        
        
        
        public function saveQuestion(){
            $question = $this->input->post('question');
            $op1 = $this->input->post('option1');
//            $op2 = $this->input->post('option2');
//            $op3 = $this->input->post('option3');
//            $op4 = $this->input->post('option4');
            $answer= $this->input->post('answer');
            $explain=$this->input->post('explain');
            $test_id=$this->input->post('test_id');
            
//            $option = array($op1,$op2,$op3,$op4);
            
            $data = array(
                'test_id' =>$test_id,
                'question' => $question,
                'answer_choices'=> $op1,
                'correct_answer'=> $answer,
                'explanation'=>$explain
	);
            $this->main_model->saveQuestion($data);
            $this->load->view('header');
            $this->load->view('questions');
    
        }
        
        function edit(){
             
        $test_id= $this->input->post('test_id');  	
    	$info = $this->main_model->getQuestion($test_id);
    	$this->load->view('header');
        $data['test_id']=$test_id;
        $data['info'] = $info;
        $this->load->view('updatequestions',$data);
    }
           
    function editQuestion($id,$test_id){
        $question = $this->input->post('question');
            $op1 = $this->input->post('option1');
            $answer= $this->input->post('answer');
            $explain=$this->input->post('explain');
            $this->main_model->editQuestion($question,$op1,$answer,$explain,$id);
            
            $info = $this->main_model->getQuestion($test_id);
            $this->load->view('header');
            $data['test_id']=$test_id;
            $data['info'] = $info;
            $this->load->view('updatequestions',$data);
    
    }
    function checkSession() {
    
        $session = $this->session->all_userdata();

        if(!(array_key_exists('loggedIn', $session) && $session['loggedIn']))  {
            return false;
        }
        else{
            $last=$this->session->userdata('lastActivity');
            $sess_out=$this->session->userdata('sess_expiration');
            
            if($sess_out + $last < $this->input->server("REQUEST_TIME")){
                
                $this->session->sess_destroy();
                return false;
            }
            else{
                
                $this->session->set_userdata('lastActivity', $this->input->server("REQUEST_TIME"));
                return true;
            }
        }
    }

    function contact(){
        $this->load->view('contact-header');
        $this->load->view('contact');
        $this->load->view('landing/footer');
    }

    function contactmessage(){
        
         $name = $_POST['name'];
         $email = $_POST['email'];
         $phoneno = $_POST['mobileno'];
         $message = $_POST['message'];

         $data = array(
                        'name' => $name,
                        'email' => $email,
                        'phoneno' => $phoneno,
                        'message' => $message );

         // $res = $this->main_model->contact($data);
         // if($res>0){
         //    $resEmail = $this->sendemail();
         //    

         // }
         if($this->main_model->contact($data)){
            $resEmail = $this->sendemail($data);
            print_r($resEmail);
         }
         else{
            echo "Sorry Your message has not been send yet.";
         }
         

         

    }
    function sendemail($data){
        //$this->load->library('email');
        $this->email->from('admin@dailyprep', 'DailyPrep');
        $this->email->to('sachin.jaiswal@numetriclabz.com');   
        $this->email->subject('[DailyPrep] A new contact message found');
        $message = "<html><head><head></head><body><p>Hi,</p><p>A new contact message found.</p><p><strong>Sender Name : </strong></p>".$data['name']."<br/><p><strong>Sender Email : </strong></p>".$data['email']."<br/><p><strong>Sender PhoneNumber : </strong></p>".$data['phoneno']."<br/><p><strong>Message : </strong></p>".$data['message']."<br/><p>Sincerely,</p><p>DailyPrep Team</p></body></html>";
        
        $this->email->message($message);
        return $this->email->send();
    }
}