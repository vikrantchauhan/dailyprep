
<!-- Main jumbotron for a primary marketing message or call to action -->
<div class="dp-header-section" >

	<div class="dp-landing-header-login">
		<a href="<?php echo base_url(); ?>user/login">
			<button class="dp-landing-login">Login</button>
		</a>
	</div>
	<div class="dp-header-container">
	
		<img src="<?php echo base_url(); ?>images/daily-logo.png">
	
                <h1 class="dp-header-main-heading">
                   GATE - 2017
                </h1>
		
		
		<p class="dp-header-sec-heading">Free Online Test Series, Mock Test, Practice Test, Solved and Unsolved Previous Year Papers</p>

		<a href="<?php echo base_url(); ?>user/register">
                    <button class="dp-button-l">SIGN UP FOR A FREE ACCOUNT</button>
		</a>

	</div>
</div>

<div class="dp-section-margin">
	<div class="dp-courses-section">

		<h2>Courses We Offer</h2>
	
		<div class="dp-section-inner-margin">
                    <a href="<?php echo base_url(); ?>user/register">
			<div class="dp-course-block">
				<img class="dp-block-margin" src="<?php echo base_url(); ?>images/cse.png"/>
				<p class="dp-block-margin">CSE</p>
			</div>
                    </a>
                    <a href="<?php echo base_url(); ?>user/register">
			<div class="dp-course-block">
				<img class="dp-block-margin" src="<?php echo base_url(); ?>images/ece.png"/>
				<p class="dp-block-margin">ECE</p>
			</div>
                    </a>
                    <a href="<?php echo base_url(); ?>user/register">
			<div class="dp-course-block">
				<img class="dp-block-margin" src="<?php echo base_url(); ?>images/me.png"/>
				<p class="dp-block-margin">ME</p>
			</div>
                    </a>
                    <a href="<?php echo base_url(); ?>user/register">
			<div class="dp-course-block">
				<img class="dp-block-margin" src="<?php echo base_url(); ?>images/ee.png"/>
				<p class="dp-block-margin">EE</p>
			</div>
                    </a>
		</div>
	</div>
</div>

<div class="dp-section-margin dp-white-background">
	

		<h2>How Does DailyPrep Helps</h2>
	
		<div class="dp-how-margin">
	
			<div class="dp-how-section-text">
                            <img src="<?php echo base_url(); ?>images/video.png"><br/>
				<span>Video Tutorial</span><br>
				<span>For Every Topic</span>
			</div>
			<div class="dp-how-section-image">
				<img class="dp-how-section-img" src="<?php echo base_url(); ?>images/video_scetion.png">
			</div>
		</div>
	
		<div class="dp-how-margin">
	
			<div class="dp-how-section-image">
				<img class="dp-how-section-img" src="<?php echo base_url(); ?>images/presentation_2.png">
			</div>
		
			<div class="dp-how-section-text">
                            <img src="<?php echo base_url(); ?>images/presentation-content.png"><br/>
				<span>Text Content </span><br>
				<span>In Form of Presentation</span>
			</div>
		</div>
	
		<div class="dp-how-margin">
			<div class="dp-how-section-text">
                            <img src="<?php echo base_url(); ?>images/question-pape.png"><br/>
				<span>Practise Questions</span><br>
				<span>With Great Explanation</span>
			</div>
			<div class="dp-how-section-image">
				<img class="dp-how-section-img" src="<?php echo base_url(); ?>images/presentation_2.png">
			</div>
		</div>
	
</div>

<div class="dp-section-margin">

		<h2>Other Benifits/Features</h2>
	
		<div class="dp-section-inner-margin">
			<div class="dp-benifit-block">
				<img class="dp-block-margin" src="<?php echo base_url(); ?>images/adaptive-learning.png"/>
				<p class="dp-block-margin">Adaptive Learning</p>
			</div>
		
			<div class="dp-benifit-block">
				<img class="dp-block-margin" src="<?php echo base_url(); ?>images/graph.png"/>
				<p class="dp-block-margin">Performance Evaluation</p>
			</div>
		
			<div class="dp-benifit-block">
				<img class="dp-block-margin" src="<?php echo base_url(); ?>images/free-test.png"/>
				<p class="dp-block-margin">Mock Tests</p>
			</div>

			<div class="dp-benifit-block">
				<img class="dp-block-margin" src="<?php echo base_url(); ?>images/previousyear.png"/>
				<p class="dp-block-margin">Previous Years<br>(Solved & Unsolved)</p>
			</div>
		</div>
</div>

<div class="dp-section-margin dp-white-background">

		<h2>Attempt A Practice Test</h2>
	
		<div class="dp-section-inner-margin">
		
                    <div class="dp-section-inner-margin">
                    <a href="<?php echo base_url(); ?>user/register">
			<div class="dp-course-block">
				<img class="dp-block-margin" src="<?php echo base_url(); ?>images/cse.png"/>
				<p class="dp-block-margin">CSE</p>
			</div>
                    </a>
                    <a href="<?php echo base_url(); ?>user/register">
			<div class="dp-course-block">
				<img class="dp-block-margin" src="<?php echo base_url(); ?>images/ece.png"/>
				<p class="dp-block-margin">ECE</p>
			</div>
                    </a>
                    <a href="<?php echo base_url(); ?>user/register">
			<div class="dp-course-block">
				<img class="dp-block-margin" src="<?php echo base_url(); ?>images/me.png"/>
				<p class="dp-block-margin">ME</p>
			</div>
                    </a>
                    <a href="<?php echo base_url(); ?>user/register">
			<div class="dp-course-block">
				<img class="dp-block-margin" src="<?php echo base_url(); ?>images/ee.png"/>
				<p class="dp-block-margin">EE</p>
			</div>
                    </a>
                    </div>
		</div>
</div><!--

 Carousel================================================== 
<div id="myCarousel" class="carousel slide" data-ride="carousel">

	<Indicators >
	<ol class="carousel-indicators">
		<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		<li data-target="#myCarousel" data-slide-to="1"></li>
		<li data-target="#myCarousel" data-slide-to="2"></li>
	</ol>

	<div class="carousel-inner" role="listbox">
	
		<div class="item active">
		
			
			<div class="container">
				
				<div class="carousel-caption dp-testimonial">
					<h1>DailyPrep has helped me in achieving my dream of joining one of the top IIT. Without them I could not have done it.</h1>
					<img class="dp-testimonial-img" src="<?php echo base_url(); ?>images/user_img.png">
					<p><span class="dp-testimonial-name">Dinesh Dogra</span><br> Computer Science & Engineering</p>
				</div>
			</div>
		</div>

	</div>
	
	<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
		<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
	</a>
	
	<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
		<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		<span class="sr-only">Next</span>
	</a>
</div> -->

 <div class="dp-section-margin dp-subscribe-box">
        <h2>Subscribe To Our Newsletter</h2>

        <div class="dp-box">

                <!-- <form action="<?php echo base_url(); ?>landing/newsletter" method="post">
                        
                       <label for="exampleInputEmail3">Email address</label><br/>
                       <input type="email" class="form-control" id="exampleInputEmail3" name="email" placeholder="Email" required>
                       <button type="submit" class="btn">Subscribe</button>

                       <span class="text-danger"><?php echo form_error('name'); ?></span>
                </form> -->

                <form id="subscribeForm" name="subscribeForm" method="post" action="<?php echo base_url(); ?>landing/newsletter" onsubmit="return">
                        
                       <label for="exampleInputEmail3">Email address</label><br/>
                       <input type="email" class="form-control" id="exampleInputEmail3" name="email" placeholder="Email" required>
                       <!-- <button type="submit" class="btn" id="btn-submit">Subscribe</button> -->

                       <input type="button" name="Submit" id="Submit" value="Subscribe" />

                      <div class="text-danger" id="validEmail">Please! Enter a valid Email</div>
                      <div class="text-danger" id="subscribed">You Have Already Subscribe With Us</div>
                      <div class="text-success" id="success">Thank You to Subscribe With Us.</div>
                      <div class="text-danger" id="tryAgain">Try Again...</div>
                
                </form>
        </div>
</div>

	
<div class="dp-section-margin dp-white-background">


                <div class="row">

                        <div class="dp-col-app">
                                <img class="dp-download-img" src="<?php echo base_url(); ?>images/download-app.png"/>
                        </div>

                        <div class="dp-col-app">
                                <h2>Download The App</h2>
                                <a href="https://play.google.com/store/apps/details?id=numtech.daily.prep"><img class="dp-download-app" src="<?php echo base_url(); ?>images/google-play.png"/></a>
                        </div>


                </div>

        </div>


  
