<footer class="foot" >
			
			
    <div class="dp-co">

        <div class="dp-fo">
                <a href="<?php echo base_url();?>landing/contact"><p>Contact Us</p></a>
                <p>Privacy Policy</p>
                <p>Terms & Conditions</p>
        </div>
        <div class="dp-fo">
                <p>Syllabus</p>
                <p>Solved Papers</p>
                <p>Unsolved Papers</p>
        </div>
        <div class="dp-fo">
                <p>Blog</p>
                <p>Alert & Notifications</p>
        </div>
    </div>
            <div class="row dp-copyright-div">
                    <div class="col-so">
                            <p>Copyright 2015 By Numetric Technologies. All Rights Reserved.</p>
                    </div>
                    <div class="col-right">
                            <img class="dp-social-img" src="<?php echo base_url(); ?>images/facebook.png">
                            <img class="dp-social-img" src="<?php echo base_url(); ?>images/twitter.png">
                            <img class="dp-social-img" src="<?php echo base_url(); ?>images/instagram.png">
                            <img class="dp-social-img" src="<?php echo base_url(); ?>images/youtube.png">
                    </div>
            </div>

    </div>
</footer>
    
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-83333307-2', 'auto');
      ga('send', 'pageview');

    </script>

    <script>

        $('#Submit').click(function() {

            var email = $('#exampleInputEmail3').val();

            //alert(email);

            var dataString = '&email=' + email; 

            $.ajax({
                type: 'POST',
                url: 'landing/subscribe',
                data:dataString,
                cache : false,
                success : function(response){

                    // var obj = JSON.parse(response);
                    //alert(response);

                    if (response == "not valid email" ) {
                        // $('#Submit').after('<div class="text-danger">Please! Enter a valid Email</div>');
                        $('#validEmail').fadeIn(1000).delay(5000).fadeOut();
                        
                    }
                    else if(response == "exit" ) {
                        // $('#Submit').after('<div class="text-danger">You Have Already Subscribe With Us</div>');

                        $('#subscribed').fadeIn(1000).delay(5000).fadeOut();
;

                    }
                    else if(response == "success"){
                        // $('#Submit').after('<div class="text-success">Thank You to Subscribe With Us.</div>');
                        $('#success').fadeIn(1000).delay(5000).fadeOut();
                    }
                    else {
                        // $('#Submit').after('<div class="text-danger">Try Again...</div>');
                        $('#tryAgain').fadeIn(1000).delay(5000).fadeOut();
                    }

                }
                
            });

        });

        
    </script>
    <script>
    $('#btn').click(function() {

        var name = $('#name').val();
        var email = $('#email').val();
        var phoneno = $('#phoneno').val();
        var message = $('#message').val();
        //alert(name);

        if(name == '' || email== '' || phoneno == '' || message == ''){
            //alert("field required")
           
            $('#error-msg').fadeIn(1000).delay(5000).fadeOut();
        }
        else{
            var dataString = '&name='+name + '&email=' + email + '&mobileno=' + phoneno + '&message=' + message;

        $.ajax({
            type: 'POST',
            url: 'contactmessage',
            data:dataString,
            cache : false,
            success : function(response){
                $('#s-msg').fadeIn(1000);
                // if(response == 'success'){
                //     $('#s-msg').fadeIn(1000);
                // }


            }
        });
        }

        
    });
</script>

	</body>
</html>
