<section class="dp-contact">
			<div class="dp-contact-us">
				<div class="container">
					<div class="row">
						<div class="col-sm-8 col-sm-offset-2 text-center">

							<h2>Contact Us</h2>
							<hr>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>
						
						</div>
					</div>

					<div class="row form-box">
						<div class="col-sm-6 text-center">
							<div class="google-map">
								<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3508.9905557444586!2d77.03604161467464!3d28.41954183250228!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d229e8903adbb%3A0xce5171e7f7efb3f5!2sJMD+MEGAPOLIS%2C+Sector+48%2C+Gurgaon%2C+Haryana+122018!5e0!3m2!1sen!2sin!4v1474483628118" width="500" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
							</div>
						 </div>

						 <div class="col-sm-6 text-center">
						 	<div class="contact-form">
						 		<h4> Get In Touch With Us</h4>

						 		<div class="text-success" id="s-msg">
						 			<span> Thank you your message has been submitted to us</span>
						 		</div>
						 		<div class="text-danger" id="error-msg">
						 			<span> All fields are required</span>
						 		</div>
						 		<form action="#" method="post" data-toggle="validator">
							 			
							 			<div class="row">
							 				<div class="col-sm-12">
							 					
									 			<div class="form-group">
									 				<input class="form-control" name="name" id="name" type="text" placeholder="Your Name" required>
									 			</div>
									 		
							 				</div>

							 				<div class="col-sm-12">
							 					
									 			<div class="form-group">
									 				<input class="form-control" name="email" id="email" type="email" placeholder="Your Email" required>
									 			</div>
									 		
							 				</div>

							 				<div class="col-sm-12">
							 					
									 			<div class="form-group">
									 				<input class="form-control" name="mobileno" id="phoneno" type="tel" placeholder="Your Phone Number" required>
									 			</div>
									 		
							 				</div>

							 				<div class="col-sm-12">
							 					
									 			<div class="form-group">
									 				<textarea class="form-control" name="message" id="message" type="text" placeholder="Your Message" rows="5" required></textarea>
									 			</div>
									 		
							 				</div>

							 				<div class="col-sm-12">
							 					
									 			<div class="form-group">
									 				<input class="btn submit-btn" name="btn" id="btn" type="button" value="Submit">
									 			</div>
									 		
							 				</div>

							 			</div>
					
						 		</form>
						 	</div>
						 </div>
					</div>
				</div>
			</div>
		</section>