<!DOCTYPE html>
<html lang="en" ng-app="testApp">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>DialyPrep</title>
		<meta name="google-signin-client_id" content="990887233872-rbqr5rurc6bq8ek79t3sud9krjvhji5e.apps.googleusercontent.com">

		<!-- jQuery ui css -->
		<link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">

		<!-- bootstrap css -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

		<!-- custom CSS -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/dashboard.css">

		<!-- font awesome -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/font-awesome/css/font-awesome.min.css">

		
				<!-- Facebook Pixel Code -->
		<script>
		!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
		n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
		document,'script','https://connect.facebook.net/en_US/fbevents.js');

		fbq('init', '176894866070811');
		fbq('track', "PageView");</script>
		<noscript><img height="1" width="1" style="display:none"
		src="https://www.facebook.com/tr?id=176894866070811&ev=PageView&noscript=1"
		/></noscript>
		<!-- End Facebook Pixel Code -->
        <script type="text/x-mathjax-config">
            MathJax.Hub.Config({
                tex2jax: {
                    inlineMath: [['$','$'], ['\\(','\\)']],
                    processEscapes: true
                }
            });
        </script>
        
        <script type="text/javascript" async src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_CHTML"></script>

        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
  		<script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
                
	</head>
	
	<body>
	<?php echo '<script> var username = "'.$this->session->userdata('userName').'"</script>'; ?>
	<?php echo '<script> var userImage = "'.base_url().'upload/profile_pic/'.$this->session->userdata('userImage').'"</script>'; ?>