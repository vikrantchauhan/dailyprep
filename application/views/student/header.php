<!DOCTYPE html>
<html lang="en" ng-app="testApp">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>DialyPrep</title>

		<meta name="google-signin-client_id" content="990887233872-rbqr5rurc6bq8ek79t3sud9krjvhji5e.apps.googleusercontent.com">
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/dashboard.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/font-awesome/css/font-awesome.min.css">
				<!-- Facebook Pixel Code -->
		<script>
		!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
		n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
		document,'script','https://connect.facebook.net/en_US/fbevents.js');

		fbq('init', '176894866070811');
		fbq('track', "PageView");</script>
		<noscript><img height="1" width="1" style="display:none"
		src="https://www.facebook.com/tr?id=176894866070811&ev=PageView&noscript=1"
		/></noscript>
		<!-- End Facebook Pixel Code -->
	</head>
	
	<body>
	
		<div class="container-fluid">

			<div class="row">
			
				<div class="navbar-header navbar-inverse">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>

				<!--div id="navbar" class="col-sm-3 col-md-2 sidebar collapse">
					<div class="dp-navbar-img-container text-center">
						<img class="dp-navbar-img" src="<?php echo base_url(); ?>images/web-logo.png"/>
					</div>

					<div class="dp-stream">

						<select class="dp-stream-select">
							<option>CSE</option>
							<option>ECE</option>
							<option>EE</option>
							<option>ME</option>
						</select>
					</div>

					<ul class="nav nav-sidebar">

						<p class="dp-nav-section">Study</p>

						<li>
							<a href="/learn/<? echo $user_info['exam']; ?>"><span class="glyphicon glyphicon-book" aria-hidden="true"></span>Learn</a>
						</li>
						
						<li class="<? print_r(($page=="practice") ? "active" : ""); ?>">
							<a href="/practice/<? echo $user_info['exam']; ?>"><i class="fa fa-pencil" aria-hidden="true"></i>Practice</a>
						</li>
						
						<li>
							<a href="/test-series/<? echo $user_info['exam']; ?>"><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>Test Series</a>
						</li>
						<!--<li><a href="#"><i class="fa fa-commenting-o fa-2" aria-hidden="true"></i>Doubts</a></li>-->

						<!--p class="dp-nav-section">Compare</p>

						<li>
							<a href="/friends"><i class="fa fa-users" aria-hidden="true"></i>Friends</a>
						</li>
						
						<li>
							<a href="/leaderboard"><i class="fa fa-line-chart" aria-hidden="true"></i>Leaderboard</a>
						</li>

						<p class="dp-nav-section">Study Tools</p>

						<li>
							<a href="/dashboard"><i class="fa fa-tachometer" aria-hidden="true"></i>Dashboards</a>
						</li>

						<li class="<? print_r(($page=="previous") ? "active" : ""); ?>">
							<a href="/previous-years/<? echo $user_info['exam']; ?>"><i class="fa fa-file-text-o" aria-hidden="true"></i>Previous Papers</a>
						</li>

						<li>
							<a href="/discuss"><i class="fa fa-comments-o" aria-hidden="true"></i>Discuss</a>
						</li>
					</ul>
				</div><!-- Side Navigation Bar Ends -->
				
				<div class="col-sm-12 ccol-md-12 main" ng-controller="testCtrl">
		
					<div class="dp-dashboard-header">
						<img id="notify" src="<?php echo base_url(); ?>images/notification.png">
						<div class="dropdown">
							<span><?php echo $this->session->userdata('userName'); ?></span>
							<img src="<?php echo base_url(); ?>images/profile.png">
							<div class="dropdown-content">
								<p>My Profile</p>
								 <a href="<?php echo base_url(); ?>user/logout">Log out</a>
							</div>
						</div>
					</div>
