		<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script> -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
		<script src="https://code.angularjs.org/1.2.9/angular-sanitize.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-route.js"></script>
        <script src="<?php echo base_url(); ?>js/bootbox.js"></script>
        <script src="<?php echo base_url(); ?>js/exam.js"></script>

        <script src="https://apis.google.com/js/platform.js?onload=onLoad" async defer></script>

         <script type="text/javascript">
        	function signOut() {
			    var auth2 = gapi.auth2.getAuthInstance();
			    auth2.signOut().then(function () {
			        //console.log("signed signOut");
			        window.location.replace("/user/logout");
			    });
			}
			
			function onLoad() {
     			gapi.load('auth2', function() {
        		gapi.auth2.init();
      			});
    		}
        </script>



        <script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-83333307-2', 'auto');
		  ga('send', 'pageview');

		</script>
	</body>
	
</html>