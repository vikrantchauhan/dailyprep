					<div class="container-fluid">
						<h1 class="page-header">Previous Year Papers</h1>

						<div class="row placeholders">
							
							<h2>Practice with Questions from past test papers</h2>
							<div class="col-xs-7 col-sm-3 placeholder" ng-repeat="x in stream">
								<div class="test-stream" style="color: {{x.color_code}}">
					    			<img src="<?php echo base_url(); ?>images/{{x.abbreviation}}_2.png">
					    			<h4>{{x.name}}</h4>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

