<?php //echo "<pre>",print_r($tests);die(); ?>
<div class="container-fluid">
    <div class="page-header">
        <a href="<?php echo base_url(); ?>previous-years/cse"><img src="<?php echo base_url(); ?>images/web-logo.png" style="margin-right: 2%;" id="test-logo"></a>
        <p>Previous Year Papers</p>
        <div class="dropdown" style="float: right;">
        	<span><?php echo $this->session->userdata('userName'); ?></span>
            <img  class="user-img" id="userImage" src="<?php echo base_url(); ?>images/profile.png">
            <div class="dropdown-content" >
                <p>My Profile</p>
                <br/>
                <!-- <a href="<?php echo base_url(); ?>user/logout/">Sign out</a> -->
                <a  onclick="signOut()">Sign out</a>
            </div>
        </div>
    </div>
    <div class=" container placeholders">
        <?php 
            for($i = 0; $i < count($resumable); $i++ ){
        ?>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="dp-ques-year ">
                <div class="question-year">
                    <div class="paper-title">
                        <h4>
                            <?php echo $resumable[$i]['name']; ?>
                        </h4>
                    </div>
                    <div class="syllabus-info">
                        <div class="tooltip">Syllabus Info
                            <span class="tooltiptext"><?php echo $resumable[$i]['exam_category'] ?> Syllabus</span>
                        </div>
                    </div>
                    <div class="paper-details">
                        <ul>
                            <li> 
                                <span><?php echo $resumable[$i]['total_count']; ?></span> 
                                Questions
                            </li>
                            <li> <span>180</span> Minutes </li>
                        </ul>
                    </div>
                    <div>
                        <?php 
                        		if(count($resumable[$i]['test_info']) > 0) { 
                        			if($resumable[$i]['test_info'][0]['test_status'] == 'resumed') {
                        				$redirect_uri = base_url().'previous-years/cse/'.$resumable[$i]['id'].'#/test';
                        				$text = 'Resume';
                        			} else {
                        				$redirect_uri = base_url().'previous-years/cse/'.$resumable[$i]['id'].'#/instruction';
                        				$text = 'Start';
                        			}
                        
                        ?>
                        	<a class="btn btn-primary custom-dp-btn-primary dp-start-test" href="<?php echo $redirect_uri; ?>" id="resume"><?php echo $text; ?></a>
                        <?php }
                            else{ ?>
                        <a class="btn btn-primary custom-dp-btn-primary dp-start-test" href="<?php echo base_url().'previous-years/cse/'.$resumable[$i]['id'].'#/instruction'; ?>">Start</a>
                        <?php }?>
                    </div>
                </div>
            </div>
        </div>
        <?php
            }
        ?>  
        
        <?php 
            for($i = 0; $i < count($remaining); $i++ ){
        ?>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="dp-ques-year ">
                <div class="question-year">
                    <div class="paper-title">
                        <h4>
                            <?php echo $remaining[$i]['name']; ?>
                        </h4>
                    </div>
                    <div class="syllabus-info">
                        <div class="tooltip">Syllabus Info
                            <span class="tooltiptext"><?php echo $remaining[$i]['exam_category'] ?> Syllabus</span>
                        </div>
                    </div>
                    <div class="paper-details">
                        <ul>
                            <li> 
                                <span><?php echo $remaining[$i]['total_count']; ?></span> 
                                Questions
                            </li>
                            <li> <span>180</span> Minutes </li>
                        </ul>
                    </div>
                    <div>
                        <?php 
                        		if(isset($remaining[$i]['test_info']) && count($remaining[$i]['test_info']) > 0) { 
                        			if($remaining[$i]['test_info'][0]['test_status'] == 'resumed') {
                        				$redirect_uri = base_url().'previous-years/cse/'.$remaining[$i]['id'].'#/test';
                        				$text = 'Resume';
                        			} else {
                        				$redirect_uri = base_url().'previous-years/cse/'.$remaining[$i]['id'].'#/instruction';
                        				$text = 'Start';
                        			}
                        
                        ?>
                        	<a class="btn btn-primary custom-dp-btn-primary dp-start-test" href="<?php echo $redirect_uri; ?>" id="resume"><?php echo $text; ?></a>
                        <?php }
                            else{ ?>
                        <a class="btn btn-primary custom-dp-btn-primary dp-start-test" href="<?php echo base_url().'previous-years/cse/'.$remaining[$i]['id'].'#/instruction'; ?>">Start</a>
                        <?php }?>
                    </div>
                </div>
            </div>
        </div>
        <?php
            }
        ?> 
        
        <?php 
            for($i = 0; $i < count($completed); $i++ ){
        ?>
        
        	<div class="col-md-3 col-sm-6 col-xs-12">
        	<div class="solution-body">
                <div class="solution-card">
                    
                    <div class="solution-title">
                        <h4 class="paper-title"><?php echo $completed[$i]['name']; ?></h4>
                    </div>

                    <div class="solution-tags">
                        <span class="paper-tags"><?php echo $completed[$i]['exam_category'] ?> </span>
                    </div>
                    
                    <div class="syllabus-info">
                        
                        <div class="tooltip">Syllabus Info
                            <span class="tooltiptext"><?php echo $completed[$i]['exam_category'] ?> Syllabus</span>
                        </div>
                    
                    </div>

                    <div class="solution-details">
                        <ul>
                            
                            <li>
                                <span class="attempted-ques first-li-span">
                                    <?php echo $completed[$i]['test_info']['attempted']; ?>
                                </span>
                                <span class="total-ques">
                                    /<?php echo $completed[$i]['total_count']; ?>
                                </span>
                                Questions
                            </li>

                            <li>
                                <span class="obtain-marks first-li-span">
                                    <?php echo ($completed[$i]['test_info']['pos_marks'] - $completed[$i]['test_info']['neg_marks']); ?>
                                </span>
                                <span class="total-marks">
                                    /<?php echo ($completed[$i]['test_info']['total_marks']); ?>
                                </span>
                                Marks
                            </li>

                            <li>
                                <span class="attempted-time first-li-span">
                                    180
                                </span>
                                <span class="total-time">
                                    /180
                                </span>
                                Minuets
                            </li>

                        </ul>
                    </div>

                    
                   <div class="solution-analytics-share">
                    
                        <a href="<?php echo base_url().'previous-years/cse/'.$completed[$i]['id'].'#/solutions'; ?>" class="paper-solution btn"  data-toggle="tooltip">
                                
                            <div class="tooltip">
                               <i class="fa fa-book" aria-hidden="true"></i>
                                <span class="tooltiptext">Solution</span>
                            </div>
                        
                        </a>

                        <a href="#" class="paper-analytics btn" class="btn" data-toggle="tooltip">
                                
                            <div class="tooltip">
                                <i class="fa fa-pie-chart" aria-hidden="true"></i>
                                <span class="tooltiptext">Analytics</span>
                            </div>
                        </a>

                        <a href="#" class="paper-share btn" class="btn" data-toggle="tooltip">
                            
                            <div class="tooltip">
                                <i class="fa fa-share-alt" aria-hidden="true"></i>
                                <span class="tooltiptext">Share</span>
                            </div>
                        </a>

                   </div>

                
                </div>
            </div>
            </div>
        <?php
            }
        ?>            
    </div>
</div>