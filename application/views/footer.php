		<footer class="dp-footer" >
			
			
			<div class="container">
				
				<div class="row dp-links">
				
					<div class="col-sm-4">
						<a href="<?php echo base_url();?>landing/contact"><p>Contact Us</p></a>
						<p>Privacy Policy</p>
						<p>Terms & Conditions</p>
					</div>
					<div class="col-sm-3 col-sm-offset-1">
						<p>Syllabus</p>
						<p>Solved Papers</p>
						<p>Unsolved Papers</p>
					</div>
					<div class="col-sm-3  col-sm-offset-1">
						<p>Blog</p>
						<p>Alert & Notifications</p>
					</div>
				</div>
				<div class="row dp-copyright-div">
					<div class="col-md-8">
						<p>Copyright 2015 By Numetric Technologies. All Rights Reserved.</p>
					</div>
					<div class="col-md-4 text-right">
						<img class="dp-social-img" src="<?php echo base_url(); ?>images/facebook.png">
						<img class="dp-social-img" src="<?php echo base_url(); ?>images/twitter.png">
						<img class="dp-social-img" src="<?php echo base_url(); ?>images/instagram.png">
						<img class="dp-social-img" src="<?php echo base_url(); ?>images/youtube.png">
					</div>
				</div>
				
			</div>
		</footer>

		<script type="text/javascript">
			
			//<![CDATA[
			window.fbAsyncInit = function() {
			   FB.init({
			     appId      : '835833723217739', // App ID
			     //appId      :'1246528622032128', //App Id dev
			     channelURL : '', // Channel File, not required so leave empty
			     status     : true, // check login status
			     cookie     : true, // enable cookies to allow the server to access the session
			     oauth      : true, // enable OAuth 2.0
			     xfbml      : false  // parse XFBML
			   });
			};
			
			// logs the user in the application and facebook
			function login(){
			FB.getLoginStatus(function(response){
			     if(response.status === 'connected'){
			            // window.location.href = 'fbconnect.php';
			           // console.log("connected");
			           
			            testAPI();
			     }else{
			        FB.login(function(response) {
			                if(response.authResponse) {
			              //if (response.perms)
			                    //window.location.href = 'fbconnect.php';
			                    //console.log("connected");
			                    testAPI();
			            } else {
			              // user is not logged in
			              FB.login()
			            }
			     },{scope:'email'}); // which data to access from user profile
			 }
			 // statusChangeCallback(response);
			});
			}

			  // Here we run a very simple test of the Graph API after login is
			  // successful.  See statusChangeCallback() for when this call is made.
			  function testAPI() {
    
			    //console.log('Welcome!  Fetching your information.... ');
			    FB.api('/me', {
			        fields: ['id','email', 'name', 'picture']
			    }, function(response) {

			    	var id = response.id;
			        var name = response.name;
			        var email = response.email;
			        var picture = response.picture.data.url;
			        //console.log(picture);
			        //console.log(id);
			        
			        var img_url = "https://graph.facebook.com/" + id + "/picture?type=square"

			        //console.log("dd" + img_url);

			        var dataString = 'user_ID=' + id + '&user_email=' + email + '&user_name=' + name + '&img_url=' + img_url;
			        //alert(dataString);
			        // AJAX code to submit form.
			        $.ajax({
			            type: "POST",
			            url: "/user/socialuserData/facebook",
			            data: dataString,
			            cache: false,
			            success: function(response) {
			            	
			             //alert(response);
			                var obj = JSON.parse(response);
			                if (obj.exam_id == null) {
			                	window.location.replace("/user-register/"+obj.id);
			                	//alert(response);
			                }
			                else{
		                		window.location.replace("/previous-years/cse");
		                	}
			            }
			        });

			    });

			}
			
			// Load the SDK Asynchronously
			(function() {
			   var e = document.createElement('script'); e.async = true;
			   e.src = document.location.protocol + '//connect.facebook.net/en_US/all.js';                
			   document.getElementById('fb-root').appendChild(e);
			}());
			//]]>
	</script>
		
		<script type="text/javascript">
			
			function onSuccess(googleUser) {

		    	var profile = googleUser.getBasicProfile();
		    	var user_ID = profile.getId();
		    	var user_name =profile.getName();
		    	var user_email = profile.getEmail();
		    	var img_url = profile.getImageUrl()
		    	//console.log('Image URL: ' + profile.getImageUrl());

		    	var dataString = 'user_ID=' + user_ID + '&user_email=' + user_email + '&user_name=' + user_name + '&img_url=' + img_url;

		    	//console.log(user_ID, user_name , user_email);

		    	// AJAX code to submit form.
				$.ajax({
					type: "POST",
					url: "/user/socialuserData/google",
					data: dataString,
					cache: false,
					success: function(response){
						//alert(response);
						var obj = JSON.parse(response);
						
						if(obj.exam_id == null){
							//alert(response][0].exam_id);
							//$user_email= jsonresponse[0].email;
							//alert(response[0].exam_id);
							
							window.location.replace("/user-register/"+obj.id);
						}
						else{
							window.location.replace("/previous-years/cse");
						}
					}
				});

			}

			function onFailure(error) {
				alert(error);
			}

			function renderButton() {
			    gapi.signin2.render('gSignIn', {
			        'scope': 'profile email',
			        'width': 240,
			        'height': 50,
			        'longtitle': true,
			        'theme': 'dark',
			        'onsuccess': onSuccess,
			        'onfailure': onFailure
			    });
			}

			// function signOut() {
			//     var auth2 = gapi.auth2.getAuthInstance();
			//     auth2.signOut().then(function () {
			//         $('.userContent').html('');
			//         $('#gSignIn').slideDown('slow');
			//     });
			// }
		</script>
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script src="https://apis.google.com/js/client:platform.js?onload=renderButton" async defer></script>
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-83333307-2', 'auto');
		  ga('send', 'pageview');

		</script>
	</body>
</html>
