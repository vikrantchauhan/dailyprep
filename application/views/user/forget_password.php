<div class="register-box">
<div class="register-logo">
		Password Recovery
</div>

	<div class="register-box-body">
		

		<form action="<?php echo base_url() ; ?>user/resend" method="post">

			
			<div class="form-group has-feedback">
				
				<input type="email" name="email" class="form-control" placeholder="Email">
				
				<span class="text-danger"><?php echo form_error('email'); ?></span>
			</div>

			
			
			<div class="row">
				
                            <button type="submit" class="btn btn-primary btn-block btn-flat reg">Reset Password</button>
			
				<!-- /.col -->
			</div>
			
		</form>
            
            
	</div>
</div>