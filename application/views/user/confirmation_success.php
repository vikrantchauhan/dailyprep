<div class="dp-misc-box">
	<p class="dp-misc-top">
		<img src="<? echo base_url();?>images/correct.png">
	</p>
	<p class="dp-misc-top">
		<span class="dp-misc-text">Congratulations</span>
		<br>
		<span  class="dp-misc-sec-text">Your account has been successfully confirmed. Please Login to continue.</span>
	</p>
	<p class="dp-misc-top">
		<a href="<? echo base_url();?>user/login"  class="dp-misc-btn" type="button">Login</a>
	</p>
</div>
