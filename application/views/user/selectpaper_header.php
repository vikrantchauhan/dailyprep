<!DOCTYPE html>
<html>
<head>
	<title>DialyPrep</title>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/register.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  	<!-- <script type="text/javascript">
	  	$(document).ready(function(){
		    $("select").change(function(){
		        var examId = $(this).val();
		        $('#datebox').val(examId);
		    });
		});
  	</script> -->

  	<script type="text/javascript">

		$(document).ready(function(){
			$("#myModal").on('show.bs.modal', function(event){
		        var button = $(event.relatedTarget);  // Button that triggered the modal
		        var titleData = button.data('title'); // Extract value from data-* attributes
		        var examId = button.data('val')
		        $(this).find('#examIddata').attr("value", examId);
		        $(this).find('.dp-t').text(titleData);

		        $("#sendData").click(function(){
			        
			        var exam_id = examId;
			        var userId = $('#userId').val();

			        //alert(exam_id);
			        //alert(userId);
					//var dataString = '&exam_id' + exam_id + '&userId' + userId ;
					var dataString = '&exam_id=' + exam_id + '&userId=' + userId;
					//alert(dataString);

					$.ajax({
						type:"POST",
						url: '<?php  echo base_url(); ?>/user/showdashboard',
						data : dataString,
						cache: false,
						success: function(response){
							// window.location.replace("/previous-years/cse");
							var obj = JSON.parse(response);
							//alert(obj.abbreviation);
							
								window.location.replace("/previous-years/"+ obj.abbreviation);
							

						}
					});
			    });
		    });
		});

	</script>
    
	<style type="text/css">
		.dp-course-block {
			box-shadow: none;
			width: 200px;
    		font-size: 18px;
		}
		.bs-example .launch-model{
			width: 65%;
		}
		.dp-selectpaper-box .dp-box-title{
			margin-top: 28px;
	    	margin-bottom: 39px;
		}

		.bs-example .col{
			padding-top: 30px;
			padding-bottom: 40px;
		}
		.adjust-size {
			padding-bottom: 23px;
		}
		.stream{
			font-size: 16px;
		}
	</style>

</head>
<body>
	<header class="reg-header">
        <div class="dp-logo">
            <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>images/webscreen-logo.png"></a>
        </div>
        
    </header>

