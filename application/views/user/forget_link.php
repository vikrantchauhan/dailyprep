<div class="register-box">
    <div class="register-logo">
        Email Activation
    </div>

    <div class="register-box-body">


        <form action="<?php echo base_url() ; ?>user/resendEmail" method="post">


            <div class="form-group has-feedback">

                <input type="email" name="email" class="form-control" placeholder="Email">
                <span class="text-danger"><?php echo form_error('email'); ?></span>
            </div>



            <div class="row">

                <button type="submit" class="btn btn-primary btn-block btn-flat reg">Resend Link</button>

            </div>

        </form>


    </div>
</div>