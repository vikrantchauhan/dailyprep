
		<div class="dp-selectpaper-box">

			<h3 class="text-success text-center">You have successfully registered with <strong>DailyPrep</strong></h3>

			
			<div class="dp-form-box">
			<h4 class="text-center dp-box-title">Please select your stream to continue</h4>
				<!-- <form action="/user/showdashboard" method="post" class="text-center">
					
					<select id="dropdown" >
						<option selected disabled>Select Your Stream</option>
						<option value="1">CSE</option>
						<option value="2">ECE</option>
						<option value="3">EE</option>
						<option value="4">ME</option>
					</select>        
				    

					<input type="hidden" name="id", value="<?php echo $user; ?>">
					<input type="hidden" id="datebox" name="options", value="">
				                
				    <button type="submit" class="btn btn-primary btn-block btn-flat reg">Submit</button>
				            
				</form> -->

	<div class="bs-example">
    <!-- Button HTML (to Trigger Modal) -->
    <!-- <input type="button" class="btn btn-lg btn-primary launch-modal" value="Launch Demo Modal"> -->
    <!-- <button class="btn  btn-default launch-modal" >
    	<div class="dp-course-block">
											<img class="dp-block-margin" src="<?php echo base_url(); ?>images/cse.png"/>
											<p class="dp-block-margin">CSE</p>
										</div>	
    </button>

    <button class="btn  btn-default launch-modal" >
    	<div class="dp-course-block">
											<img class="dp-block-margin" src="<?php echo base_url(); ?>images/ece.png"/>
											<p class="dp-block-margin">ECE</p>
										</div>	
    </button> -->
    
    	<div class="container">
    		<div class="row">
    			<div class="col-sm-6 col-md-3 text-center col">
    				
    				<button class="btn  btn-default launch-modal" data-toggle="modal" data-target="#myModal" data-val="1" data-title= "Computer Science and Information Technology" data-backdrop="static" data-keyboard="false">
    					<div class="dp-course-block ">
							<img class="dp-block-margin" src="<?php echo base_url(); ?>images/cse.png"/>
							<p class="dp-block-margin">GATE CSE </p>
							<p class="stream">Computer Science <br>and Information Technology</p>
						</div>	
    				</button>
    			
    			</div>

    			<div class="col-sm-6 col-md-3 text-center col">
    				
    				<button class="btn  btn-default launch-modal" data-toggle="modal" data-target="#myModal" data-val="2" data-backdrop="static" data-title= "Electronics and Communication Engg." data-keyboard="false">
    					<div class="dp-course-block">
							<img class="dp-block-margin" src="<?php echo base_url(); ?>images/ece.png"/>
							<p class="dp-block-margin">GATE ECE </p>
							<p class="stream">Electronics <br>and Communication Engg.</p>
						</div>	
    				</button>
    			
    			</div>

    			<div class="col-sm-6 col-md-3 text-center col">
    				
    				<button class="btn  btn-default launch-modal" data-toggle="modal" data-target="#myModal" data-val="3" data-backdrop="static" data-title= "Electrical Engineering" data-keyboard="false">
    					<div class="dp-course-block adjust-size">
							<img class="dp-block-margin" src="<?php echo base_url(); ?>images/ee.png"/>
							<p class="dp-block-margin">GATE EE </p>
							<p class="stream">Electrical Engineering</p>
						</div>	
    				</button>
    			
    			</div>

    			<div class="col-sm-6 col-md-3 text-center col">
    				
    				<button class="btn  btn-default launch-modal" data-toggle="modal" data-target="#myModal" data-val="4" data-title= "Mechanical Engineering" data-backdrop="static" data-keyboard="false">
    					<div class="dp-course-block adjust-size">
							<img class="dp-block-margin" src="<?php echo base_url(); ?>images/me.png"/>
							<p class="dp-block-margin">GATE ME </p>
							<p class="stream">Mechanical Engineering</p>
						</div>	
    				</button>
    			
    			</div>
    		</div>
    	</div>
    
    <!-- Modal HTML -->
    <div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Confirmation</h4>
                </div>
                <div class="modal-body">
                    <p>Do you want to continue with <span class="dp-t"></span></p>
                    <p class="text-warning"><small>If you don't save, your changes will be lost.</small></p>
                    <input type="hidden" name="userID" id="userId" value="<?php echo $user; ?>">
                    <input type="hidden" name="examId" value="" id="examIddata">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="sendData">Continue</button>
                </div>
            </div>
        </div>
    </div>
</div>  
				
				</div>

			</div>
		</div>
</body>
</html>