<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="google-signin-client_id" content="198181357121-kq325qhivfafe7br1813el9d56moudhb.apps.googleusercontent.com">  <!-- live website app google client id -->

         <!-- <meta name="google-signin-client_id" content="990887233872-rbqr5rurc6bq8ek79t3sud9krjvhji5e.apps.googleusercontent.com"> -->  <!-- devlopment app google client id -->


        
        <title>DialyPrep</title>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/register.css">
                <!-- Facebook Pixel Code -->
        <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
        n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
        document,'script','https://connect.facebook.net/en_US/fbevents.js');

        fbq('init', '176894866070811');
        fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=176894866070811&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Facebook Pixel Code -->
        
</head>
<body>
            
    <header class="reg-header">
        <div class="dp-logo">
            <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>images/webscreen-logo.png"></a>
        </div>
        
        <div class="dp-log-box">
            <a href="<?php echo base_url(); ?>user/login"  class=" <? print_r(($page == 'register') ? 'dp-show' : 'dp-hide') ?>"><button class="dp-button">Login</button></a>
            <a href="<?php echo base_url(); ?>user/register"  class=" <? print_r(($page == 'login') ? 'dp-show' : 'dp-hide') ?>"><button class="dp-button">Sign Up</button></a>
        </div> 
    </header>
