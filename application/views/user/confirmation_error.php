<div class="dp-misc-box">
	<p class="dp-misc-top">
		<img class="dp-misc-img" src="<? echo base_url();?>images/cancel.png">
	</p>
	<p class="dp-misc-top">
		<span class="dp-misc-text">Sorry</span>
		<br>
		<span  class="dp-misc-sec-text">Your link has expired. Resend the confirmation link again.</span>
	</p>
	
	<p class="dp-misc-top">
		<a href="<? echo base_url();?>user/resendConfirmationLink/<? echo $email; ?>"  class="dp-misc-btn" type="button">Resend Link</a>
	</p>
</div>
