
<div class="register-box">

	<div class="register-logo">
		Register
	</div>

	<div class="register-box-body">

		<div id="fb-root"></div>
	    <a href='#' onclick='login();'><button class="dp-fb">Sign in with Facebook</button></a>
	    <div id="status"></div>
	
	    <div id="gSignIn"></div>
		
		<div class="line-separator" data-prompt="OR"></div>
		
		<!-- HTML for displaying user details -->
		<div class="userContent"></div>
		
		<form action="<?php echo base_url(); ?>user/register" method="post" >
		
			<div class="form-group has-feedback">
			
				<input type="text" name="name" class="form-control" placeholder="Name" pattern="[a-zA-Z ]+" oninvalid="setCustomValidity('Please enter valid name')"
    onkeypress="try{setCustomValidity('')}catch(e){}" required >
				<span class="text-danger"><?php echo form_error('name'); ?></span>
			</div>
			
			<div class="form-group has-feedback">
				
				<input type="email" name="email" class="form-control" placeholder="Email"  required>
				<span class="text-danger"><?php echo form_error('email'); ?></span>
			</div>
			
			<div class="form-group has-feedback">
			
				<input type="tel" name="phone" class="form-control" placeholder="Phone Number" pattern= "[7-9]{1}[0-9]{9}"  oninvalid="setCustomValidity('Please enter valid phone number.')"
    onkeypress="try{setCustomValidity('')}catch(e){}" minlength="10" maxlength="10" required>
				<span class="text-danger"><?php echo form_error('phone'); ?></span>
			</div>
			
			<div class="form-group has-feedback">
			
				<input type="password" name="password" class="form-control" placeholder="Password" required>
				<span class="text-danger"><?php echo form_error('password'); ?></span>
			</div>
			
			<div class="form-group has-feedback">
			
				<input type="password" name="passconf" class="form-control" placeholder="Confirm password" required>
				<span class="text-danger"><?php echo form_error('passconf'); ?></span>
			</div>

			<div class="row">
				
                <button type="submit" class="btn btn-primary btn-block btn-flat reg">Register</button>
				<!-- /.col -->
			</div>
		</form>
	</div>
</div>
    
