<div class="register-box">

	<div class="register-logo">
		Login
	</div>
	
	<div id="fb-root"></div>
	
	<a href='#' onclick='login();'><button class="dp-fb">Sign in with Facebook</button></a>
	
	<div id="status"></div>
	
	<div id="gSignIn" ></div>

	<div class="line-separator" data-prompt="OR"></div>

	<div class="register-box-body">
		

		<form action="<?php echo base_url(); ?>user/login" method="post">

			
			<div class="form-group has-feedback">
				
				<input type="email" name="email" class="form-control" placeholder="Email">
				
				<span class="text-danger"><?php echo form_error('email'); ?></span>
			</div>

			
			<div class="form-group has-feedback">
			
				<input type="password" name="password" class="form-control" placeholder="Password">
				
				<span class="text-danger"><?php echo form_error('password'); ?></span>
			</div>


			<div class="row">
				
                            <button type="submit" class="btn btn-primary btn-block btn-flat reg">Login</button>
			
				<!-- /.col -->
			</div>
			
		</form>
            
            
	</div>
	 <?php 	if($error > 0 )	{  ?>
	 <span class="text-danger" style="margin-left: 5%;" >Incorrect username or password</span>
	<?php }?>

    <div class="forget">
        <a href="<?php echo base_url(); ?>user/forget">Forget Password</a>
    </div>
   
</div>
