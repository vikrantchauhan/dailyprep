<div class="register-box">
<div class="register-logo">
		Set New Password
</div>

	<div class="register-box-body">
		

            <form action="<?php echo base_url(); ?>user/updatePassword" method="post">

			
			<div class="form-group has-feedback">
			
				<input type="password" name="password" class="form-control" placeholder="New Password">
				
				<span class="text-danger"><?php echo form_error('password'); ?></span>
			</div>


			<div class="form-group has-feedback">
			
				<input type="password" name="passconf" class="form-control" placeholder="Confirm Password">
				
				<span class="text-danger"><?php echo form_error('passconf'); ?></span>
			</div>

                        <input type="hidden" name="email" value="<?php print_r($user); ?>"/>

				
            		<button type="submit" class="btn btn-primary btn-block btn-flat reg">Submit</button>


			
		</form>
            
            
	</div>
</div>