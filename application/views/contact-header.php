<!DOCTYPE html>
<html lang="en">
<head>
	  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Free GATE Preparation Online | Study Material | Mock Tests | Test Series</title>
    <meta name="description" content="DailyPrep is a free learning app for GATE exam. Get access to study material, solved and unsolved gate papers papers and test series">
    <meta name="keywords" content="GATE, Free Online Test Series, Mock Test, Solved Papers, Unsolved Papers">

    <link rel='stylesheet' href="<?php echo base_url(); ?>css/bootstrap.min.css" type='text/css' media='all' />
     <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <link rel='stylesheet' href="<?php echo base_url(); ?>css/main.css" type='text/css' media='all' />
    

</head>
<body>

    <header class="reg-header">
        <div class="dp-logo">
            <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>images/webscreen-logo.png"></a>
        </div>
        
        <div class="dp-log-box">
            <a href="<?php echo base_url(); ?>user/login"><button class="dp-button">Login</button></a>
            <a href="<?php echo base_url(); ?>user/register"><button class="dp-button">Sign Up</button></a>
        </div> 
    </header>