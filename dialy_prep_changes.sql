/********************** Changes on 19-september-2016 **********************************************/
ALTER TABLE  `user_test_report` ADD  `current_question_id` INT( 11 ) NOT NULL AFTER  `test_status` ;

/********************** Changes on 22-september-2016 **********************************************/
ALTER TABLE  `user_test_details` ADD  `is_marked` INT( 1 ) NOT NULL DEFAULT  '0' AFTER  `is_Correct`


/********************** Changes on 27-september-2016 **********************************************/
alter table user add column image varchar(255);
alter table tests add column specific_instructions text;


/********************** Changes on 27-september-2016 **********************************************/
ALTER TABLE  `test_info` ADD  `positive_mark` DOUBLE NOT NULL DEFAULT  '00.00' AFTER  `question` ;
ALTER TABLE  `test_info` ADD  `negative_mark` DOUBLE NOT NULL DEFAULT  '00.00' AFTER  `positive_mark` ;

/********************** Changes on 28-september-2016 **********************************************/
ALTER TABLE  `user` CHANGE  `image`  `image` VARCHAR( 1024 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'profile.png';

/********************** Changes on 29-september-2016 **********************************************/
ALTER TABLE  `tests` ADD  `total_time_duration` INT( 11 ) NOT NULL AFTER  `total_count` ;
UPDATE `tests` SET `total_time_duration`='10800';
ALTER TABLE  `user_test_details` CHANGE  `time_taken`  `time_taken` INT( 11 ) NULL DEFAULT  '0';
UPDATE `user_test_details` SET `time_taken`='0';

/********************** Changes on 03-October-2016 **********************************************/
CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phoneno` varchar(50) NOT NULL,
  `message` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */; 
