<div class="page-header">
	<a href="/previous-years/cse"><img src="/images/web-logo.png" style="margin-right: 2%;" id="test-logo"></a>
    <p>{{test_name}}</p>
</div>
<div class="row" id="analysis-page">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading overall-analytics-head">
                    <h3 class="panel-title">Overall Analytics</h3>
                    <div class="analysis-nav hidden-xs">
                        <!-- ngIf: test.sections.length > 1 -->
                        <!-- ngIf: tagAnalysisArr && tagAnalysisArr.length --><button type="button" class="btn btn-sm text-uppercase pull-left js-chapter-level ng-scope" ng-if="tagAnalysisArr &amp;&amp; tagAnalysisArr.length">Chapter Level</button><!-- end ngIf: tagAnalysisArr && tagAnalysisArr.length -->
                        <!-- ngIf: (topEasy && topEasy.length > 0) || (topDifficult && topDifficult.length > 0) || (topTricky && topTricky.length > 0) -->
                    </div>
                </div>
                <div class="panel-body">
                    <p class="text-center overall-rank ng-binding">Dev, your rank is <span class="ng-binding">146</span> among <span class="ng-binding">370</span> students</p>
                    <div class="overall-analytics">
                        <div class="row">
                            <div class="col-xs-6 col-sm-3 pad-sm-r8 overall-analytics-each">
                                <div class="inner">
                                    <canvas id="oa-score" width="140" height="140"></canvas>
                                    <div class="text ng-binding">
                                        <span id="oa-score-text">3.35</span>/16
                                        <div>Marks</div>
                                    </div>
                                </div>
                                <div class="rate js-rate-4" ng-class="getRateStringClass(myAnalysis[0].marks, averageMarks, analysis.maxMarks)">
                                    <i class="tb-icon tb-thumb-up"></i>
                                    <i class="tb-icon tb-thumb-down"></i>
                                    <span class="rate-1">Poor</span>
                                    <span class="rate-2">Below Average</span>
                                    <span class="rate-3">Average</span>
                                    <span class="rate-4">Good</span>
                                    <span class="rate-5">Excellent</span>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-3 pad-sm-h8 overall-analytics-each">
                                <div class="inner">
                                    <canvas id="oa-percentile" width="140" height="140"></canvas>
                                    <div class="text">
                                        <span id="oa-percentile-text">60.81</span>%
                                        <div>Percentile</div>
                                    </div>
                                </div>
                                <div class="rate js-rate-4" ng-class="getRateStringClass(myPercentile, 50, 100)">
                                    <i class="tb-icon tb-thumb-up"></i>
                                    <i class="tb-icon tb-thumb-down"></i>
                                    <span class="rate-1">Poor</span>
                                    <span class="rate-2">Below Average</span>
                                    <span class="rate-3">Average</span>
                                    <span class="rate-4">Good</span>
                                    <span class="rate-5">Excellent</span>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-3 pad-sm-h8 overall-analytics-each">
                                <div class="inner">
                                    <canvas id="oa-accuracy" width="140" height="140"></canvas>
                                    <div class="text">
                                        <span id="oa-accuracy-text">42.86</span>%
                                        <div>Accuracy</div>
                                    </div>
                                </div>
                                <div class="rate js-rate-2" ng-class="getRateStringClass(accuracyStudent, overallAverageAccuracy, 100)">
                                    <i class="tb-icon tb-thumb-up"></i>
                                    <i class="tb-icon tb-thumb-down"></i>
                                    <span class="rate-1">Poor</span>
                                    <span class="rate-2">Below Average</span>
                                    <span class="rate-3">Average</span>
                                    <span class="rate-4">Good</span>
                                    <span class="rate-5">Excellent</span>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-3 pad-sm-l8 overall-analytics-each">
                                <div class="inner">
                                    <canvas id="oa-attempt" width="140" height="140"></canvas>
                                    <div class="text ng-binding">
                                        <span id="oa-attempt-text">7</span>/10
                                        <div>Attempt</div>
                                    </div>
                                </div>
                                <div class="rate js-rate-5" ng-class="getRateStringClass(attemptStudent, overallAverageAttempt, 100)">
                                    <i class="tb-icon tb-thumb-up"></i>
                                    <i class="tb-icon tb-thumb-down"></i>
                                    <span class="rate-1">Poor</span>
                                    <span class="rate-2">Below Average</span>
                                    <span class="rate-3">Average</span>
                                    <span class="rate-4">Good</span>
                                    <span class="rate-5">Excellent</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="row">
                <div class="col-xs-12 col-md-6 pad-md-r8">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Question Distribution</h3>
                        </div>
                        <div class="panel-body">
                            <div id="overall-analytics-question" style="min-height: 400px;"><div style="position: relative;"><div dir="ltr" style="position: relative; width: 569px; height: 400px;"><div aria-label="A chart." style="position: absolute; left: 0px; top: 0px; width: 100%; height: 100%;"><svg width="569" height="400" aria-label="A chart." style="overflow: hidden;"><defs id="defs"></defs><rect x="0" y="0" width="569" height="400" stroke="none" stroke-width="0" fill="#ffffff"></rect><g><rect x="157" y="347" width="254" height="14" stroke="none" stroke-width="0" fill-opacity="0" fill="#ffffff"></rect><g><rect x="157" y="347" width="65" height="14" stroke="none" stroke-width="0" fill-opacity="0" fill="#ffffff"></rect><g><text text-anchor="start" x="176" y="358.9" font-family="Arial" font-size="14" stroke="none" stroke-width="0" fill="#aab8c2">Correct</text></g><circle cx="164" cy="354" r="7" stroke="none" stroke-width="0" fill="#2cbf5d"></circle></g><g><rect x="245" y="347" width="73" height="14" stroke="none" stroke-width="0" fill-opacity="0" fill="#ffffff"></rect><g><text text-anchor="start" x="264" y="358.9" font-family="Arial" font-size="14" stroke="none" stroke-width="0" fill="#aab8c2">Incorrect</text></g><circle cx="252" cy="354" r="7" stroke="none" stroke-width="0" fill="#fe3652"></circle></g><g><rect x="341" y="347" width="70" height="14" stroke="none" stroke-width="0" fill-opacity="0" fill="#ffffff"></rect><g><text text-anchor="start" x="360" y="358.9" font-family="Arial" font-size="14" stroke="none" stroke-width="0" fill="#aab8c2">Skipped</text></g><circle cx="348" cy="354" r="7" stroke="none" stroke-width="0" fill="#a7a7a7"></circle></g></g><g><path d="M285,116.35L285,61A123,123,0,0,1,401.97995150430387,222.00909030811852L349.33897332736717,204.90499966946518A67.65,67.65,0,0,0,285,116.35" stroke="#ffffff" stroke-width="1" fill="#2cbf5d"></path><text text-anchor="start" x="366.1284260451657" y="126.71384940003055" font-family="Arial" font-size="12" stroke="none" stroke-width="0" fill="#000000">3</text></g><g><path d="M220.6610266726329,204.90499966946524L168.02004849569613,222.0090903081186A123,123,0,0,1,285,61L285,116.35A67.65,67.65,0,0,0,220.6610266726329,204.90499966946524" stroke="#ffffff" stroke-width="1" fill="#a7a7a7"></path><text text-anchor="start" x="196.87157395483428" y="126.71384940003058" font-family="Arial" font-size="12" stroke="none" stroke-width="0" fill="#000000">3</text></g><g><path d="M349.33897332736717,204.90499966946518L401.97995150430387,222.00909030811852A123,123,0,0,1,168.02004849569613,222.0090903081186L220.6610266726329,204.90499966946524A67.65,67.65,0,0,0,349.33897332736717,204.90499966946518" stroke="#ffffff" stroke-width="1" fill="#fe3652"></path><text text-anchor="start" x="281.5" y="293.14480609744646" font-family="Arial" font-size="12" stroke="none" stroke-width="0" fill="#000000">4</text></g><g></g></svg><div aria-label="A tabular representation of the data in the chart." style="position: absolute; left: -10000px; top: auto; width: 1px; height: 1px; overflow: hidden;"><table><thead><tr><th>Task</th><th>questions analysis</th></tr></thead><tbody><tr><td>Correct</td><td>3</td></tr><tr><td>Incorrect</td><td>4</td></tr><tr><td>Skipped</td><td>3</td></tr></tbody></table></div></div></div><div aria-hidden="true" style="display: none; position: absolute; top: 410px; left: 579px; white-space: nowrap; font-family: Arial; font-size: 14px;">Skipped</div><div></div></div></div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 pad-md-l8">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Time Distribution (in minutes)</h3>
                        </div>
                        <div class="panel-body">
                            <div id="overall-analytics-time" style="min-height: 400px;"><div style="position: relative;"><div dir="ltr" style="position: relative; width: 569px; height: 400px;"><div aria-label="A chart." style="position: absolute; left: 0px; top: 0px; width: 100%; height: 100%;"><svg width="569" height="400" aria-label="A chart." style="overflow: hidden;"><defs id="defs"></defs><rect x="0" y="0" width="569" height="400" stroke="none" stroke-width="0" fill="#ffffff"></rect><g><rect x="157" y="347" width="254" height="14" stroke="none" stroke-width="0" fill-opacity="0" fill="#ffffff"></rect><g><rect x="157" y="347" width="65" height="14" stroke="none" stroke-width="0" fill-opacity="0" fill="#ffffff"></rect><g><text text-anchor="start" x="176" y="358.9" font-family="Arial" font-size="14" stroke="none" stroke-width="0" fill="#aab8c2">Correct</text></g><circle cx="164" cy="354" r="7" stroke="none" stroke-width="0" fill="#2cbf5d"></circle></g><g><rect x="245" y="347" width="73" height="14" stroke="none" stroke-width="0" fill-opacity="0" fill="#ffffff"></rect><g><text text-anchor="start" x="264" y="358.9" font-family="Arial" font-size="14" stroke="none" stroke-width="0" fill="#aab8c2">Incorrect</text></g><circle cx="252" cy="354" r="7" stroke="none" stroke-width="0" fill="#fe3652"></circle></g><g><rect x="341" y="347" width="70" height="14" stroke="none" stroke-width="0" fill-opacity="0" fill="#ffffff"></rect><g><text text-anchor="start" x="360" y="358.9" font-family="Arial" font-size="14" stroke="none" stroke-width="0" fill="#aab8c2">Skipped</text></g><circle cx="348" cy="354" r="7" stroke="none" stroke-width="0" fill="#a7a7a7"></circle></g></g><g><path d="M285,116.35L285,61A123,123,0,0,1,405.31215489025806,158.4268620294156L351.17168518964195,169.93477411617857A67.65,67.65,0,0,0,285,116.35" stroke="#ffffff" stroke-width="1" fill="#2cbf5d"></path><text text-anchor="start" x="335.79005758504206" y="111.27818500782693" font-family="Arial" font-size="12" stroke="none" stroke-width="0" fill="#000000">0.13</text></g><g><path d="M239.73331447982318,133.72625255645434L202.69693541786035,92.59318646628061A123,123,0,0,1,284.9999999999999,61L284.99999999999994,116.35A67.65,67.65,0,0,0,239.73331447982318,133.72625255645434" stroke="#ffffff" stroke-width="1" fill="#a7a7a7"></path><text text-anchor="start" x="237.32464731402538" y="93.95998430632639" font-family="Arial" font-size="12" stroke="none" stroke-width="0" fill="#000000">0.07</text></g><g><path d="M351.17168518964195,169.93477411617857L405.31215489025806,158.4268620294156A123,123,0,1,1,202.6969354178604,92.59318646628054L239.7333144798232,133.72625255645428A67.65,67.65,0,1,0,351.17168518964195,169.93477411617857" stroke="#ffffff" stroke-width="1" fill="#fe3652"></path><text text-anchor="start" x="244.82821399000989" y="285.67573439588415" font-family="Arial" font-size="12" stroke="none" stroke-width="0" fill="#000000">0.4</text></g><g></g></svg><div aria-label="A tabular representation of the data in the chart." style="position: absolute; left: -10000px; top: auto; width: 1px; height: 1px; overflow: hidden;"><table><thead><tr><th>Task</th><th>questions analysis</th></tr></thead><tbody><tr><td>Correct</td><td>0.13</td></tr><tr><td>Incorrect</td><td>0.4</td></tr><tr><td>Skipped</td><td>0.07</td></tr></tbody></table></div></div></div><div aria-hidden="true" style="display: none; position: absolute; top: 410px; left: 579px; white-space: nowrap; font-family: Arial; font-size: 14px;">Skipped</div><div></div></div></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="ana-right-box" id="sectional-level">
                <div class="row">
                    <div class="col-xs-12 ng-hide" ng-show="test.sections.length > 1">
                        <div class="row">
                            <div class="col-xs-12 col-lg-6 pad-lg-r8">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Marks Breakdown by Section</h3>
                                        <div class="compare-toggle">
                                            <p>Compare</p>
                                            <div class="tb-toggle">
                                                <input ng-click="toggleScoreBreakdownComparison(isShowScoreBreakdown)" ng-model="isShowScoreBreakdown" type="checkbox" id="toggle-score-breakdown" class="ng-pristine ng-untouched ng-valid">
                                                <label for="toggle-score-breakdown"></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <div id="score-breakdown"><div style="position: relative;"><div dir="ltr" style="position: relative; width: 400px; height: 100px;"><div aria-label="A chart." style="position: absolute; left: 0px; top: 0px; width: 100%; height: 100%;"><svg width="400" height="100" aria-label="A chart." style="overflow: hidden;"><defs id="defs"><clipPath id="_ABSTRACT_RENDERER_ID_0"><rect x="80" y="10" width="300" height="50"></rect></clipPath></defs><rect x="0" y="0" width="400" height="100" stroke="none" stroke-width="0" fill="#ffffff"></rect><g><rect x="215" y="83" width="24" height="12" stroke="none" stroke-width="0" fill-opacity="0" fill="#ffffff"></rect><g><rect x="215" y="83" width="24" height="12" stroke="none" stroke-width="0" fill-opacity="0" fill="#ffffff"></rect><g><text text-anchor="start" x="244" y="93.2" font-family="Arial" font-size="12" stroke="none" stroke-width="0" fill="#222222">Marks</text></g><rect x="215" y="83" width="24" height="12" stroke="none" stroke-width="0" fill="#eabb39"></rect></g></g><g><rect x="80" y="10" width="300" height="50" stroke="none" stroke-width="0" fill-opacity="0" fill="#ffffff"></rect><g clip-path="url(https://testbook.com/gate-ec/tests/5732cd51995a2d5236608e5e/analysis#_ABSTRACT_RENDERER_ID_0)"><g><rect x="80" y="10" width="1" height="50" stroke="none" stroke-width="0" fill="#cccccc"></rect><rect x="155" y="10" width="1" height="50" stroke="none" stroke-width="0" fill="#cccccc"></rect><rect x="230" y="10" width="1" height="50" stroke="none" stroke-width="0" fill="#cccccc"></rect><rect x="304" y="10" width="1" height="50" stroke="none" stroke-width="0" fill="#cccccc"></rect><rect x="379" y="10" width="1" height="50" stroke="none" stroke-width="0" fill="#cccccc"></rect></g><g><rect x="81" y="20" width="124" height="30" stroke="none" stroke-width="0" fill="#eabb39"></rect></g><g><rect x="80" y="10" width="1" height="50" stroke="none" stroke-width="0" fill="#333333"></rect></g><g><rect x="205" y="35" width="0" height="1" stroke="none" stroke-width="0" fill="#999999"></rect></g></g><g></g><g><g><text text-anchor="end" x="68" y="39.2" font-family="Arial" font-size="12" stroke="none" stroke-width="0" fill="#222222">Chapter Test</text></g></g><g><g><g><text text-anchor="end" x="201" y="38.15" font-family="Arial" font-size="9" stroke="none" stroke-width="0" fill="#404040">3.35</text></g></g></g></g><g><g><text text-anchor="middle" x="230" y="75.53333333333333" font-family="Arial" font-size="12" font-style="italic" stroke="none" stroke-width="0" fill="#222222">Marks</text><rect x="80" y="65.33333333333333" width="300" height="12" stroke="none" stroke-width="0" fill-opacity="0" fill="#ffffff"></rect></g></g><g></g></svg><div aria-label="A tabular representation of the data in the chart." style="position: absolute; left: -10000px; top: auto; width: 1px; height: 1px; overflow: hidden;"><table><thead><tr><th>Sections</th><th>Marks</th></tr></thead><tbody><tr><td>Chapter Test</td><td>3.35</td></tr></tbody></table></div></div></div><div aria-hidden="true" style="display: none; position: absolute; top: 110px; left: 410px; white-space: nowrap; font-family: Arial; font-size: 12px;">Marks</div><div></div></div></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-lg-6 pad-lg-l8">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Accuracy Breakdown by Section</h3>
                                        <div class="compare-toggle">
                                            <p>Compare</p>
                                            <div class="tb-toggle">
                                                <input ng-click="drawAccuracyBreakdownComparison(isShowAccuracyBreakdown)" ng-model="isShowAccuracyBreakdown" type="checkbox" id="toggle-accuracy-breakdown" class="ng-pristine ng-untouched ng-valid">
                                                <label for="toggle-accuracy-breakdown"></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <div id="accuracy-breakdown"><div style="position: relative;"><div dir="ltr" style="position: relative; width: 400px; height: 100px;"><div aria-label="A chart." style="position: absolute; left: 0px; top: 0px; width: 100%; height: 100%;"><svg width="400" height="100" aria-label="A chart." style="overflow: hidden;"><defs id="defs"><clipPath id="_ABSTRACT_RENDERER_ID_1"><rect x="80" y="10" width="300" height="50"></rect></clipPath></defs><rect x="0" y="0" width="400" height="100" stroke="none" stroke-width="0" fill="#ffffff"></rect><g><rect x="215" y="83" width="24" height="12" stroke="none" stroke-width="0" fill-opacity="0" fill="#ffffff"></rect><g><rect x="215" y="83" width="24" height="12" stroke="none" stroke-width="0" fill-opacity="0" fill="#ffffff"></rect><g><text text-anchor="start" x="244" y="93.2" font-family="Arial" font-size="12" stroke="none" stroke-width="0" fill="#222222">Accuracy</text></g><rect x="215" y="83" width="24" height="12" stroke="none" stroke-width="0" fill="#eabb39"></rect></g></g><g><rect x="80" y="10" width="300" height="50" stroke="none" stroke-width="0" fill-opacity="0" fill="#ffffff"></rect><g clip-path="url(https://testbook.com/gate-ec/tests/5732cd51995a2d5236608e5e/analysis#_ABSTRACT_RENDERER_ID_1)"><g><rect x="80" y="10" width="1" height="50" stroke="none" stroke-width="0" fill="#cccccc"></rect><rect x="155" y="10" width="1" height="50" stroke="none" stroke-width="0" fill="#cccccc"></rect><rect x="230" y="10" width="1" height="50" stroke="none" stroke-width="0" fill="#cccccc"></rect><rect x="304" y="10" width="1" height="50" stroke="none" stroke-width="0" fill="#cccccc"></rect><rect x="379" y="10" width="1" height="50" stroke="none" stroke-width="0" fill="#cccccc"></rect></g><g><rect x="81" y="20" width="127" height="30" stroke="none" stroke-width="0" fill="#eabb39"></rect></g><g><rect x="80" y="10" width="1" height="50" stroke="none" stroke-width="0" fill="#333333"></rect></g><g><rect x="208" y="35" width="0" height="1" stroke="none" stroke-width="0" fill="#999999"></rect></g></g><g></g><g><g><text text-anchor="end" x="68" y="39.2" font-family="Arial" font-size="12" stroke="none" stroke-width="0" fill="#222222">Chapter Test</text></g></g><g><g><g><text text-anchor="end" x="204" y="38.15" font-family="Arial" font-size="9" stroke="none" stroke-width="0" fill="#404040">42.86%</text></g></g></g></g><g><g><text text-anchor="middle" x="230" y="75.53333333333333" font-family="Arial" font-size="12" font-style="italic" stroke="none" stroke-width="0" fill="#222222">Accuracy</text><rect x="80" y="65.33333333333333" width="300" height="12" stroke="none" stroke-width="0" fill-opacity="0" fill="#ffffff"></rect></g></g><g></g></svg><div aria-label="A tabular representation of the data in the chart." style="position: absolute; left: -10000px; top: auto; width: 1px; height: 1px; overflow: hidden;"><table><thead><tr><th>Sections</th><th>Accuracy</th></tr></thead><tbody><tr><td>Chapter Test</td><td>42.86</td></tr></tbody></table></div></div></div><div aria-hidden="true" style="display: none; position: absolute; top: 110px; left: 410px; white-space: nowrap; font-family: Arial; font-size: 12px;">Accuracy</div><div></div></div></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 ng-hide" ng-show="test.sections.length > 1">
                        <div class="row">
                            <div class="col-xs-12 col-lg-6 pad-lg-r8">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Questions Breakdown</h3>
                                        <div class="compare-toggle">
                                            <p>Compare</p>
                                            <div class="tb-toggle">
                                                <input ng-click="drawQuestionBreakdownComparison(isShowQuestionBreakdown)" ng-model="isShowQuestionBreakdown" type="checkbox" id="toggle-questions-breakdown" class="ng-pristine ng-untouched ng-valid">
                                                <label for="toggle-questions-breakdown"></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <div id="question-breakdown"><div style="position: relative;"><div dir="ltr" style="position: relative; width: 400px; height: 160px;"><div aria-label="A chart." style="position: absolute; left: 0px; top: 0px; width: 100%; height: 100%;"><svg width="400" height="160" aria-label="A chart." style="overflow: hidden;"><defs id="defs"><clipPath id="_ABSTRACT_RENDERER_ID_2"><rect x="80" y="13" width="300" height="104"></rect></clipPath></defs><rect x="0" y="0" width="400" height="160" stroke="none" stroke-width="0" fill="#ffffff"></rect><g><rect x="167" y="142" width="120" height="12" stroke="none" stroke-width="0" fill-opacity="0" fill="#ffffff"></rect><g><rect x="167" y="142" width="24" height="12" stroke="none" stroke-width="0" fill-opacity="0" fill="#ffffff"></rect><g><text text-anchor="start" x="196" y="152.2" font-family="Arial" font-size="12" stroke="none" stroke-width="0" fill="#222222">Incorrect</text></g><rect x="167" y="142" width="24" height="12" stroke="none" stroke-width="0" fill="#fe9aa8"></rect></g><g><rect x="215" y="142" width="24" height="12" stroke="none" stroke-width="0" fill-opacity="0" fill="#ffffff"></rect><g><text text-anchor="start" x="244" y="152.2" font-family="Arial" font-size="12" stroke="none" stroke-width="0" fill="#222222">Correct</text></g><rect x="215" y="142" width="24" height="12" stroke="none" stroke-width="0" fill="#2cbf5d"></rect></g><g><rect x="263" y="142" width="24" height="12" stroke="none" stroke-width="0" fill-opacity="0" fill="#ffffff"></rect><g><text text-anchor="start" x="292" y="152.2" font-family="Arial" font-size="12" stroke="none" stroke-width="0" fill="#222222">Skipped</text></g><rect x="263" y="142" width="24" height="12" stroke="none" stroke-width="0" fill="#c4c4c4"></rect></g></g><g><rect x="80" y="13" width="300" height="104" stroke="none" stroke-width="0" fill-opacity="0" fill="#ffffff"></rect><g clip-path="url(https://testbook.com/gate-ec/tests/5732cd51995a2d5236608e5e/analysis#_ABSTRACT_RENDERER_ID_2)"><g><rect x="80" y="13" width="1" height="104" stroke="none" stroke-width="0" fill="#cccccc"></rect><rect x="155" y="13" width="1" height="104" stroke="none" stroke-width="0" fill="#cccccc"></rect><rect x="230" y="13" width="1" height="104" stroke="none" stroke-width="0" fill="#cccccc"></rect><rect x="304" y="13" width="1" height="104" stroke="none" stroke-width="0" fill="#cccccc"></rect><rect x="379" y="13" width="1" height="104" stroke="none" stroke-width="0" fill="#cccccc"></rect></g><g><rect x="-293" y="29" width="597" height="23" stroke="none" stroke-width="0" fill="#fe9aa8"></rect><rect x="-293" y="53" width="448" height="24" stroke="none" stroke-width="0" fill="#2cbf5d"></rect><rect x="-293" y="78" width="448" height="23" stroke="none" stroke-width="0" fill="#c4c4c4"></rect></g><g><rect x="304" y="40" width="0" height="1" stroke="none" stroke-width="0" fill="#999999"></rect><rect x="155" y="65" width="0" height="1" stroke="none" stroke-width="0" fill="#999999"></rect><rect x="155" y="89" width="0" height="1" stroke="none" stroke-width="0" fill="#999999"></rect></g></g><g></g><g><g><text text-anchor="end" x="68" y="69.2" font-family="Arial" font-size="12" stroke="none" stroke-width="0" fill="#222222">Chapter Test</text></g></g><g><g><g><text text-anchor="end" x="300" y="43.15" font-family="Arial" font-size="9" stroke="none" stroke-width="0" fill="#404040">4</text></g></g><g><g><text text-anchor="end" x="151" y="68.15" font-family="Arial" font-size="9" stroke="none" stroke-width="0" fill="#ffffff">3</text></g></g><g><g><text text-anchor="end" x="151" y="92.15" font-family="Arial" font-size="9" stroke="none" stroke-width="0" fill="#404040">3</text></g></g></g></g><g><g><text text-anchor="middle" x="230" y="133.53333333333333" font-family="Arial" font-size="12" font-style="italic" stroke="none" stroke-width="0" fill="#222222">Number of Questions</text><rect x="80" y="123.33333333333334" width="300" height="12" stroke="none" stroke-width="0" fill-opacity="0" fill="#ffffff"></rect></g></g><g></g></svg><div aria-label="A tabular representation of the data in the chart." style="position: absolute; left: -10000px; top: auto; width: 1px; height: 1px; overflow: hidden;"><table><thead><tr><th>Sections</th><th>Incorrect</th><th>Correct</th><th>Skipped</th></tr></thead><tbody><tr><td>Chapter Test</td><td>4</td><td>3</td><td>3</td></tr></tbody></table></div></div></div><div aria-hidden="true" style="display: none; position: absolute; top: 170px; left: 410px; white-space: nowrap; font-family: Arial; font-size: 12px;">Skipped</div><div></div></div></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-lg-6 pad-lg-l8">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Time Breakdown</h3>
                                        <div class="compare-toggle">
                                            <p>Compare</p>
                                            <div class="tb-toggle">
                                                <input ng-click="drawTimeBreakdownComparison(isShowTimeBreakdown)" ng-model="isShowTimeBreakdown" type="checkbox" id="toggle-time-breakdown" class="ng-pristine ng-untouched ng-valid">
                                                <label for="toggle-time-breakdown"></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <div id="time-breakdown"><div style="position: relative;"><div dir="ltr" style="position: relative; width: 400px; height: 160px;"><div aria-label="A chart." style="position: absolute; left: 0px; top: 0px; width: 100%; height: 100%;"><svg width="400" height="160" aria-label="A chart." style="overflow: hidden;"><defs id="defs"><clipPath id="_ABSTRACT_RENDERER_ID_3"><rect x="80" y="13" width="300" height="104"></rect></clipPath></defs><rect x="0" y="0" width="400" height="160" stroke="none" stroke-width="0" fill="#ffffff"></rect><g><rect x="167" y="142" width="120" height="12" stroke="none" stroke-width="0" fill-opacity="0" fill="#ffffff"></rect><g><rect x="167" y="142" width="24" height="12" stroke="none" stroke-width="0" fill-opacity="0" fill="#ffffff"></rect><g><text text-anchor="start" x="196" y="152.2" font-family="Arial" font-size="12" stroke="none" stroke-width="0" fill="#222222">Incorrect</text></g><rect x="167" y="142" width="24" height="12" stroke="none" stroke-width="0" fill="#fe9aa8"></rect></g><g><rect x="215" y="142" width="24" height="12" stroke="none" stroke-width="0" fill-opacity="0" fill="#ffffff"></rect><g><text text-anchor="start" x="244" y="152.2" font-family="Arial" font-size="12" stroke="none" stroke-width="0" fill="#222222">Correct</text></g><rect x="215" y="142" width="24" height="12" stroke="none" stroke-width="0" fill="#2cbf5d"></rect></g><g><rect x="263" y="142" width="24" height="12" stroke="none" stroke-width="0" fill-opacity="0" fill="#ffffff"></rect><g><text text-anchor="start" x="292" y="152.2" font-family="Arial" font-size="12" stroke="none" stroke-width="0" fill="#222222">Skipped</text></g><rect x="263" y="142" width="24" height="12" stroke="none" stroke-width="0" fill="#c4c4c4"></rect></g></g><g><rect x="80" y="13" width="300" height="104" stroke="none" stroke-width="0" fill-opacity="0" fill="#ffffff"></rect><g clip-path="url(https://testbook.com/gate-ec/tests/5732cd51995a2d5236608e5e/analysis#_ABSTRACT_RENDERER_ID_3)"><g><rect x="80" y="13" width="1" height="104" stroke="none" stroke-width="0" fill="#cccccc"></rect><rect x="155" y="13" width="1" height="104" stroke="none" stroke-width="0" fill="#cccccc"></rect><rect x="230" y="13" width="1" height="104" stroke="none" stroke-width="0" fill="#cccccc"></rect><rect x="304" y="13" width="1" height="104" stroke="none" stroke-width="0" fill="#cccccc"></rect><rect x="379" y="13" width="1" height="104" stroke="none" stroke-width="0" fill="#cccccc"></rect></g><g><rect x="81" y="29" width="298" height="23" stroke="none" stroke-width="0" fill="#fe9aa8"></rect><rect x="81" y="53" width="96" height="24" stroke="none" stroke-width="0" fill="#2cbf5d"></rect><rect x="81" y="78" width="51" height="23" stroke="none" stroke-width="0" fill="#c4c4c4"></rect></g><g><rect x="80" y="13" width="1" height="104" stroke="none" stroke-width="0" fill="#333333"></rect></g><g><rect x="379" y="40" width="0" height="1" stroke="none" stroke-width="0" fill="#999999"></rect><rect x="177" y="65" width="0" height="1" stroke="none" stroke-width="0" fill="#999999"></rect><rect x="132" y="89" width="0" height="1" stroke="none" stroke-width="0" fill="#999999"></rect></g></g><g></g><g><g><text text-anchor="end" x="68" y="69.2" font-family="Arial" font-size="12" stroke="none" stroke-width="0" fill="#222222">Chapter Test</text></g></g><g><g><g><text text-anchor="end" x="375" y="43.15" font-family="Arial" font-size="9" stroke="none" stroke-width="0" fill="#404040">0.4</text></g></g><g><g><text text-anchor="end" x="173" y="68.15" font-family="Arial" font-size="9" stroke="none" stroke-width="0" fill="#ffffff">0.13</text></g></g><g><g><text text-anchor="end" x="128" y="92.15" font-family="Arial" font-size="9" stroke="none" stroke-width="0" fill="#404040">0.07</text></g></g></g></g><g><g><text text-anchor="middle" x="230" y="133.53333333333333" font-family="Arial" font-size="12" font-style="italic" stroke="none" stroke-width="0" fill="#222222">Time (in minutes)</text><rect x="80" y="123.33333333333334" width="300" height="12" stroke="none" stroke-width="0" fill-opacity="0" fill="#ffffff"></rect></g></g><g></g></svg><div aria-label="A tabular representation of the data in the chart." style="position: absolute; left: -10000px; top: auto; width: 1px; height: 1px; overflow: hidden;"><table><thead><tr><th>Sections</th><th>Incorrect</th><th>Correct</th><th>Skipped</th></tr></thead><tbody><tr><td>Chapter Test</td><td>0.4</td><td>0.13</td><td>0.07</td></tr></tbody></table></div></div></div><div aria-hidden="true" style="display: none; position: absolute; top: 170px; left: 410px; white-space: nowrap; font-family: Arial; font-size: 12px;">Skipped</div><div></div></div></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ngIf: tagAnalysisArr && tagAnalysisArr.length --><div class="col-xs-12 ng-scope" id="chapter-level" ng-if="tagAnalysisArr &amp;&amp; tagAnalysisArr.length">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Chapter Breakdown</h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <ul class="chapter-name-list list-unstyled">
                                            <!-- ngRepeat: t in tagAnalysisArr --><li ng-repeat="t in tagAnalysisArr" class="ng-scope">
                                                <div class="row">
                                                    <div class="col-xs-6 text text-ellipsis ng-binding"><span class="ng-binding">1.</span>Linear Algebra</div>
                                                    <div class="col-xs-6 text-1 text-right ng-binding">Accuracy 42.86%</div>
                                                    <div class="col-xs-12">
                                                        <ul class="chapter-score-list list-unstyled">
                                                            <li>Correct <span class="ng-binding">3</span></li>
                                                            <li>Incorrect <span class="ng-binding">4</span></li>
                                                            <li>Total <span class="ng-binding">10</span></li>
                                                            <li>Time <span class="ng-binding">0.6 mins</span></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <div class="range">
                                                            <span ng-style="{'width': 42.86+'%' }" style="width: 42.86%;"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li><!-- end ngRepeat: t in tagAnalysisArr -->
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- end ngIf: tagAnalysisArr && tagAnalysisArr.length -->
                    <div class="col-xs-12 ng-hide" id="question-level" ng-show="(topEasy &amp;&amp; topEasy.length > 0) || (topDifficult &amp;&amp; topDifficult.length > 0) || (topTricky &amp;&amp; topTricky.length > 0)">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Question Level</h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-12 top-10-easy-hard-tricky">
                                        <ul class="nav nav-tabs nav-justified mar-b32" role="tablist">
                                            <li role="presentation" class="active"><a href="#top-10-easy" aria-controls="top-10-easy" role="tab" data-toggle="tab">Top 10 Easy</a></li>
                                            <li role="presentation"><a href="#top-10-hard" aria-controls="top-10-hard" role="tab" data-toggle="tab">Top 10 Difficult</a></li>
                                            <li role="presentation"><a href="#top-10-tricky" aria-controls="top-10-tricky" role="tab" data-toggle="tab">Top 10 Tricky</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane active" id="top-10-easy">
                                                <ul class="list-choices">
                                                    <!-- ngRepeat: q in topEasy -->
                                                </ul>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="top-10-hard">
                                                <ul class="list-choices">
                                                    <!-- ngRepeat: q in topDifficult -->
                                                </ul>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="top-10-tricky">
                                                <ul class="list-choices">
                                                    <!-- ngRepeat: q in topTricky -->
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ana-left-box">

                <!-- ngIf: isFeedbackExists -->

                <!-- ngIf: !isFeedbackExists --><div class="panel panel-default ng-scope" ng-if="!isFeedbackExists">
                    <div class="panel-heading">
                        <h3 class="panel-title">Feedback</h3>
                    </div>
                    <div class="panel-body test-rating">
                        <h5 class="mar-v0">Rate the quality of test</h5>
                        <div class="rating-form">
                            <span class="tb-icon tb-star" ng-class="{'js-selected':rating>4}" ng-click="setRating(5)"></span> <!-- conditional class "js-selected" , this is rating 5 -->
                            <span class="tb-icon tb-star" ng-class="{'js-selected':rating>3}" ng-click="setRating(4)"></span> <!-- conditional class "js-selected" , this is rating 4 -->
                            <span class="tb-icon tb-star" ng-class="{'js-selected':rating>2}" ng-click="setRating(3)"></span> <!-- conditional class "js-selected" , this is rating 3 -->
                            <span class="tb-icon tb-star" ng-class="{'js-selected':rating>1}" ng-click="setRating(2)"></span> <!-- conditional class "js-selected" , this is rating 2 -->
                            <span class="tb-icon tb-star" ng-class="{'js-selected':rating>0}" ng-click="setRating(1)"></span> <!-- conditional class "js-selected" , this is rating 1 -->
                        </div>
                        <!-- ngIf: isShowFeedbackInput && !isFeedbackExists -->
                    </div>
                </div><!-- end ngIf: !isFeedbackExists -->

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Top Rankers</h3>
                    </div>
                    <div class="panel-body leader-board-box">
                        <ul class="leader-board list-unstyled">
                            <!-- ngRepeat: x in leaderBoardArr --><li ng-repeat="x in leaderBoardArr" class="ng-scope">
                                <div class="rank ng-binding">1.</div>
                                <div class="img" ng-style="{'background-image':'url(//storage.googleapis.com/testbook/static/assets/assets/img/utility/tb-avatar.svg)'}" style="background-image: url(&quot;//storage.googleapis.com/testbook/static/assets/assets/img/utility/tb-avatar.svg&quot;);">
                                    </div>
                                <div class="name-marks">
                                    <h5 class="text-ellipsis ng-binding">ashish singla</h5>
                                    <p class="ng-binding">16/16</p>
                                </div>
                            </li><!-- end ngRepeat: x in leaderBoardArr --><li ng-repeat="x in leaderBoardArr" class="ng-scope">
                                <div class="rank ng-binding">1.</div>
                                <div class="img" ng-style="{'background-image':'url(//storage.googleapis.com/testbook/static/dp/579d7df59389863d2ab8d152_tb)'}" style="background-image: url(&quot;//storage.googleapis.com/testbook/static/dp/579d7df59389863d2ab8d152_tb&quot;);">
                                    </div>
                                <div class="name-marks">
                                    <h5 class="text-ellipsis ng-binding">Sanjeet Kumar</h5>
                                    <p class="ng-binding">16/16</p>
                                </div>
                            </li><!-- end ngRepeat: x in leaderBoardArr --><li ng-repeat="x in leaderBoardArr" class="ng-scope">
                                <div class="rank ng-binding">1.</div>
                                <div class="img" ng-style="{'background-image':'url(//testbookbucket.s3-ap-southeast-1.amazonaws.com/account_images/42653_1406741813)'}" style="background-image: url(&quot;//testbookbucket.s3-ap-southeast-1.amazonaws.com/account_images/42653_1406741813&quot;);">
                                    </div>
                                <div class="name-marks">
                                    <h5 class="text-ellipsis ng-binding">Rajat Singla</h5>
                                    <p class="ng-binding">16/16</p>
                                </div>
                            </li><!-- end ngRepeat: x in leaderBoardArr --><li ng-repeat="x in leaderBoardArr" class="ng-scope">
                                <div class="rank ng-binding">1.</div>
                                <div class="img" ng-style="{'background-image':'url(//storage.googleapis.com/testbook/static/assets/assets/img/utility/tb-avatar.svg)'}" style="background-image: url(&quot;//storage.googleapis.com/testbook/static/assets/assets/img/utility/tb-avatar.svg&quot;);">
                                    </div>
                                <div class="name-marks">
                                    <h5 class="text-ellipsis ng-binding">vijaya kiran thota</h5>
                                    <p class="ng-binding">16/16</p>
                                </div>
                            </li><!-- end ngRepeat: x in leaderBoardArr --><li ng-repeat="x in leaderBoardArr" class="ng-scope">
                                <div class="rank ng-binding">1.</div>
                                <div class="img" ng-style="{'background-image':'url(//storage.googleapis.com/testbook/static/assets/assets/img/utility/tb-avatar.svg)'}" style="background-image: url(&quot;//storage.googleapis.com/testbook/static/assets/assets/img/utility/tb-avatar.svg&quot;);">
                                    </div>
                                <div class="name-marks">
                                    <h5 class="text-ellipsis ng-binding">ask</h5>
                                    <p class="ng-binding">16/16</p>
                                </div>
                            </li><!-- end ngRepeat: x in leaderBoardArr --><li ng-repeat="x in leaderBoardArr" class="ng-scope">
                                <div class="rank ng-binding">6.</div>
                                <div class="img" ng-style="{'background-image':'url(//storage.googleapis.com/testbook/static/dp/56604ed5995a2d6c94c66db7_tb)'}" style="background-image: url(&quot;//storage.googleapis.com/testbook/static/dp/56604ed5995a2d6c94c66db7_tb&quot;);">
                                    </div>
                                <div class="name-marks">
                                    <h5 class="text-ellipsis ng-binding">Utkarsh Sinha</h5>
                                    <p class="ng-binding">15/16</p>
                                </div>
                            </li><!-- end ngRepeat: x in leaderBoardArr --><li ng-repeat="x in leaderBoardArr" class="ng-scope">
                                <div class="rank ng-binding">6.</div>
                                <div class="img" ng-style="{'background-image':'url(//storage.googleapis.com/testbook/static/dp/577629dd995a2d3b090a8d40_1467365821_tb)'}" style="background-image: url(&quot;//storage.googleapis.com/testbook/static/dp/577629dd995a2d3b090a8d40_1467365821_tb&quot;);">
                                    </div>
                                <div class="name-marks">
                                    <h5 class="text-ellipsis ng-binding">sravan kumar</h5>
                                    <p class="ng-binding">15/16</p>
                                </div>
                            </li><!-- end ngRepeat: x in leaderBoardArr --><li ng-repeat="x in leaderBoardArr" class="ng-scope">
                                <div class="rank ng-binding">8.</div>
                                <div class="img" ng-style="{'background-image':'url(//storage.googleapis.com/testbook/static/dp/57a3a1bb93898622d5da70d9_tb)'}" style="background-image: url(&quot;//storage.googleapis.com/testbook/static/dp/57a3a1bb93898622d5da70d9_tb&quot;);">
                                    </div>
                                <div class="name-marks">
                                    <h5 class="text-ellipsis ng-binding">Venkata Kusumitha</h5>
                                    <p class="ng-binding">14.7/16</p>
                                </div>
                            </li><!-- end ngRepeat: x in leaderBoardArr --><li ng-repeat="x in leaderBoardArr" class="ng-scope">
                                <div class="rank ng-binding">8.</div>
                                <div class="img" ng-style="{'background-image':'url(//testbookbucket.s3-ap-southeast-1.amazonaws.com/account_images/89620_1426324194)'}" style="background-image: url(&quot;//testbookbucket.s3-ap-southeast-1.amazonaws.com/account_images/89620_1426324194&quot;);">
                                    </div>
                                <div class="name-marks">
                                    <h5 class="text-ellipsis ng-binding">vikas agrawal</h5>
                                    <p class="ng-binding">14.7/16</p>
                                </div>
                            </li><!-- end ngRepeat: x in leaderBoardArr --><li ng-repeat="x in leaderBoardArr" class="ng-scope">
                                <div class="rank ng-binding">8.</div>
                                <div class="img" ng-style="{'background-image':'url(//storage.googleapis.com/testbook/static/assets/assets/img/utility/tb-avatar.svg)'}" style="background-image: url(&quot;//storage.googleapis.com/testbook/static/assets/assets/img/utility/tb-avatar.svg&quot;);">
                                    </div>
                                <div class="name-marks">
                                    <h5 class="text-ellipsis ng-binding">Ashish dubey</h5>
                                    <p class="ng-binding">14.7/16</p>
                                </div>
                            </li><!-- end ngRepeat: x in leaderBoardArr -->
                        </ul>
                    </div>
                </div>
                <div class="panel panel-default" style="display : none">
                    <div class="panel-body text-center">
                        <h6 class="panel-title">Are you up for discussions?</h6>
                        <a href="#" class="btn btn-theme btn-block">Lets Discuss</a>
                    </div>
                </div>
            </div>
        </div>
    </div>