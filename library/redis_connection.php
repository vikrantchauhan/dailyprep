<?php

require "Predis/autoload.php";
Predis\Autoloader::register();


/*
    COMMANDS
    service redis_6379 start
    cat /var/run/redis_6379.pid
    kill pid

*/

class redisConnection {
    function checkRedisConnection() {
        //when master crashes and our slave become master and master become slave then change the script
        $masterRedis = $this->checkMasterStatus();        
        if(is_array($masterRedis) && isset($masterRedis['success']) && $masterRedis['success'] == "fail") {
            //$this->email($masterRedis['msg']);
            $redis = $this->checkSlaveStatus();
            return $redis;
            /*$status = $this->makeSlaveAsMaster();
            if("success" == "success")
                {
                $redis = $this->checkSlaveStatus();
                $redis->rpush("mylist", "fdsfd");
                }
            else
                {
                return "Couldn't connect to Redis";
                }*/
        }
        else {
            return $masterRedis;
        }
    }
    
    function checkMasterStatus() {
        
        try {
           $redis = new Predis\Client(array(
					"scheme" => "tcp",
					"host" => "127.0.0.1",
					"password" => "payapp_redis",
					"port" => 6969));
		   echo $redis->ping();
           return $redis;
        }
        catch (Exception $e) {
            //echo "Couldn't connect to Redis";
	        $result['success'] = "fail";
	        $result['msg']     =  $e->getMessage();
	        return $result;
        }
    }
    
    function checkSlaveStatus() {
        try {
           $redis = new Predis\Client(array(
					"scheme" => "tcp",
					"host" => "45.79.173.150",
					"password" => "payapp_redis",
					"port" => 6969));
		   echo $redis->ping();
		   //echo "ffsdf";
		   $redis->rpush("mylist", "fdsfd");
		   $redis->rpush("mylisst", "fdsfd");
           return $redis;
        }
        catch (Exception $e) {
            $status = $this->makeSlaveAsMaster();
            sleep(5);
            $this->checkSlaveStatus();
            //echo "Couldn't connect to Redis";
	        $result['success'] = "fail";
	        $result['msg']     =  $e->getMessage();
	        return $result;
        }
    }
    
    function email($txt) {
		$url = 'https://api.sendgrid.com/';
        $user = 'trakleaf';
        $pass = 'Numetric12345!@#$%';

        $params = array(
            'api_user'  => $user,
            'api_key'   => $pass,
            'to'        => 'sj@numetriclabz.com',
            'subject'   => 'Payapp master redis is not working',
            'text'      => "Payapp master redis stopped working. Fix this asap. $txt",
            'from'      => 'suneel@numetriclabz.com',
            'bcc'       => 'udit@numetriclabz.com',
            'bcc'       => 'suneel@numetriclabz.com',
          );

        $request =  $url.'api/mail.send.json';

        // Generate curl request
        $session = curl_init($request);
        // Tell curl to use HTTP POST
        curl_setopt ($session, CURLOPT_POST, true);
        // Tell curl that this is the body of the POST
        curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
        // Tell curl not to return headers, but do return the response
        curl_setopt($session, CURLOPT_HEADER, false);
        // Tell PHP not to use SSLv3 (instead opting for TLS)
        curl_setopt($session, CURLOPT_SSLVERSION, 'CURL_SSLVERSION_TLSv1_2');
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);

        // obtain response
        $response = curl_exec($session);
        curl_close($session);
        // print everything out
        print_r($response);
	}
	
	function makeSlaveAsMaster() {
	    $request =  'http://45.79.173.150/payapp_redis.php';
        $params = array("auth" => "payapp_key");
        // Generate curl request
        $session = curl_init($request);
        // Tell curl to use HTTP POST
        curl_setopt ($session, CURLOPT_POST, true);
        // Tell curl that this is the body of the POST
        curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
        // Tell curl not to return headers, but do return the response
        curl_setopt($session, CURLOPT_HEADER, false);
        // Tell PHP not to use SSLv3 (instead opting for TLS)
        curl_setopt($session, CURLOPT_SSLVERSION, 'CURL_SSLVERSION_TLSv1_2');
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);

        // obtain response
        $response = curl_exec($session);
        curl_close($session);
        // print everything out
        print_r($response);
	}

}

