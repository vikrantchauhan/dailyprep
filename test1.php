<div class="page-header">
	<a href="/previous-years/cse"><img src="/images/web-logo.png" style="margin-right: 2%;" id="test-logo"></a>
    <p>{{test_name}}</p>
    <div class="totaltime">
    	<!-- <img src="/images/stopwatch.png">
    	<span id="hour">03</span>:<span id="min">00</span>:<span id="sec">00</span>
    	<img src="/images/pause.png" id="pauseButton">-->
    	<livetest-timer test-duration="{{remainingTime}}" is-test-submitted="false" is-timer-paused="{{isTimerPaused}}" test-time-over-ptr="testTimeOver" resume-ptr="resumeTimerCB" hide-zero="n">
    		<span class="pull-right timer">
			    <span class="label label-default ng-binding">{{TH}}</span> :
			    <span class="label label-default ng-binding">{{TM}}</span> :
			    <span class="label label-default ng-binding">{{TS}}</span>
			</span>
		</livetest-timer>
		<img src="/images/pause.png" id="pauseButton" ng-click="pauseTimer(currentQuestion)">
    </div>
    <!-- <button class="dp-submit-test">CALCULATOR</button> -->
    <button type="button" class="dp-submit-test btn btn-info btn-lg" ng-click="ShowHide()">CALCULATOR</button>
</div>

<div >
    <div id="big_wrapper" ng-show="IsVisible">
    <div class="cal-header">
                 <span class="drag-text">Drag Here</span>
                 <button type="button" id="close" ng-click="HideCalculator()">Close</button> 
            </div>
        <div id="form_wrapper" class="form-box">

            
            <!-- <form id="formone" name="calc">
                <input id="display" type="text" name="display" value=" " disabled contenteditable="false" >
                <br>
                <input class="button number" type="button" value="1" onClick="calc.display.value+=1">
                <input class="button number" type="button" value="2" onClick="calc.display.value+=2">
                <input class="button number" type="button" value="3" onClick="calc.display.value+=3">
                <input class="button three" type="button" value="C" onClick="Resetfunction(this.form)">
                <input class="button three" type="button" value="<-" onClick="backspace(this.form)">
                <input class="button three" type="button" value="=" onClick="evaluation(this.form)">
                 <br>
                <input class="button number" type="button" value="4" onClick="calc.display.value+=4">
                <input class="button number" type="button" value="5" onClick="calc.display.value+=5">
                <input class="button number" type="button" value="6" onClick="calc.display.value+=6">
                <input class="button opps" type="button" value="-" onClick="calc.display.value+='-'">
                <input class="button opps" type="button" value="%" onClick="calc.display.value+='%'">
                <input class="button" type="button" value="cos" onClick="cos_function()">
                <br>
                <input class="button number" type="button" value="7" onClick="calc.display.value+=7">
                <input class="button number" type="button" value="8" onClick="calc.display.value+=8">
                <input class="button number" type="button" value="9" onClick="calc.display.value+=9">
                <input class="button opps" type="button" value="*" onClick="calc.display.value+='*'">
                <input class="button" type="button" value="n!" onClick="fact_function()">
                <input class="button" type="button" value="sin" onClick="sin_function()">
                <br>
                <input class="button opps" type="button" value="." onClick="calc.display.value+='.'">
                <input class="button number" type="button" value="0" onClick="calc.display.value+=0">
                <input class="button opps" type="button" value="," onClick="calc.display.value+=','">
                <input class="button opps" type="button" value="+" onClick="calc.display.value+='+'">
                <input class="button opps" type="button" value="/" onClick="calc.display.value+='/'">
                <input class="button" type="button" value="tan" onClick="tan_function()">
                <br>
                <input class="button" type="button" value="E" onClick="calc.display.value+=2.718">
                <input class="button" type="button" value="pi" onClick="calc.display.value+=3.141">
                <input class="button" type="button" value="x^y" onClick="power_function()">
                <input class="button" type="button" value="(" onClick="openpara(this.value)">
                <input class="button" type="button" value=")" onClick="closepara(this.value)">
                <input class="button" type="button" value="log" onClick="log_function()">
                <br>
                <input class="button" type="button" value="sqrt" onClick="sqrt_function()">       
                <input class="button" type="button" value="LN2" onClick="calc.display.value+=0.693">
                <input class="button" type="button" value="LN10" onClick="calc.display.value+=2.302">
                <input class="button" type="button" value="log2E" onClick="calc.display.value+=1.442">
                <input class="button" type="button" value="log10E" onClick="calc.display.value+=0.434">
                <input class="button" type="button" value="EXP" onClick="exp_function">
          
            </form> -->
                        <iframe width="100%" height= "100%" src="/getcalculatore.php" frameborder="0" allowfullscreen></iframe>
        </div>
    </div>
</div>

<div id="test-page">
	<div class="left-sidebar js-trigger-toggle-sidebar">
	    <div class="left-sidebar-inner" id="questions" ng-show="isCurTab('questions')">
	        <h4 class="question-no">
	            Q. {{set.next_offset}}
	            <div class="questime">
	            	<!-- <span id="minQ">00</span>:<span id="secQ">00</span>-->
	            	<count-up-timer secs="{{set.time_taken}}" current-section="1" current-question="{{currentQuestion}}" hide-zero="y" stop="{{stop}}">
					    <no-style ng-show="isVisible(TH)" class="ng-binding ng-hide">00:</no-style>{{TM}}:{{TS}}
					</count-up-timer>
            	</div>
	        </h4>
	        <div class="question-block">
	            <div class="question">
	                <h4 class="question-full" math-jax-bind="question"></h4>
	            </div>
	            <div class="row options">
	                <div class="col-xs-12">
	                    <div class="btn-group btn-group-vertical" data-toggle="buttons" id="question_options">
	                        <label ng-repeat="val in choices" class="{{set.markedOption == $index ? 'btn active' : 'btn'}}" ng-click="submitopen($index)">
	                        <input type="radio" name="$index" data-ng-model="option" data-ng-value="$index" ng-init="$index==set.markedOption ?(option=$index):''">
	                        <i class="fa fa-circle-o fa-2x"></i>
	                        <i class="fa fa-dot-circle-o fa-2x"></i>
	                        <span math-jax-bind="val"> </span>
	                        </label>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <div class="sidebar-open-bg"></div>
	        <button class="dp-start-test previous js-trigger-toggle-sidebar" id="Qpanel">
		        <i class="fa fa-bars show-sidebar" aria-hidden="true"></i>
		        <i class="fa fa-times hide-sidebar" aria-hidden="true"></i>
	        </button>
	        <div class="button-block">
	            <div class="inner-button-block">
	                <button class="btn dp-start-test previous" id="clear">Clear <span class="temp-text">Response</span></button>
	                <button class="btn dp-start-test previous" ng-click="bookmark(test_id, user_id, set.next_offset,$event)" id="review_mark">Mark for Review <span class="temp-text">&amp; Next</span><span class="icon">→</span></button>
	                <button class="btn dp-start-test next" ng-class="{'active' : !nextdisable}" ng-click="search(test_id, user_id, set.next_offset,$event)" id="submit-question">Submit <span class="temp-text">&amp; Next</span><span class="icon">→</span></button>
	                <button class="btn dp-start-test next" ng-class="{'active' : nextdisable}" ng-click="submitPaper()" id="submit_test_full1">Submit Paper</button>
	            </div>
	        </div>
	    </div>
	    <div class="left-sidebar-inner" id="qpaper" ng-show="isCurTab('qpaper')">
	        <div class="header">
	            <h4 class="text-center">Question Paper</h4>
	        </div>
	        <div class="question-block">
	            <div class="body-inner">
	                <ul class="list-group">
	                    <li class="list-group-item" ng-repeat="question in qPaper track by $index"><span><b>Q. {{$index+1}})</b></span> <span math-jax-bind="question.question"></span></li>
	                </ul>
	            </div>
	        </div>
	        <div class="sidebar-open-bg"></div>
	        <button class="dp-start-test previous js-trigger-toggle-sidebar" id="Qpanel">
		        <i class="fa fa-bars show-sidebar" aria-hidden="true"></i>
		        <i class="fa fa-times hide-sidebar" aria-hidden="true"></i>
	        </button>
	        <div class="button-block">
	            <div class="inner-button-block">
	                <button class="btn dp-start-test previous" id="back" ng-click="setTab('questions')">Back</button>
	            </div>
	        </div>
	    </div>
	    <div class="left-sidebar-inner" id="instructions"  ng-show="isCurTab('instructions')">
	        <div class="header">
	            <h4 class="text-center">Instructions</h4>
	        </div>
	        <div class="question-block">
	            <div class="body-inner">
					<p ng-bind-html="instructions" style="padding: 20px;"></p>
	            </div>
	        </div>
	        <div class="sidebar-open-bg"></div>
	        <button class="dp-start-test previous js-trigger-toggle-sidebar" id="Qpanel">
		        <i class="fa fa-bars show-sidebar" aria-hidden="true"></i>
		        <i class="fa fa-times hide-sidebar" aria-hidden="true"></i>
	        </button>
	        <div class="button-block">
	            <div class="inner-button-block">
	                <button class="btn dp-start-test previous" id="back" ng-click="setTab('questions')">Back</button>
	            </div>
	        </div>
	    </div>
	</div>
    <div class="right-sidebar js-trigger-toggle-sidebar">
        <div class="student-details">
            <span class="dropdown">
                <img id="userImage" src="/images/profile.png">
                <div class="dropdown-content">
                    <p>My Profile</p>
                    <a href="javascript:void(0);" onclick="signOut();">Sign out</a>
                </div>
            </span>
            <span>{{user_name}}</span>
        </div>
        <div class="row q-possible-states">
            <div class="col-xs-4">
                <div class="attempted">{{countArray.attemptedCount}}</div>
                Answered
            </div>
            <div  class="col-xs-4">
                <div class="marked">{{countArray.markedCounts}}</div>
                Marked
            </div>
            <div  class="col-xs-4">
                <div>{{countArray.notVisitedCount}}</div>
                Not Visited
            </div>
            <div  class="col-xs-5">
                <div class="skipped">{{countArray.skippedCount}}</div>
                Not Answered
            </div>
            <div  class="col-xs-7">
                <div class="marked attempted">{{countArray.markedAndAnsweredCount}}</div>
                Marked And Answered
            </div>
        </div>
        <h3 class="mob-sidebar-section">Question Panel</h3>
        <div class="ques-navigation">
            <ul class="ques-nav-ul">
                <li ng-repeat="x in totalno" ng-click="search(test_id, user_id ,x, $event)" ng-class="[getClass(responseArray[x])]">{{x+1}}</li>
            </ul>
        </div>
        <div class="action-btns">
            <button class="btn dp-start-test pull-left" ng-click="setTab('qpaper')">Question Paper</button>
            <button class="btn dp-start-test pull-right" ng-click="setTab('instructions')"> Instructions</button>
            <button class="btn dp-start-test" ng-click="submitPaper()" id="submit_test_full">Submit Test</button>
        </div>
    </div>
</div>

<div class="modal fade" id="resultModal" tabindex="-1" role="dialog" aria-labelledby="summaryLabel" aria-hidden="true" data-backdrop="static" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" ng-click="showSolutions()"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
                <h3 class="modal-title" id="summaryLabel">Summary of {{test_name}}</h3>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>No. of questions</th>
                                <th>Answered</th>
                                <th>Not Answered</th>
                                <th>Marked For Review</th>
                                <th>Not Visited</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- ngRepeat: section in test.sections track by $index -->
                            <tr>
                                <td>{{marksReport.tques}}</td>
                                <td>{{countArray.attemptedCount ? countArray.attemptedCount : '0'}}</td>
                                <td>{{countArray.skippedCount ? countArray.skippedCount : '0'}}</td>
                                <td>{{countArray.markedCounts ? countArray.markedCounts : '0'}}</td>
                                <td>{{countArray.notVisitedCount ? countArray.notVisitedCount: '0'}}</td>
                            </tr>
                            <!-- end ngRepeat: section in test.sections track by $index -->
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <a href="/" ng-show="isTestPaused" class="btn theme-btn pull-left">My Tests</a>
                <button ng-show="isTestPaused" class="btn theme-btn" ng-click="resumeTimer(currentQuestion)">Resume Test</button>
                <button ng-show="isTestSubmitted" class="btn theme-btn" ng-click="showSolutions()">Continue</button>
            </div>
        </div>
    </div>
</div>