Date.dateDiff = function(e, t, s) {
    e = e.toLowerCase();
    var n = s - t,
        i = {
            w: 6048e5,
            d: 864e5,
            h: 36e5,
            m: 6e4,
            s: 1e3
        },
        o = n / i[e];
    return Math.floor(o)
};
var app = angular.module('testApp', ['ngSanitize', 'ngRoute']);

app.config(['$routeProvider',
    function config($routeProvider) {

        $routeProvider.
        when('/instruction', {
            templateUrl: '/instruction.php',
            controller: 'testInstructions'
        }).
        when('/test', {
            templateUrl: '/test1.php',
            controller: 'testCtrl'
        }).
        when('/solutions', {
            templateUrl: '/solutions.php',
            controller: 'solutionsCtrl'
        }).
        when('/analysis', {
            templateUrl: '/analysis.php',
            controller: 'analysisCtrl'
        });

    }
]);

app.controller('testInstructions', ['$scope', '$http', '$location', function($scope, $http, $location) {
    var test_id = window.location.pathname.split('/')[3];
    $http.get('/practice/gettestestinstructions', {
            params: {
                'test_id': test_id
            }
        })
        .then(function(response) {
            //console.log(response.data.data[0]); 
            $scope.info = response.data.data[0];

        });
}]);


app.directive('mathJaxBind', function() {
    var refresh = function(element) {
        MathJax.Hub.Queue(["Typeset", MathJax.Hub, element]);
    };
    return {
        link: function(scope, element, attrs) {
            scope.$watch(attrs.mathJaxBind, function(newValue) {
                element.html(newValue);
                refresh(element[0]);
            });
        }
    };
});

app.directive('livetestTimer', function() {
    return {
        restrict: "E",
        scope: {
            testDuration: "@testDuration",
            activeSectionIndex: "@activeSectionIndex",
            isTestSubmitted: "@isTestSubmitted",
            isTimerPaused: "@isTimerPaused",
            resumePtr: "=resumePtr",
            testTimeOverPtr: "=testTimeOverPtr",
            hideZero: "@hideZero"
        },
        templateUrl: "/livetest-timer.php",
        controller: ["$scope", "$interval", function(e, t) {
            e.intervalPromise = null, e.TH = "00", e.TM = "00", e.TS = "00", e.initTimerOnLoad = function(s) {
                e.intervalPromise && t.cancel(e.intervalPromise), e.populateTime(s)
            }, e.populateTime = function(s) {
                e.renderTimer(s);
                var n = !1;
                e.intervalPromise = t(function() {
                    "false" == e.isTimerPaused ? (n && e.resumePtr && (n = !1, e.resumePtr(e.pausedFor), e.pausedFor = 0), e.renderTimer(s--)) : (n = !0, e.pausedFor++)
                }, 1e3, 0, !0)
            }, e.renderTimer = function(s) {
                var n = s % 60,
                    i = Math.floor(s / 60) % 60,
                    o = Math.floor(s / 3600) % 24,
                    r = function(e) {
                        return e < 10 ? "0" + e : e
                    };
                if (e.TH = r(o), e.TM = r(i), e.TS = r(n), o <= 0 && i <= 0 && n <= 0) return t.cancel(e.intervalPromise), e.testTimeOverPtr && e.testTimeOverPtr(), !0
            }, e.isVisible = function(t) {
                return !("y" == e.hideZero && 0 == Number(t))
            }, e.$watch(function(e) {
                return e.testDuration + ":" + e.activeSectionIndex + ":" + e.isTestSubmitted
            }, function(s, n) {
                var i = s.split(":"),
                    o = Number(i[0]);
                o > 0 && "false" == i[2] ? e.initTimerOnLoad(Number(o)) : e.intervalPromise && t.cancel(e.intervalPromise)
            })
        }]
    }
});

app.directive("countDownTimer", function() {
    return {
        restrict: "E",
        scope: {
            testDuration: "@testDuration",
            activeSectionIndex: "@activeSectionIndex",
            isTestSubmitted: "@isTestSubmitted",
            isTimerPaused: "@isTimerPaused",
            resumePtr: "=resumePtr",
            testTimeOverPtr: "=testTimeOverPtr",
            hideZero: "@hideZero"
        },
        templateUrl: "/timer.php",
        controller: ["$scope", "$interval", function(e, t) {
            e.intervalPromise = null, e.TH = "00", e.TM = "00", e.TS = "00", e.pausedFor = 0, e.initTimerOnLoad = function(s) {
                e.intervalPromise && t.cancel(e.intervalPromise), e.populateTime(s)
            }, e.populateTime = function(s) {
                e.renderTimer(s);
                var n = !1;
                e.intervalPromise = t(function() {
                    "false" == e.isTimerPaused ? (n && e.resumePtr && (n = !1, e.resumePtr(e.pausedFor), e.pausedFor = 0), e.renderTimer(s--)) : (n = !0, e.pausedFor++)
                }, 1e3, 0, !0)
            }, e.renderTimer = function(s) {
                var n = s % 60,
                    i = Math.floor(s / 60) % 60,
                    o = Math.floor(s / 3600) % 24,
                    r = function(e) {
                        return e < 10 ? "0" + e : e
                    };
                if (e.TH = r(o), e.TM = r(i), e.TS = r(n), o <= 0 && i <= 0 && n <= 0) return t.cancel(e.intervalPromise), e.testTimeOverPtr && e.testTimeOverPtr(), !0
            }, e.isVisible = function(t) {
                return !("y" == e.hideZero && 0 == Number(t))
            }, e.$watch(function(e) {
                return e.testDuration + ":" + e.activeSectionIndex + ":" + e.isTestSubmitted
            }, function(s, n) {
                var i = s.split(":"),
                    o = Number(i[0]);
                o > 0 && "false" == i[2] ? e.initTimerOnLoad(Number(o)) : e.intervalPromise && t.cancel(e.intervalPromise)
            })
        }]
    }
});

app.directive("countUpTimer", function() {
    return {
        restrict: "E",
        scope: {
            secs: "@secs",
            hideZero: "@hideZero",
            stop: "@stop",
            currentSection: "@currentSection",
            currentQuestion: "@currentQuestion"
        },
        templateUrl: "/timer.php",
        controller: ["$scope", "$interval", function(e, t) {
            e.intervalPromise = null, e.TH = "00", e.TM = "00", e.TS = "00", e.initTimerOnLoad = function(s) {
                e.intervalPromise && t.cancel(e.intervalPromise), e.populateTime(s)
            }, e.populateTime = function(s) {
                e.intervalPromise = t(function() {
                    "true" == e.stop ? t.cancel(e.intervalPromise) : e.renderTimer(s++)
                }, 1e3, 0, !0)
            }, e.renderTimer = function(t) {
                var s = t % 60,
                    n = Math.floor(t / 60) % 60,
                    i = Math.floor(t / 3600) % 24,
                    o = function(e) {
                        return e < 10 ? "0" + e : e
                    };
                e.TH = o(i), e.TM = o(n), e.TS = o(s)
            }, e.isVisible = function(t) {
                return !("y" == e.hideZero && 0 == Number(t))
            }, e.$watch(function(e) {
                return e.secs + ":" + e.currentSection + ":" + e.currentQuestion
            }, function(s, n) {
                var i = Number(s.split(":")[0]);
                i >= 0 ? e.initTimerOnLoad(i) : e.intervalPromise && t.cancel(e.intervalPromise)
            })
        }]
    }
});

app.controller('testCtrl', ['$scope', '$http', function($scope, $http) {
    var r = /\d+/g;
    var cur_url = window.location.href;
    var num = cur_url.match(r);
    $scope.question = "";
    $scope.answer = null;
    $scope.test_id = null;
    $scope.test_name = '';
    $scope.user_id = null;
    $scope.question_id = null;
    $scope.utr_id = null;
    $scope.report = false;
    $scope.userReport = null;
    $scope.level = false;
    $scope.responseArray = null;
    $scope.isMarked = null;
    $scope.qPaper = null;
    $scope.currentTab = 'questions';
    $scope.isTime = true;
    $scope.isCalc = true;
    $scope.marksReport = null;
    $scope.remainingTime = null;
    $scope.timeTakenForOneQuestion = 0;
    $scope.questionStartTime = null;
    $scope.isTimerPaused = false;
    $scope.stop = false;
    $scope.pausedFor = 0;
    $scope.currentQuestion=0;
    $scope.user_name='';
    if (num) {
        var test_id = num[0];
        $http({
            method: 'POST',
            url: '/practice/gettestdetails',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'test_id=' + test_id
        }).success(function(data) {
            var obj = data.data[0];
            if (obj.test_status == 'completed') {
                window.location.href = '#/solutions';
            } else {
                $scope.test_id = obj.test_id;
                $scope.test_name = obj.test_name;
                $scope.user_id = obj.user_id;
                $scope.currentQuestion = obj.current_question_offset;
                $scope.qPaper = obj.question_paper;
                $scope.instructions = obj.instructions;
                $scope.remainingTime = obj.remaining_time;
                $scope.user_name=data.user_details.user_name;
                $http.get("/api/check", {
                        params: {
                            "user_id": $scope.user_id,
                            "test_id": $scope.test_id,
                            "offset": obj.current_question_offset,
                            "question_id": $scope.question_id,
                            "answer_id": $scope.answer,
                            "is_Correct": isCorrect,
                            "utr_id": $scope.utr_id,
                            "time_taken": 0
                        }
                    })
                    .then(function(response) {
                        $scope.answer = null;
                        $scope.level = true;
                        $scope.set = response.data;
                        $scope.question = response.data.question;
                        $scope.currentQuestion = obj.current_question_offset;
                        $scope.question_id = response.data.id;
                        $scope.choices = JSON.parse(response.data.answer_choices);
                        $scope.utr_id = response.data.utr_id;
                        $scope.questionStartTime = new Date;
                        if ($scope.set.next_offset >= $scope.set.total)
                            $scope.nextdisable = true;
                        $http.get("/api/response", {
                                params: {
                                    "utr_id": $scope.utr_id,
                                    "test_id": $scope.test_id,
                                    "user_id": $scope.user_id
                                }
                            })
                            .then(function(response) {
                                $scope.responseArray = response.data.data;
                                $scope.countArray = response.data.counts;
                            });
                    });

                $http.get("/api/getTotalQuetions", {
                        params: {
                            "test_id": $scope.test_id
                        }
                    })
                    .then(function(response) {
                        $scope.totalno = response.data.totalQ;
                    });
            }

        });
    }

    if ($scope.current_question_id > 0)
        $scope.prevdisable = false;

    var isCorrect = null;

    if ($scope.level)
        isCorrect = ($scope.set.correct_answer == $scope.answer) ? true : false;

    $scope.search = function(test_id, user_id, offset, event) {

    	$scope.test_id = test_id;
        $scope.user_id = user_id;
        $scope.givenAns = [];
        $scope.check = true;
        $scope.nextdisable = false;
        $scope.prevdisable = true;
        $scope.submitdisable = true;

        if (offset > 0)
            $scope.prevdisable = false;

        var isCorrect = null;

        if ($scope.level)
            isCorrect = ($scope.set.correct_answer == $scope.answer) ? true : false;

        var currentTime = new Date;
        //var timeTaken = ($scope.questionStartTime - currentTime);
        var timeTaken = Date.dateDiff("s", $scope.questionStartTime, currentTime);
        $http.get("/api/check", {
                params: {
                    "user_id": user_id,
                    "test_id": test_id,
                    "offset": offset,
                    "question_id": $scope.question_id,
                    "answer_id": $scope.answer,
                    "is_Correct": isCorrect,
                    "utr_id": $scope.utr_id,
                    "time_taken": timeTaken
                }
            })
            .then(function(response) {
                $scope.answer = null;
                $scope.level = true;
                $scope.set = response.data;
                $scope.currentQuestion = response.data.prev_offset + 1;
                $scope.question = response.data.question;
                $scope.question_id = response.data.id;
                $scope.choices = JSON.parse(response.data.answer_choices);
                $scope.utr_id = response.data.utr_id;
                $scope.questionStartTime = new Date;
                if ($scope.set.next_offset >= $scope.set.total)
                    $scope.nextdisable = true;
                $('#question_options>label').each(function() {
                    $(this).removeClass('active');
                    //$(this).find('.fa-circle-o').css('display','inline');
                    //$(this).find('.fa-dot-circle-o').css('display','none');
                });

                $http.get("/api/response", {
                        params: {
                            "utr_id": $scope.utr_id,
                            "test_id": $scope.test_id,
                            "user_id": $scope.user_id
                        }
                    })
                    .then(function(response) {
                        $scope.responseArray = response.data.data;
                        $scope.countArray = response.data.counts;
                    });
            });
        $scope.setTab('questions');

    }

    $scope.bookmark = function(test_id, user_id, offset, event) {

        /* the $http service allows you to make arbitrary ajax requests.
         * in this case you might also consider using angular-resource and setting up a
         * User $resource. */

        $scope.test_id = test_id;
        $scope.user_id = user_id;
        $scope.givenAns = [];
        $scope.check = true;
        $scope.nextdisable = false;
        $scope.prevdisable = true;
        $scope.submitdisable = true;

        if (offset > 0)
            $scope.prevdisable = false;

        var isCorrect = null;

        if ($scope.level)
            isCorrect = ($scope.set.correct_answer == $scope.answer) ? true : false;

        var currentTime = new Date;
        //var timeTaken = ($scope.questionStartTime - currentTime);
        var timeTaken = Date.dateDiff("s", $scope.questionStartTime, currentTime);

        $http.get("/api/check", {
                params: {
                    "user_id": user_id,
                    "test_id": test_id,
                    "offset": offset,
                    "question_id": $scope.question_id,
                    "answer_id": $scope.answer,
                    "is_Correct": isCorrect,
                    "utr_id": $scope.utr_id,
                    "isMarked": true,
                    "time_taken": timeTaken
                }
            })
            .then(function(response) {
                console.log(response);
                $scope.answer = null;
                $scope.level = true;
                $scope.set = response.data;
                $scope.currentQuestion = response.data.prev_offset + 1;
                $scope.question = response.data.question;
                $scope.question_id = response.data.id;
                $scope.choices = JSON.parse(response.data.answer_choices);

                $scope.utr_id = response.data.utr_id;
                $scope.questionStartTime = new Date;
                if ($scope.set.next_offset >= $scope.set.total)
                    $scope.nextdisable = true;


                $http.get("/api/response", {
                        params: {
                            "utr_id": $scope.utr_id,
                            "test_id": $scope.test_id,
                            "user_id": $scope.user_id
                        }
                    })
                    .then(function(response) {
                        $scope.responseArray = response.data.data;
                        $scope.countArray = response.data.counts;
                    });
            });

    }



    $scope.submitopen = function(val, elem) {
        $scope.answer = val;
        $scope.submitdisable = false;
    }


    $scope.submitPaper = function() {

        $http.get('/api/submit_paper', {
                params: {
                    "user_id": $scope.user_id,
                    "test_id": $scope.test_id
                }
            })
            .then(function(response) {
                if (response.data.report.test_status == "completed") {
                    //window.location.href = '#/solutions';
                    $http.get("/api/response", {
                            params: {
                                "utr_id": $scope.utr_id,
                                "test_id": $scope.test_id,
                                "user_id": $scope.user_id
                            }
                        })
                        .then(function(response) {
                            $scope.marksReport = response.data.report;
                            $scope.countArray = response.data.counts;
                            $scope.isTestSubmitted = true;
                            $scope.isTestPaused = false;
                            $('#resultModal').modal('show');
                        });
                }
            });
    };

    $scope.showSolutions = function() {
        $('#resultModal').modal('hide');
        window.location.href = '#/solutions';
        location.reload(true);
    };

    $scope.totalQ = function(test_id) {
        $http.get("/api/getTotalQuetions", {
                params: {
                    "test_id": test_id
                }
            })
            .then(function(response) {
                $scope.totalno = response.data.totalQ;
            });
    }

    $scope.getClass = function(data) {
        if (data) {
            if (data['attempted'] == ("Yes") && data['is_Correct'] != null && data['isMarked'] == 0)
                return "attempted";
            else if (data['attempted'] == ("Yes") && data['is_Correct'] == null && data['isMarked'] == 1)
                return "marked";
            else if (data['attempted'] == ("Yes") && data['is_Correct'] != null && data['isMarked'] == 1)
                return "marked attempted";
            else if (data['attempted'] == ("Yes") && data['is_Correct'] == null)
                return "skipped";
        } else {
            return '';
        }
    }

    $scope.isCurTab = function(tab) {
        if ($scope.currentTab == tab) {
            return true;
        } else {
            return false
        }
    }

    $scope.setTab = function(tab) {
        $scope.currentTab = tab;
    }

    $scope.pauseTimer = function(offset) {
        bootbox.confirm({
            message: "Are You Sure You Want To Pause The Test?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-test-primary'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-gray-default'
                }
            },
            callback: function(result) {
                if (result) {
                    $scope.isTimerPaused = true;
                    $scope.stop = true;
                    var currentTime = new Date;
                    var timeTaken = Date.dateDiff("s", $scope.questionStartTime, currentTime);

                    $http.get("/api/check", {
                            params: {
                                "user_id": $scope.user_id,
                                "test_id": $scope.test_id,
                                "offset": offset,
                                "question_id": $scope.question_id,
                                "answer_id": $scope.answer,
                                "utr_id": $scope.utr_id,
                                "time_taken": timeTaken
                            }
                        })
                        .then(function(response) {
                            $http.get("/api/response", {
                                    params: {
                                        "utr_id": $scope.utr_id,
                                        "test_id": $scope.test_id,
                                        "user_id": $scope.user_id
                                    }
                                })
                                .then(function(response) {
                                    $scope.marksReport = response.data.report;
                                    $scope.countArray = response.data.counts;
                                    $scope.isTestSubmitted = false;
                                    $scope.isTestPaused = true;
                                    $('#resultModal').modal('show');
                                });
                        });
                }
            }
        });
    }

    $scope.resumeTimer = function(offset) {
        $('#resultModal').modal('hide');
        $scope.isTimerPaused = false;
        $scope.stop = false;
        $scope.questionStartTime = new Date;
        $http.get("/api/check", {
            params: {
                "user_id": $scope.user_id,
                "test_id": $scope.test_id,
                "offset": offset,
                "question_id": $scope.question_id,
                "answer_id": $scope.answer,
                "is_Correct": isCorrect,
                "utr_id": $scope.utr_id,
                "time_taken": 0
            }
        })
        .then(function(response) {
            $scope.answer = null;
            $scope.level = true;
            $scope.set = response.data;
            $scope.question = response.data.question;
            $scope.currentQuestion = response.data.prev_offset + 1;
            $scope.question_id = response.data.id;
            $scope.choices = JSON.parse(response.data.answer_choices);
            $scope.utr_id = response.data.utr_id;
            $scope.questionStartTime = new Date;
            if ($scope.set.next_offset >= $scope.set.total)
                $scope.nextdisable = true;
            $http.get("/api/response", {
                    params: {
                        "utr_id": $scope.utr_id,
                        "test_id": $scope.test_id,
                        "user_id": $scope.user_id
                    }
                })
                .then(function(response) {
                    $scope.responseArray = response.data.data;
                    $scope.countArray = response.data.counts;
                    $scope.isTestSubmitted = false;
                    $scope.isTestPaused = false;
                });
        });
    }

    $scope.IsVisible = false;
    $scope.ShowHide = function () {
         $scope.IsVisible =  true;
         //alert("szagza");
    }

    $scope.HideCalculator = function(){
        $scope.IsVisible = false;
    }


     
}]);

app.controller('solutionsCtrl', ['$scope', '$http', function($scope, $http) {
    var r = /\d+/g;
    var cur_url = window.location.href;
    var num = cur_url.match(r);
    $scope.question = "";
    $scope.answer = null;
    $scope.test_id = null;
    $scope.user_id = null;
    $scope.question_id = null;
    $scope.utr_id = null;
    $scope.report = false;
    $scope.userReport = null;
    $scope.level = false;
    $scope.responseArray = null;
    $scope.isMarked = null;
    $scope.qPaper = null;
    $scope.current_offset = 0;
    $scope.currentTab = 'questions';
    $scope.prevdisable = true;
    $scope.nextdisable = false;
    $scope.test_name = '';
    $scope.marksReport = null;
    $scope.user_name=''
    if (num) {
        var test_id = num[0];
        $http({
            method: 'POST',
            url: '/practice/gettestdetails',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'test_id=' + test_id
        }).success(function(data) {
            var obj = data.data[0];
            $scope.test_name = obj.test_name;
            $scope.test_id = obj.test_id;
            $scope.user_id = obj.user_id;
            $scope.current_question_offeset = obj.current_question_offset;
            $scope.qPaper = obj.question_paper;
            $scope.instructions = obj.instructions;
            $scope.utr_id = obj.utr_id;
            $scope.user_name=data.user_details.user_name;

            $http.get("/api/response", {
                    params: {
                        "utr_id": $scope.utr_id,
                        "test_id": $scope.test_id,
                        "user_id": $scope.user_id
                    }
                })
                .then(function(response) {
                    $scope.responseArray = response.data.data;
                    $scope.set = response.data.data[$scope.current_offset];
                    $scope.choices = JSON.parse(response.data.data[$scope.current_offset]['answer_choices']);
                    $scope.countArray = response.data.counts;
                    $scope.marksReport = response.data.report;
                });

            $http.get("/api/getTotalQuetions", {
                    params: {
                        "test_id": $scope.test_id
                    }
                })
                .then(function(response) {
                    $scope.totalno = response.data.totalQ;
                });

        });
    }


    $scope.totalQ = function(test_id) {
        $http.get("/api/getTotalQuetions", {
                params: {
                    "test_id": test_id
                }
            })
            .then(function(response) {
                $scope.totalno = response.data.totalQ;
            });
    }

    $scope.getClass = function(data) {
        if (data) {
            if (data['attempted'] == ("Yes") && data['is_Correct'] != null && data['isMarked'] == 0)
                return "attempted";
            else if (data['attempted'] == ("Yes") && data['is_Correct'] == null && data['isMarked'] == 1)
                return "marked";
            else if (data['attempted'] == ("Yes") && data['is_Correct'] != null && data['isMarked'] == 1)
                return "marked attempted";
            else if (data['attempted'] == ("Yes") && data['is_Correct'] == null)
                return "skipped";
        } else {
            return '';
        }
    }

    $scope.solutionsClass = function(data, index) {
        if (data.markedOption == index && data.correct_answer == index) {
            return 'btn right-answer';
        } else if (data.markedOption == index && data.correct_answer != index) {
            return 'btn wrong-answer';
        } else if (data.correct_answer == index) {
            return 'btn right-answer';
        } else {
            return 'btn';
        }
    }

    $scope.isCurTab = function(tab) {
        if ($scope.currentTab == tab) {
            return true;
        } else {
            return false
        }
    }

    $scope.setTab = function(tab) {
        $scope.currentTab = tab;
    }

    $scope.getNext = function(offset) {
        $scope.current_offset = offset;
        $scope.set = $scope.responseArray[offset];
        $scope.choices = JSON.parse($scope.responseArray[offset]['answer_choices']);
        if ($scope.current_offset > 0) {
            $scope.prevdisable = false;
        } else {
            $scope.prevdisable = true;
        }
        if (offset == $scope.totalno.length - 1) {
            $scope.nextdisable = true;
        } else {
            $scope.nextdisable = false;
        }
        $scope.setTab('questions');
    }


}]);



app.controller('analysisCtrl', ['$scope', '$http', function($scope, $http) {
    var r = /\d+/g;
    var cur_url = window.location.href;
    var num = cur_url.match(r);
    $scope.question = "";
    $scope.answer = null;
    $scope.test_id = null;
    $scope.user_id = null;
    $scope.question_id = null;
    $scope.utr_id = null;
    $scope.report = false;
    $scope.userReport = null;
    $scope.level = false;
    $scope.responseArray = null;
    $scope.isMarked = null;
    $scope.qPaper = null;
    $scope.current_offset = 0;
    $scope.currentTab = 'questions';
    $scope.prevdisable = true;
    $scope.nextdisable = false;
    $scope.test_name = '';
    $scope.marksReport = null;
    $scope.user_name = '';
    if (num) {
        var test_id = num[0];
        $http({
            method: 'POST',
            url: '/practice/gettestdetails',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'test_id=' + test_id
        }).success(function(data) {
            var obj = data.data[0];
            $scope.test_name = obj.test_name;
            $scope.test_id = obj.test_id;
            $scope.user_id = obj.user_id;
            $scope.current_question_offeset = obj.current_question_offset;
            $scope.qPaper = obj.question_paper;
            $scope.instructions = obj.instructions;
            $scope.utr_id = obj.utr_id;
            $scope.user_name = data.user_details.user_name;

            $http.get("/api/response", {
                    params: {
                        "utr_id": $scope.utr_id,
                        "test_id": $scope.test_id,
                        "user_id": $scope.user_id
                    }
                })
                .then(function(response) {
                    $scope.responseArray = response.data.data;
                    $scope.set = response.data.data[$scope.current_offset];
                    $scope.choices = JSON.parse(response.data.data[$scope.current_offset]['answer_choices']);
                    $scope.countArray = response.data.counts;
                    $scope.marksReport = response.data.report;
                });

            $http.get("/api/getTotalQuetions", {
                    params: {
                        "test_id": $scope.test_id
                    }
                })
                .then(function(response) {
                    $scope.totalno = response.data.totalQ;
                });

        });
    }


    $scope.totalQ = function(test_id) {
        $http.get("/api/getTotalQuetions", {
                params: {
                    "test_id": test_id
                }
            })
            .then(function(response) {
                $scope.totalno = response.data.totalQ;
            });
    }

    $scope.getClass = function(data) {
        if (data) {
            if (data['attempted'] == ("Yes") && data['is_Correct'] != null && data['isMarked'] == 0)
                return "attempted";
            else if (data['attempted'] == ("Yes") && data['is_Correct'] == null && data['isMarked'] == 1)
                return "marked";
            else if (data['attempted'] == ("Yes") && data['is_Correct'] != null && data['isMarked'] == 1)
                return "marked attempted";
            else if (data['attempted'] == ("Yes") && data['is_Correct'] == null)
                return "skipped";
        } else {
            return '';
        }
    }

    $scope.solutionsClass = function(data, index) {
        if (data.markedOption == index && data.correct_answer == index) {
            return 'btn right-answer';
        } else if (data.markedOption == index && data.correct_answer != index) {
            return 'btn wrong-answer';
        } else if (data.correct_answer == index) {
            return 'btn right-answer';
        } else {
            return 'btn';
        }
    }

    $scope.isCurTab = function(tab) {
        if ($scope.currentTab == tab) {
            return true;
        } else {
            return false
        }
    }

    $scope.setTab = function(tab) {
        $scope.currentTab = tab;
    }

    $scope.getNext = function(offset) {
        $scope.current_offset = offset;
        $scope.set = $scope.responseArray[offset];
        $scope.choices = JSON.parse($scope.responseArray[offset]['answer_choices']);
        if ($scope.current_offset > 0) {
            $scope.prevdisable = false;
        } else {
            $scope.prevdisable = true;
        }
        if (offset == $scope.totalno.length - 1) {
            $scope.nextdisable = true;
        } else {
            $scope.nextdisable = false;
        }
        $scope.setTab('questions');
    }


}]);

$(document).ready(function() {

    var width = $(window).width();


    $("#submit-question").click(function() {
        $('input[name="pageSet"]').attr('checked', false);
        $('label').removeClass('active');
        QClock.restart();

    });


    if (width < 700) {

        $(".ques-nav-ul").click(function() {
            $(".ques-navigation").css("display", "none");
        });

        $("#submit_test_full").click(function() {
            $(".page-header").prepend("<a href='/previous-years/cse' style='position: absolute;top: 3%;color: #FFF;font-size: 0.8em;'> Back</a>");
        });

        $(document).on("click", ".closeQ", function() {
            $(".ques-navigation").css("display", "none");
        });

        $(document).on("click", "#Qpanel", function() {
            //$(".ques-navigation").css("display", "block");
            sidebarToggle();
        });

        function sidebarClose() {
            $(".js-trigger-toggle-sidebar").removeClass("active"), $("#sidebar").removeClass("active"), $(".tp-left-box").removeClass("active")
        }

        function sidebarToggle() {
            $(".js-trigger-toggle-sidebar").toggleClass("active"), $("#sidebar").toggleClass("active"), $(".tp-left-box").toggleClass("active")
        }

    }


    $(window).load(function() {
        $('.dp-submit-test').css('display', 'inline-block');
        if (document.getElementById('userName')) {
            document.getElementById('userName').innerHTML = username;
        }

        if (document.getElementById('userImage')) {
            document.getElementById('userImage').setAttribute('src', userImage);
        }
    });

    $(document).on("click", ".ques-nav-ul", function() {
        $('input[name="pageSet"]').attr('checked', false);
        $('label').removeClass('active');
    });

    $(document).on("click", "#clear", function() {
        $('input[type="radio"]').attr('checked', false);
        $('label').removeClass('active');
    });

    $(window).on('load',function() {

    $( "#big_wrapper" ).draggable({
           containment: "#test-page"
        });
    // $( "#draggable" ).resizable();
  });



});