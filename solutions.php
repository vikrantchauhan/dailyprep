<div class="page-header">
	<a href="/previous-years/cse"><img src="/images/web-logo.png" style="margin-right: 2%;" id="test-logo"></a>
    <p>{{test_name}}</p>
</div>
<div id="test-page">
	<div class="left-sidebar js-trigger-toggle-sidebar">
	    <div class="left-sidebar-inner" id="questions" ng-show="isCurTab('questions')">
	        <h4 class="question-no">
	            Q. {{current_offset + 1}}
	        </h4>
	        <div class="question-block">
	            <div class="question">
	                <h4 class="question-full" math-jax-bind="set.question"></h4>
	            </div>
	            <div class="row solutions">
	                <div class="col-xs-12">
	                    <div class="btn-group btn-group-vertical" data-toggle="buttons">
	                        <label ng-repeat="val in choices" ng-class="[solutionsClass(set,$index)]">
		                        <i class="fa fa-check" aria-hidden="true"></i>
		                        <i class="fa fa-times" aria-hidden="true"></i>
		                        <span math-jax-bind="val"> </span>
	                        </label>
	                    </div>
	                </div>
	            </div>
	            <div class="row explanations" ng-if="set.explanation && set.explanation.trim() != ''">
	            	<h4>Solution:</h4>
	            	<p math-jax-bind="set.explanation"></p>
	            </div>
	        </div>
	        <div class="sidebar-open-bg"></div>
	        <button class="dp-start-test previous js-trigger-toggle-sidebar" id="Qpanel">
		        <i class="fa fa-bars show-sidebar" aria-hidden="true"></i>
		        <i class="fa fa-times hide-sidebar" aria-hidden="true"></i>
	        </button>
	        <div class="button-block">
	            <div class="inner-button-block">
	                <button class="btn dp-start-test previous" ng-class="{'disabled' : prevdisable}" ng-click="getNext(current_offset - 1)">Previous</button>
	                <button class="btn dp-start-test next" ng-class="{'active' : !nextdisable}" ng-click="getNext(current_offset + 1)">Next<span class="icon">→</span></button>
	            </div>
	        </div>
	    </div>
	    <div class="left-sidebar-inner" id="qpaper" ng-show="isCurTab('qpaper')">
	        <div class="header">
	            <h4 class="text-center">Question Paper</h4>
	        </div>
	        <div class="question-block">
	            <div class="body-inner">
	                <ul class="list-group">
	                    <li class="list-group-item" ng-repeat="question in qPaper track by $index"><span><b>Q. {{$index+1}})</b></span> <span math-jax-bind="question.question"></span></li>
	                </ul>
	            </div>
	        </div>
	        <div class="sidebar-open-bg"></div>
	        <button class="dp-start-test previous js-trigger-toggle-sidebar" id="Qpanel">
		        <i class="fa fa-bars show-sidebar" aria-hidden="true"></i>
		        <i class="fa fa-times hide-sidebar" aria-hidden="true"></i>
	        </button>
	        <div class="button-block">
	            <div class="inner-button-block">
	                <button class="btn dp-start-test previous" id="back" ng-click="setTab('questions')">Back</button>
	            </div>
	        </div>
	    </div>
	    <div class="left-sidebar-inner" id="instructions"  ng-show="isCurTab('instructions')">
	        <div class="header">
	            <h4 class="text-center">Instructions</h4>
	        </div>
	        <div class="question-block">
	            <div class="body-inner">
					<p ng-bind-html="instructions" style="padding: 20px;"></p>
	            </div>
	        </div>
	        <div class="sidebar-open-bg"></div>
	        <button class="dp-start-test previous js-trigger-toggle-sidebar" id="Qpanel">
		        <i class="fa fa-bars show-sidebar" aria-hidden="true"></i>
		        <i class="fa fa-times hide-sidebar" aria-hidden="true"></i>
	        </button>
	        <div class="button-block">
	            <div class="inner-button-block">
	                <button class="btn dp-start-test previous" id="back" ng-click="setTab('questions')">Back</button>
	            </div>
	        </div>
	    </div>
	</div>
    <div class="right-sidebar js-trigger-toggle-sidebar">
        <div class="student-details">
            <span class="dropdown">
                <img id="userImage" src="/images/profile.png">
                <div class="dropdown-content">
                    <p>My Profile</p>
                    <a href="javascript:void(0);" onclick="signOut();">Sign out</a>
                </div>
            </span>
            <span>{{user_name}}</span>
        </div>
        <div class="row q-possible-states">
            <div class="col-xs-4">
                <div class="attempted">{{countArray.attemptedCount}}</div>
                Answered
            </div>
            <div  class="col-xs-4">
                <div class="marked">{{countArray.markedCounts}}</div>
                Marked
            </div>
            <div  class="col-xs-4">
                <div>{{countArray.notVisitedCount}}</div>
                Not Visited
            </div>
            <div  class="col-xs-5">
                <div class="skipped">{{countArray.skippedCount}}</div>
                Not Answered
            </div>
            <div  class="col-xs-7">
                <div class="marked attempted">{{countArray.markedAndAnsweredCount}}</div>
                Marked And Answered
            </div>
        </div>
        <h3 class="mob-sidebar-section">Question Panel</h3>
        <div class="ques-navigation">
            <ul class="ques-nav-ul">
                <li ng-repeat="x in totalno" ng-click="getNext(x)" ng-class="[getClass(responseArray[x])]">{{x+1}}</li>
            </ul>
        </div>
        <div class="action-btns">
            <button class="btn dp-start-test pull-left" ng-click="setTab('qpaper')">Question Paper</button>
            <button class="btn dp-start-test pull-right" data-target="#summaryModal" data-toggle="modal"> Summary</button>
        </div>
    </div>
</div>

<div class="modal fade" id="summaryModal" tabindex="-1" role="dialog" aria-labelledby="summaryLabel" aria-hidden="true" data-backdrop="static" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
                <h3 class="modal-title" id="summaryLabel">Summary</h3>
            </div>
            <div class="modal-body">
                <h4 class="text-center mar-t0 ng-binding">Total Marks: {{marksReport.obtained_marks}}</h4>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>No. of questions</th>
                                <th>Correct</th>
                                <th>Incorrect</th>
                                <th>Skipped</th>
                                <th>Marks</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- ngRepeat: section in test.sections track by $index -->
                            <tr>
                                <td>{{marksReport.tques}}</td>
                                <td>{{marksReport.correct ? marksReport.correct : '0'}}</td>
                                <td>{{marksReport.incorrect ? marksReport.incorrect : '0'}}</td>
                                <td>{{marksReport.skipped ? marksReport.skipped : '0'}}</td>
                                <td>{{marksReport.obtained_marks ? marksReport.obtained_marks : '0'}}</td>
                            </tr>
                            <!-- end ngRepeat: section in test.sections track by $index -->
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <a href="" class="btn btn-theme test-analysis-btn">Continue to Test Analysis</a>-->
            </div>
        </div>
    </div>
</div>