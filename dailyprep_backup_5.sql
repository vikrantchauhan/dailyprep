-- MySQL dump 10.13  Distrib 5.7.13, for Linux (x86_64)
--
-- Host: localhost    Database: dailyprep
-- ------------------------------------------------------
-- Server version	5.7.13-0ubuntu0.16.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `chapter_master`
--

DROP TABLE IF EXISTS `chapter_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chapter_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `abbreviation` varchar(100) DEFAULT NULL,
  `subject_id` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` varchar(100) DEFAULT NULL,
  `modified_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `id` (`id`),
  KEY `sub_id` (`subject_id`),
  CONSTRAINT `sub_id` FOREIGN KEY (`subject_id`) REFERENCES `subject_master` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chapter_master`
--

LOCK TABLES `chapter_master` WRITE;
/*!40000 ALTER TABLE `chapter_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `chapter_master` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER chapter_master_update_entry BEFORE UPDATE ON chapter_master FOR EACH ROW SET NEW.modify_date = CURRENT_TIMESTAMP */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `exam_chapter`
--

DROP TABLE IF EXISTS `exam_chapter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exam_chapter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_id` int(11) NOT NULL,
  `chapter_id` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` varchar(100) DEFAULT NULL,
  `modified_by` varchar(100) DEFAULT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `ex_ec_id` (`exam_id`),
  KEY `chap_ec_id` (`chapter_id`),
  CONSTRAINT `chap_ec_id` FOREIGN KEY (`chapter_id`) REFERENCES `chapter_master` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ex_ec_id` FOREIGN KEY (`exam_id`) REFERENCES `exam_master` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exam_chapter`
--

LOCK TABLES `exam_chapter` WRITE;
/*!40000 ALTER TABLE `exam_chapter` DISABLE KEYS */;
/*!40000 ALTER TABLE `exam_chapter` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER exam_chapter_update_entry BEFORE UPDATE ON exam_chapter FOR EACH ROW SET NEW.modify_date = CURRENT_TIMESTAMP */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `exam_master`
--

DROP TABLE IF EXISTS `exam_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exam_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `abbreviation` varchar(100) DEFAULT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` varchar(100) DEFAULT NULL,
  `modified_by` varchar(100) DEFAULT NULL,
  `color_code` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exam_master`
--

LOCK TABLES `exam_master` WRITE;
/*!40000 ALTER TABLE `exam_master` DISABLE KEYS */;
INSERT INTO `exam_master` VALUES (1,'GATE-CSE','GATE Computer Science and Engineering Exam','cse','2016-09-07 11:39:38','2016-09-07 11:43:30',NULL,NULL,'#fe8664'),(2,'GATE-ECE','GATE Electronics and Communication Engineering Exam','ece','2016-09-07 11:39:38','2016-09-07 11:43:42',NULL,NULL,'#56bdde'),(3,'GATE-EE','GATE Electrical Engineering Exam','ee','2016-09-07 11:40:33','2016-09-07 11:43:53',NULL,NULL,'#b198dc'),(4,'GATE-ME','GATE Mechanical Engineering Exam','me','2016-09-07 11:40:33','2016-09-07 11:44:04',NULL,NULL,'#6dc7be');
/*!40000 ALTER TABLE `exam_master` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER exam_master_update_entry BEFORE UPDATE ON exam_master FOR EACH ROW SET NEW.modify_date = CURRENT_TIMESTAMP */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `exam_subject`
--

DROP TABLE IF EXISTS `exam_subject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exam_subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` varchar(100) DEFAULT NULL,
  `modified_by` varchar(100) DEFAULT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `ex_id` (`exam_id`),
  KEY `sub_es_id` (`subject_id`),
  CONSTRAINT `ex_id` FOREIGN KEY (`exam_id`) REFERENCES `exam_master` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sub_es_id` FOREIGN KEY (`subject_id`) REFERENCES `subject_master` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exam_subject`
--

LOCK TABLES `exam_subject` WRITE;
/*!40000 ALTER TABLE `exam_subject` DISABLE KEYS */;
/*!40000 ALTER TABLE `exam_subject` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER exam_subject_update_entry BEFORE UPDATE ON exam_subject FOR EACH ROW SET NEW.modify_date = CURRENT_TIMESTAMP */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `exam_topic`
--

DROP TABLE IF EXISTS `exam_topic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exam_topic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` varchar(100) DEFAULT NULL,
  `modified_by` varchar(100) DEFAULT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `ex_et_id` (`exam_id`),
  KEY `top_et_id` (`topic_id`),
  CONSTRAINT `ex_et_id` FOREIGN KEY (`exam_id`) REFERENCES `exam_master` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `top_et_id` FOREIGN KEY (`topic_id`) REFERENCES `topic_master` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exam_topic`
--

LOCK TABLES `exam_topic` WRITE;
/*!40000 ALTER TABLE `exam_topic` DISABLE KEYS */;
/*!40000 ALTER TABLE `exam_topic` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER exam_topic_update_entry BEFORE UPDATE ON exam_topic FOR EACH ROW SET NEW.modify_date = CURRENT_TIMESTAMP */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `learning_outcome`
--

DROP TABLE IF EXISTS `learning_outcome`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_outcome` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` varchar(100) DEFAULT NULL,
  `modified_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_outcome`
--

LOCK TABLES `learning_outcome` WRITE;
/*!40000 ALTER TABLE `learning_outcome` DISABLE KEYS */;
INSERT INTO `learning_outcome` VALUES (4,'Analysis','2016-09-08 10:30:51','0000-00-00 00:00:00',NULL,NULL),(3,'Application','2016-09-08 10:30:51','0000-00-00 00:00:00',NULL,NULL),(2,'Comprehension','2016-09-08 10:30:51','0000-00-00 00:00:00',NULL,NULL),(5,'Evaluation','2016-09-08 10:30:51','0000-00-00 00:00:00',NULL,NULL),(1,'Knowledge','2016-09-08 10:30:51','0000-00-00 00:00:00',NULL,NULL),(6,'Synthesis','2016-09-08 10:30:51','0000-00-00 00:00:00',NULL,NULL);
/*!40000 ALTER TABLE `learning_outcome` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER learning_outcome_update_entry BEFORE UPDATE ON learning_outcome FOR EACH ROW SET NEW.modify_date = CURRENT_TIMESTAMP */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `logs`
--

DROP TABLE IF EXISTS `logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_email` varchar(255) DEFAULT NULL,
  `platform` varchar(255) DEFAULT NULL,
  `device_info` text,
  `api_request` text,
  `api_response` text,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` varchar(100) DEFAULT NULL,
  `modified_by` varchar(100) DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logs`
--

LOCK TABLES `logs` WRITE;
/*!40000 ALTER TABLE `logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `logs` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER logs_update_entry BEFORE UPDATE ON logs FOR EACH ROW SET NEW.modify_date = CURRENT_TIMESTAMP */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `newsletter`
--

DROP TABLE IF EXISTS `newsletter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter` (
  `news_id` int(11) NOT NULL AUTO_INCREMENT,
  `news_email` varchar(100) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`news_email`),
  UNIQUE KEY `news_id` (`news_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter`
--

LOCK TABLES `newsletter` WRITE;
/*!40000 ALTER TABLE `newsletter` DISABLE KEYS */;
INSERT INTO `newsletter` VALUES (2,'','2016-09-06 10:04:55'),(1,'vikrant.chauhan@numetriclabz.com','2016-09-01 13:10:38');
/*!40000 ALTER TABLE `newsletter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subject_master`
--

DROP TABLE IF EXISTS `subject_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subject_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `abbreviation` varchar(100) DEFAULT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` varchar(100) DEFAULT NULL,
  `modified_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subject_master`
--

LOCK TABLES `subject_master` WRITE;
/*!40000 ALTER TABLE `subject_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `subject_master` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER subject_master_update_entry BEFORE UPDATE ON subject_master FOR EACH ROW SET NEW.modify_date = CURRENT_TIMESTAMP */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `test_info`
--

DROP TABLE IF EXISTS `test_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `test_id` int(11) NOT NULL,
  `question` text,
  `answer_choices` text,
  `correct_answer` varchar(255) DEFAULT NULL,
  `explanation` text,
  `lo_id` int(11) DEFAULT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` varchar(100) DEFAULT NULL,
  `modified_by` varchar(100) DEFAULT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `lo_ti_id` (`lo_id`),
  CONSTRAINT `lo_ti_id` FOREIGN KEY (`lo_id`) REFERENCES `learning_outcome` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test_info`
--

LOCK TABLES `test_info` WRITE;
/*!40000 ALTER TABLE `test_info` DISABLE KEYS */;
INSERT INTO `test_info` VALUES (1,1,'When $a \\ne 0$, there are two solutions to \\(ax^2 + bx + c = 0\\) and they are\r\n$$x = {-b \\pm \\sqrt{b^2-{4a\\over 2}} \\over 2a}.$$','[\"1,2,3,4\", \"1,2\", \"2,3,4\", \"3,4\"]','3','          CFL’s are not closed under complementation. Regular and recursive languages are closed under complementation.   \r\n        ',NULL,'2016-09-12 04:24:39','2016-09-13 04:20:02',NULL,NULL),(2,1,'Given the language L-{ab, aa, baa}, which of the following strings are in L*? <br/> 1) abaabaaabaa <br/>  2) aaaabaaaa <br/>  3) baaaaabaaaab <br/>  4) baaaaabaa','[\"1,2 and 3 \", \"2,3 and 4 \", \"1,2 and 4 \", \"1,3 and 4\"]','2','L ={ab, aa, baa } <br/> Let S1 = ab , S2 = aa and S3 =baa <br/> abaabaaabaa can be written as S1S2S3S1S2 <br/> aaaabaaaa can be written as S1S1S3S1 <br/> baaaaabaa can be written as S3S2S1S2 ',NULL,'2016-09-12 04:29:42','0000-00-00 00:00:00',NULL,NULL),(3,1,'In the IPv4 addressing format, the number of networks allowed under Class C addresses is ','[\"2<sup>14 </sup> \", \" 2<sup>7</sup> \", \"2<sup>21</sup> \", \"2<sup>24</sup>\"]','2','For class C address, size of network field is 24 bits. But first 3 bits are fixed as 110. <br/>Hence total number of networks possible is 2<sup>21</sup>.',NULL,'2016-09-12 04:33:58','2016-09-12 04:36:27',NULL,NULL),(4,1,'Which of the following transport layer protocols is used to support electronic mail? ','[\"SMTP \", \" IP \", \" TCP \", \" UDP\"]','2','                    E-mail uses SMTP, application layer protocol which intern uses TCP transport layer protocol.   \r\n          \r\n        ',NULL,'2016-09-12 04:40:41','2016-09-12 09:35:37',NULL,NULL),(5,1,'Consider a random variable X that takes values + 1 and -1 with probability 0.5 each. The values of the cumulative distribution function F(x) at x = -1 and +1 are ','[\" 0 and 0.5 \", \" 0 and 1  \", \" 0.5 and 1 \", \" 0.25 and 0.75 \"]','2','The cumulative distribution function F( x ) = P( X <= x) <br/>F (-1)= P (X <=-1) P (X =-1 ) = 0.5 <br/>F(+1) = P(X<=+1) =P(X=-1)+P(X=-1) = 0.5 + 0.5 = 1',NULL,'2016-09-12 04:50:14','0000-00-00 00:00:00',NULL,NULL),(6,1,'Register renaming is done is pipelined processors ','[\" as an alternative to register allocation at compile time \", \" for efficient access to function parameters and local variables  \", \"to handle certain kinds of hazards  \", \"  as part of address translation \"]','2','Register renaming is done to eliminate WAR/WAW hazards. ',NULL,'2016-09-12 04:51:51','0000-00-00 00:00:00',NULL,NULL),(7,1,'The amount of ROM needed to implement a 4 bit multiplier is ','[\"64 bits\", \" 128 bits \", \"1 Kbits\", \"  2 Kbits\"]','3','For a 4 bit multiplier there are  256 combinations.  Output will contain 8 bits.  So the amount of ROM needed is 2^8 * 8 bits = 2Kbits.',NULL,'2016-09-12 04:54:53','0000-00-00 00:00:00',NULL,NULL),(9,1,'Let W(n) and A(n) denote respectively, the worst case and average case running time of an algorithm executed on an input of size n. Which of the following is ALWAYS TRUE? ','[\"A(n)=&#x3A9;(W(n)) \", \" A(n)=&#x398;(W(n)) \", \"A(n)=O(W(n)) \", \"A(n)=o(W(n)) \"]','2','The average case time can be lesser than or even equal to the worst case. So A(n) would be\r\nupper bounded by W(n) and it will not be strict upper bound as it can even be same (e.g.\r\nBubble Sort and merge sort).<br/>? A (n) = O (W( n)).',NULL,'2016-09-12 05:34:30','0000-00-00 00:00:00',NULL,NULL),(10,1,'Let G be a simple undirected planar graph on 10 vertices with 15edges. If G is a connected graph, then the number of bounded faces in any embedding of G on the plane is equal to ','[\"3 \", \" 4\", \"5\", \"6\"]','3','We have the relation V-E+F=2, by this we will get the total number of faces, F = 7. Out of 7 faces one is an unbounded face, so total 6 bounded faces. ',NULL,'2016-09-12 05:39:07','0000-00-00 00:00:00',NULL,NULL),(11,1,'The recurrence relation capturing the optimal execution time of the Towers of Hanoi problem with n discs is ','[\"T(n)= 2T( n -2)+ 2 \", \"T(n)= 2T( n -1)+ n\", \"T(n)= 2T( n /2)+ 1\", \"T(n)= 2T( n -1)+ 1\"]','3','Let the three pegs be A,B and C, the goal is to move n pegs from A to C using peg B <br/>\r\n The following sequence of steps are executed recursively<br/>\r\n 1.move n?1 discs from A to B. This leaves disc n alone on peg A --- T(n-1)<br/>\r\n 2.move disc n from A to C---------1<br/>\r\n 3.move n?1 discs from B to C so they sit on disc n----- T(n-1)<br/>\r\n So, T(n) = 2T(n-1) +1 ',NULL,'2016-09-12 05:45:25','0000-00-00 00:00:00',NULL,NULL),(12,1,'Which of the following statements are TRUE about an SQL query?<br/>\r\n P : An SQL query can contain a HAVING clause even if it does not have a\r\n GROUP BY clause<br/>\r\n Q : An SQL query can contain a HAVING clause only if it has GROUP BY\r\n clause<br/>\r\n R : All attributes used in the GROUP BY clause must appear in the SELECT\r\n clause<br/>\r\n S : Not all attributes used in the GROUP BY clause need to appear in the\r\n SELECT clause ','[\" P and R  \", \"  P and S \", \"Q and R  \", \"Q and S \"]','1','If we use a HAVING clause without a GROUP BY clause, the HAVING condition applies to\r\nall rows that satisfy the search condition. In other words, all rows that satisfy the search\r\ncondition make up a single group.<br/> So, option P is true and Q is false. \r\n        ',NULL,'2016-09-12 05:51:39','0000-00-00 00:00:00',NULL,NULL),(13,1,'Given the basic ER and relational models, which of the following is INCORRECT?','[\" An attribute of an entity can have more than one value \", \" An attribute of an entity can be composite \", \"In a row of a relational table, an attribute can have more than one value \", \"In a row of a relational table, an attribute can have exactly one value or a NULL value  \"]','2','The term ‘entity’ belongs to ER model and the term ‘relational table’ belongs to relational\r\nmodel.<br/>\r\n Options A and B both are true since ER model supports both multivalued and composite\r\nattributes.<br/>\r\n As multivalued attributes are not allowed in relational databases, in a row of a relational\r\n(table), an attribute cannot have more than one value.\r\n        ',NULL,'2016-09-12 05:53:50','0000-00-00 00:00:00',NULL,NULL),(16,1,'What is the correct translation of the following statement into mathematical logic? <br/>\r\n“Some real numbers are rational” ','[\"?x (real (x) v rational (x)) \", \"?x (real (x) ? rational (x))\",\"?x (real (x) ? rational (x))\",\"?x (real (x) ? rational (x))\"]','2',' Option A: There exists x which is either real or rational and can be both.<br/>\r\n Option B: All real numbers are rational.<br/>\r\n Option C: There exists a real number which is rational.<br/>\r\n Option D: There exists some number which is not rational or which is real. \r\n        ',NULL,'2016-09-12 06:11:29','0000-00-00 00:00:00',NULL,NULL),(17,1,'Let A be the 2 x 2 matrix with elements a<sub>11</sub> = a<sub>12</sub> = a<sub>21</sub>= + 1 and a<sub>22</sub> =-1. Then the eigen values\r\nof the matrix A<sup>19</sup> are ','[\"1024 and -1024 \", \"1024 &radic; 2 and -1024 &radic; 2 \",\"4 &radic 2 and -4 &radic 2 \",\"512 &radic;  2 and -512 &radic;  2 \"]','3','Characteristic equation of A is |A I | 0 where is the eigen value <br/>\r\n<table class=\"matrix\">\r\n        <tr>\r\n            <td>1-?</td>\r\n            <td>1</td>\r\n            \r\n        </tr>\r\n        <tr>\r\n            <td>1</td>\r\n            <td>-1-?</td>\r\n        </tr>\r\n    </table>\r\n        =0 ? ?<sup>2</sup> ?2 = 0? ? <sup>2</sup>=±&radic; 2 <br/>\r\nEvery matrix satisfies its characteristic equation Therefore  A<sup>2</sup> - 2I = 0 ? A<sup>2</sup>= 2I .<br/>\r\nA<sup>19</sup> = A<sup>18</sup>X A =( A<sup>2</sup>)<sup>9</sup> X A = ( 2I)<sup>9</sup> X A = 512 X A <br/>\r\nHence eigen values of A<sup>19</sup> are ±512 &radic;  2.',NULL,'2016-09-12 06:34:43','0000-00-00 00:00:00',NULL,NULL),(18,1,'Consider a random variable X that takes values + 1 and -1 with probability 0.5 each. The values of the cumulative distribution function F(x) at x = -1 and +1 are  12','[\" 0 and 0.5 \", \" 0 and 1  \", \" 0.5 and 1 \", \" 0.25 and 0.75 \"]','2','          The cumulative distribution function F( x ) = P( X <= x) <br/>F (-1)= P (X <=-1) P (X =-1 ) = 0.5 <br/>F(+1) = P(X<=+1) =P(X=-1)+P(X=-1) = 0.5 + 0.5 = 1  \r\n        ',NULL,'2016-09-12 07:28:51','0000-00-00 00:00:00',NULL,NULL),(20,1,'The protocol data unit (PDU) for the application layer in the Internet stack is ',' [\"Segment\",\"Datagram\",\"Message\",\"Frame \"]','2','The PDU for Datalink layer, Network layer , Transport layer and Application layer are frame,\r\ndatagram, segment and message respectively.\r\n        ',NULL,'2016-09-12 07:37:18','0000-00-00 00:00:00',NULL,NULL),(21,1,'Consider the function f(x) = sin(x) in the interval x &#x2208;[&#x220F;/ 4,7 &#x220F;/ 4 ]. The number and location (s) of the local minima of this function are ',' [\"One, at &#x220F;/ 2\",\"One, at 3 &#x220F;/ 2\",\"Two, at &#x220F; / 2 and 3&#x220F; / 2 \",\"Two, at &#x220F;/ 4and3 &#x220F;/ 2 \"]','1','Sin x has a maximum value of 1 at &#x220F; / 2  and a minimum value of –1 at 3 &#x220F; / 2  and at all angles conterminal with them.\r\n Therefore,it has one local minimum at 3&#x220F; / 2. \r\n\r\n        ',NULL,'2016-09-12 07:44:56','0000-00-00 00:00:00',NULL,NULL),(22,1,'A process executes the code<br/> fork ();<br/>fork ();<br/>fork ();<br/>\r\n The total number of child processes created is ',' [\"3\",\"4\",\"7\",\"8\"]','2','If fork is called n times, there will be total 2<sup>n</sup>running processes including the parent process. <br/>\r\nSo, there will be 2<sup>n</sup>-1 child processes. \r\n\r\n        ',NULL,'2016-09-12 07:47:06','0000-00-00 00:00:00',NULL,NULL),(23,1,'The decimal value 0.5 in IEEE single precision floating point representation has ',' [\" fraction bits of 000…000 and exponent value of 0 \",\" fraction bits of 000…000 and exponent value of -1 \",\"fraction bits of 100…000 and exponent value of 0 \",\"no exact representation \"]','1','(0.5)<sub>10</sub>= (1.0)<sub>2</sub> X 2 <sup>-1</sup>.\r\n So, exponent = -1 and fraction is 000 - - - 000 \r\n\r\n        ',NULL,'2016-09-12 07:51:34','0000-00-00 00:00:00',NULL,NULL),(24,1,'Consider the set of strings on {0,1} in which, every substring of 3 symbols has at most two\r\nzeros. For example, 001110 and 011001 are in the language, but 100010 is not. All strings of length less than 3 are also in the language. A partially completed DFA that accepts this\r\nlanguage is shown below. <br/>\r\n<img src=\"https://dailyprep.co/question-images/2012-26.png\" /><br/>\r\nThe missing arcs in the DFA are : <br/>\r\n<img src=\"https://dailyprep.co/question-images/2012-26-1.png\" /> ',' [\" X \",\"X + Y \",\"X ? Y \",\"Y\"]','0','                    XY\' + XY =  X (Y\'+ Y) = X\r\n\r\n          \r\n          \r\n        ',NULL,'2016-09-12 07:57:36','2016-09-12 11:53:53',NULL,NULL),(25,1,'The worst case running time to search for an element in a balanced binary search tree with\r\nn2<sup>n </sup> elements is :',' [\"&#x398;(n log n)  \",\"&#x398;(n 2<sup> n</sup>) \",\"&#x398;(n)  \",\"&#x398;(log n)\"]','2','The worst case search time in a balanced BST on ‘x’ nodes is logx. So, if x =n2<sup>2</sup>\r\n, then log(n2<sup>n</sup>) = log n + log(2<sup>n</sup>) = log n + n = &#x398;(n) \r\n\r\n        ',NULL,'2016-09-12 08:09:44','0000-00-00 00:00:00',NULL,NULL),(26,1,'Assuming P &#x2260; NP,  which of the following is TRUE? ',' [\" NP-complete = NP\",\" NP-complete &#x2229; = &#x3C6;\",\" NP-hard = NP\",\"P = NP-complete \"]','1','If P!=NP, then it implies that no NP-Complete problem can be solved in polynomialtime\r\nwhich implies that the set P and the set NPC are disjoint. \r\n\r\n        ',NULL,'2016-09-12 08:13:32','0000-00-00 00:00:00',NULL,NULL),(28,1,'Which of the following is TRUE?','[\" Every relation is 3NF is also in BCNF \",\"A relation R is in 3NF if every non-prime attribute of R is fully functionally dependent on\r\nevery key of R \",\"Every relation in BCNF is also in 3NF \",\"No relation can be in both BCNF and 3NF \"]','2','Option A is false since BCNF is stricter than 3NF (it needs LHS of all FDs should be candidate\r\nkey for 3NF condition)\r\n Option B is false since the definition given here is of 2NF\r\n Option C is true, since for a relation to be in BCNF it needs to be in 3NF, every relation in\r\nBCNF satisfies all the properties of 3NF.\r\n Option D is false, since if a relation is in BCNF it will always be in 3NF.             \r\n        ',NULL,'2016-09-12 08:28:53','0000-00-00 00:00:00',NULL,NULL),(29,1,'Consider the following logical inferences. <br/>\r\nI1 : If it rains then the cricket match will not be played.\r\n The cricket match was played.<br/>\r\n Inference: There was no rain. <br/>\r\nI2 : If it rains then the cricket match will not be played.\r\n It did not rain.<br/>\r\n Inference: The cricket match was played. <br/>\r\nWhich of the following is TRUE?','[\"Both I1 and I2 are correct inferences\",\r\n \"I1 is correct but I2 is not a correct inference\",\r\n \"I1 is not correct but I2 is a correct inference\",\r\n \"Both I1 and I2 are not correct inferences\"] ','1','\r\n            \r\n        ',NULL,'2016-09-12 09:57:28','0000-00-00 00:00:00',NULL,NULL),(30,1,'Consider the set of strings on {0,1} in which, every substring of 3 symbols has at most two\r\nzeros. For example, 001110 and 011001 are in the language, but 100010 is not. All strings of length less than 3 are also in the language. A partially completed DFA that accepts this\r\nlanguage is shown below. <br/>\r\n<img src=\"http://mydailyprep.com/question-images/2012-26.png\" /><br/>\r\nThe missing arcs in the DFA are : <br/>\r\n<img src=\"http://mydailyprep.com/question-images/2012-26-1.png\" />\r\n','[\"A\",\"B\",\"C\",\"D\"]','3','            \r\n        ',NULL,'2016-09-12 10:06:13','0000-00-00 00:00:00',NULL,NULL),(31,1,'Consider an instance of TCP’s Additive Increase Multiplicative decrease (AIMD) algorithm\r\nwhere the window size at the start of the slow start phase is 2 MSS and the threshold at the\r\nstart of the first transmission is 8 MSS. Assume that a timeout occurs during the fifth\r\ntransmission. Find the congestion window size at the end of the tenth transmission.  ','[\" 8MSS\",\" 14MSS\",\" 7MSS\",\" 12MSS\"]','2','          Given, initial threshold = 8 <br/>\r\n Time = 1, during 1st transmission,Congestion window size = 2 (slow start phase)<br/>\r\n Time = 2, congestion window size = 4 (double the no. of acknowledgments)<br/>\r\n Time = 3, congestion window size = 8 (Threshold meet)<br/>\r\n Time = 4, congestion window size = 9, after threshold (increase by one Additive increase)<br/>\r\n Time = 5, transmits 10 MSS, but time out occurs congestion window size = 10      <br/>\r\nHence threshold = (Congestion window size) /2= 10/2 =  5       <br/>\r\nTime = 6, transmits 2 <br/>\r\n Time = 7, transmits 4 <br/>\r\n Time = 8, transmits 5 (threshold is 5) <br/>\r\n Time = 9, transmits 6, after threshold (increase by one Additive increase) <br/>\r\n Time = 10, transmits 7 <br/>\r\nDuring 10th transmission, it transmits 7 segments hence at the end of the tenth transmission the size of congestion window is 7 MSS. \r\n          \r\n        ',NULL,'2016-09-12 11:15:39','2016-09-12 11:17:22',NULL,NULL),(32,1,'Consider a source computer (S) transmitting a file of size 106\r\n bits to a destination computer\r\n(D) over a network of two routers (R1 and R2) and three links (L1, L2, and L3). L1 connects S\r\nto R1; L2 connects R1 to R2; and L3 connects R2 to D. Let each link be of length 100km.\r\nAssume signals travel over each line at a speed of 108\r\n meters per second. Assume that the\r\nlink bandwidth on each link is 1Mbps. Let the file be broken down into 1000 packets each of\r\nsize 1000 bits. Find the total sum of transmission and propagation delays in transmitting the\r\nfile from S to D? ','[\"1005ms\",\"1010ms\",\"3000ms \",\"3003ms\"]','0','Transmission delay for 1 packet from each of S, R1 and R2 will take 1ms <br/>\r\n Propagation delay on each link L1, L2 and L3 for one packet is 1ms <br/>\r\n Therefore the sum of transmission delay and propagation delay on each link for one packet is 2ms.<br/>\r\n The first packet reaches the destination at 6<sub>th</sub>ms <br/>\r\n The second packet reaches the destination at 7<sub>th</sub>ms <br/>\r\n So inductively we can say that 1000th packet reaches the destination at 1005<sub>th</sub> ms   <br/>          \r\n        ',NULL,'2016-09-12 11:28:20','0000-00-00 00:00:00',NULL,NULL),(33,1,'Consider the virtual page reference string <br/>\r\n1,2,3,2,4,1,3,2,4,1 <br/>\r\non a demand paged virtual memory system running on a computer system that has main\r\nmemory size of 3 page frames which are initially empty. Let LRU, FIFO and OPTIMAL\r\ndenote the number of page faults under the corresponding page replacement policy. Then:','[\" OPTIMAL < LRU < FIFO \",\"OPTIMAL < FIFO < LRU \",\"OPTIMAL = LRU\",\"OPTIMAL = FIFO \"]','1','FIFO <br/>\r\n1 1 1 4 4 4<br/>\r\n2 2 2 1 1  ? (6) faults  <br/>\r\n3 3 3 2 <br/>\r\nOptimal <br/>\r\n1 1 1 1 1 <br/>\r\n2 2 4 4  ? (5) faults <br/>\r\n3 3 2 <br/>\r\nLRU <br/>\r\n1 1 1 4 4 4 2 2 2 <br/>\r\n2 2 2 2 3 3 3 1 ? (9) faults  <br/>\r\n3 3 1 1 1 4 4 <br/>\r\n\r\nOptimal < FIFO < LRU \r\n\r\n\r\n            \r\n        ',NULL,'2016-09-12 12:21:44','0000-00-00 00:00:00',NULL,NULL),(34,1,'A file system with 300 GByte disk uses a file descriptor with 8 direct block addresses, 1\r\nindirect block address and 1 doubly indirect block address. The size of each disk block is 128\r\nBytes and the size of each disk block address is 8 Bytes. The maximum possible file size in\r\nthis file system is ','[\" 3 KBytes \",\"35 KBytes \",\" 280 KBytes \",\"dependent on the size of the disk \"]','1','Each block size = 128 Bytes <br/>\r\n Disk block address = 8 Bytes <br/>\r\n? Each disk can contain = 128  16  8  = addresses\r\n Size due to 8 direct block addresses: 8 x 128 <br/>\r\n Size due to 1 indirect block address: 16 x 128 <br/>\r\n Size due to 1 doubly indirect block address: 16 x 16 x 128 <br/>\r\n Size due to 1 doubly indirect block address: 16 x 16 x 128 <br/>\r\nSo, maximum possible file size: <br/>\r\n8 X128 +16 X128 +16 X16X 128= 1024+ 2048+ 32768= 35840 Bytes= 35KBytes',NULL,'2016-09-12 12:26:22','0000-00-00 00:00:00',NULL,NULL),(35,1,'Consider the directed graph shown in the figure below. There are multiple shortest paths\r\nbetween vertices S and T. Which one will be reported by Dijkstra’s shortest path algorithm?\r\nAssume that, in any iteration, the shortest path to a vertex v is updated only when a strictly\r\nshorter path to v is discovered. \r\n<img src=\"http://mydailyprep.com/question-images/2012-33.png\"/>','[\"  SDT\",\"SBDT \",\" SACDT \",\" SACET \"]','3','Let d[v] represent the shortest path distance computed from ‘S’ <br/>\r\nInitially d[S]=0, d[A] = ? , d B . ,d T [ ] = ? ? ? ? ? = ? <br/>\r\nAnd let P[v] represent the predecessor of v in the shortest path from ‘S’ to ‘v’ and let P[v]=-1 denote that currently predecessor of ‘v’ has not been computed <br/>\r\n? Let Q be the set of vertices for which shortest path distance has not been computed<br/>\r\n? Let W be the set of vertices for which shortest path distance has not been computed<br/>\r\n? So initially, Q = {S,A,B,C,D,E,F,G,T ,W} = ?<br/>\r\n We will use the following procedure<br/>\r\n Repeat until Q is empty<br/>\r\n {\r\n 1 u = choose a vertex from Q with minimum d[u] value<br/>\r\n 2. Q =Q -u  <br/>\r\n 3. update all the adjacent vertices of u <br/>\r\n 4. W = W U{u} } <br/>\r\n d[S] = 0, d[A] = ? , d[B] = ? ,………, d[T] = ?',NULL,'2016-09-12 12:37:38','0000-00-00 00:00:00',NULL,NULL),(36,1,'A list of n strings, each of length n, is sorted into lexicographic order using the merge-sort\r\nalgorithm. The worst case running time of this computation is ','[\" O( n log n) \",\"O( n<sup>2</sup> log n) \",\" O( n<sup>2</sup>+ log n)\",\"O( n<sup>2</sup>\"]','1','The height of the recursion tree using merge sort is log n and n <sup>2</sup>\r\n comparisons are done at each level, where at most n pairs of strings are compared at each level and n comparisons are\r\nrequired to compare any two strings, So the worst case running time is O( n<sup>2</sup> log n)',NULL,'2016-09-12 12:41:10','0000-00-00 00:00:00',NULL,NULL),(37,1,'Let G be a complete undirected graph on 6 vertices. If vertices of G are labeled, then the\r\nnumber of distinct cycles of length 4 in G is equal to ','[\" 15 \",\"30\",\" 90\",\"none\"]','3','4 vertices from 6 vertices can be chosen in <sup>6</sup>c<sub>4</sub>. Number of cycles of length 4 that can be formed from those selected vertices is (4-1)!/2 (left or right/ up or down does not matter), so total number of 4 length cycles are (<sup>6</sup>c<sub>4</sub>.3!)/2 = 45. ',NULL,'2016-09-12 12:43:40','0000-00-00 00:00:00',NULL,NULL),(38,1,'How many onto (or surjective) functions are there from an n-element (n ? 2 ) set to a 2-\r\nelement set? ','[\" 2<sup>n</sup> \",\"2<sup>n</sup> -1\",\" 2<sup>n</sup> -2\",\"2(2<sup>n</sup> -2)\"]','2','Total number of functions is 2<sup>n</sup> , out of which there will be exactly two functions where all elements map to exactly one element, so total number of onto functions is 2<sup>n</sup>-2 .',NULL,'2016-09-12 12:45:48','0000-00-00 00:00:00',NULL,NULL),(39,1,'Consider the program given below, in a block-structured pseudo-language with lexical\r\nscoping and nesting of procedures permitted. <br/>\r\nProgram main; <br/>\r\n Var . . . <br/>\r\n Procedure A1; <br/>\r\n Var ….  <br/>\r\nCall A2; <br/>\r\n End A1 <br/>\r\n Procedure A2;<br/>\r\n Var . . .<br/>\r\n Procedure A21;<br/>\r\n Var . . .<br/>\r\n Call A1;<br/>\r\n End A21<br/>\r\n Call A21;<br/>\r\n End A2<br/>\r\n Call A1;<br/>\r\n End main.<br/>\r\n Consider the calling chain: Main ? A1 ? A2 ? A21 ? A1<br/>\r\n The correct set of activation records along with their access links is given by <br/>\r\n<img src=\"http://mydailyprep.com/question-images/2012-37.png\" />','[\" A \",\"B\",\" C\",\"D\"]','3','Access link is defined as link to activation record of closest lexically enclosing block in\r\nprogram text, so the closest enclosing blocks respectively for A1 ,A2 and A21 are main ,\r\nmain and A2 ',NULL,'2016-09-12 12:54:49','0000-00-00 00:00:00',NULL,NULL),(40,1,'Suppose a circular queue of capacity (n – 1) elements is implemented with an array of n\r\nelements. Assume that the insertion and deletion operations are carried out using REAR and\r\nFRONT as array index variables, respectively. Initially, REAR = FRONT = 0. The conditions\r\nto detect queue full and queue empty are :','[\" full: (REAR+1) mod n==FRONT <br/>empty: REAR ==FRONT  \",\" full:(REAR+1)mod n==FRONT  <br/>empty: (FRONT+1)mod n==REAR \",\" full: REAR==FRONT <br/>empty: (REAR+1) mod n ==FRONT\",\" full:(FRONT+1)mod n==REAR <br/>empty: REAR ==FRONT \"]  ','0','The counter example for the condition full : REAR = FRONT is\r\n Initially when the Queue is empty REAR=FRONT=0 by which the above full condition is\r\nsatisfied which is false <br/>\r\n The counter example for the condition full : (FRONT+1)mod n =REAR is\r\n Initially when the Queue is empty REAR=FRONT=0 and let n=3, so after inserting one\r\nelement REAR=1 and FRONT=0, at this point the condition full above is satisfied, but still\r\nthere is place for one more element in Queue, so this condition is also false<br/>\r\n The counter example for the condition empty : (REAR+1)mod n = FRONT is\r\n Initially when the Queue is empty REAR=FRONT=0 and let n=2, so after inserting one\r\nelement REAR=1 and FRONT=0, at this point the condition empty above is satisfied, but the queue of capacity n-1 is full here<br/>\r\n The counter example for the condition empty : (FRONT+1)mod n =REAR is\r\n Initially when the Queue is empty REAR=FRONT=0 and let n=2, so after inserting one\r\nelement REAR=1 and FRONT=0, at this point the condition empty above is satisfied, but the queue of capacity n-1 is full here ',NULL,'2016-09-12 12:57:53','0000-00-00 00:00:00',NULL,NULL),(41,1,'An Internet Service Provider (ISP) has the following chunk of CIDR-based IP addresses\r\navailable with it: 245.248.128.0/20. The ISP wants to give half of this chunk of addresses to\r\nOrganization A, and a quarter to Organization B, while retaining the remaining with itself.\r\nWhich of the following is a valid allocation of address to A and B? ','[\"245.248.136.0/21 and 245.248.128.0/22 \",\"245.248.128.0/21 and 245.248.128.0/22 \",\"245.248.132.0/22 and 245.248.132.0/21\",\" 245.248.136.0/24 and 245.248.132.0/21\"]','0','Since half of 4096 host addresses must be given to organization A, we can set 12th bit to 1\r\nand include that bit into network part of organization A, so the valid allocation of addresses to\r\nA is 245.248.136.0/21 <br/>\r\n Now for organization B, 12th bit is set to ‘0’ but since we need only half of 2048 addresses,\r\n13th bit can be set to ‘0’ and include that bit into network part of organization B so the valid\r\nallocation of addresses to B is 245.248.128.0/22             \r\n        ',NULL,'2016-09-12 12:59:34','0000-00-00 00:00:00',NULL,NULL),(42,1,'Suppose a fair six-sided die is rolled once. If the value on the die is 1, 2, or 3, the die is rolled\r\na second time. What is the probability that the sum total of values that turn up is at least 6? ','[\" 10/21\",\"  5/12 \",\"2/3\",\" 1/6\"]  ','1','',NULL,'2016-09-12 13:00:59','0000-00-00 00:00:00',NULL,NULL),(43,1,'Fetch_And_Add (X, i) is an atomic Read-Modify-Write instruction that reads the value of\r\nmemory location X, increments it by the value i, and returns the old value of X. It is used in\r\nthe pseudocode shown below to implement a busy-wait lock. L is an unsigned integer shared variable initialized to 0. The value of 0 corresponds to lock being available, while any nonzero value corresponds to the lock being not available.\r\n AcquireLock(L){ <br/>\r\n While (Fetch_And_Add(L,1)) <br/>\r\n L = 1; <br/>\r\n } <br/>\r\n Release Lock(L){ <br/>\r\n L = 0; <br/>\r\n } <br/>\r\n This implementation','[\" fails as L can overflow\",\" fails as L can take on a non-zero value when the lock is actually available \",\"works correctly but may starve some processes \",\"works correctly without starvation \"]','1','1. Acquire lock (L) { <br/>\r\n 2. While (Fetch_And_Add(L, 1)) <br/>\r\n 3. L = 1. <br/>\r\n } <br/>\r\n 4. Release Lock (L) { <br/>\r\n 5. L = 0; <br/>\r\n 6. } <br/>\r\n Let P and Q be two concurrent processes in the system currently executing as follows\r\n P executes 1,2,3 then Q executes 1 and 2 then P executes 4,5,6 then L=0 now Q executes 3 by which L will be set to 1 and thereafter no process can set L to zero, by which all the processes could starve.             \r\n        ',NULL,'2016-09-12 13:03:54','0000-00-00 00:00:00',NULL,NULL),(44,1,'Consider the 3 process, P1, P2 and P3 shown in the table. \r\n<img src=\"http://mydailyprep.com/question-images/2012-42.png\"/>\r\nThe completion order of the 3 processes under the policies FCFS and RR2 (round robin\r\nscheduling with CPU quantum of 2 time units) are ','[\" FCFS: P1, P2, P3 RR2: P1, P2, P3 \",\"   FCFS: P1, P3, P2 RR2: P1, P3, P2  \",\"FCFS: P1, P2, P3 RR2: P1, P3, P2 \",\" FCFS: P1, P3, P2 RR2: P1, P2, P3 \"]  ','2','For FCFS Execution order will be order of Arrival time so it is P1,P2,P3 <br/>\r\n Next For RR with time quantum=2,the arrangement of Ready Queue will be as follows:\r\n RQ: P1,P2,P1,P3,P2,P1,P3,P2 <br/>\r\n This RQ itself shows the order of execution on CPU(Using Gantt Chart) and here it gives the completion order as P1,P3,P2 in Round Robin algorithm. ',NULL,'2016-09-12 13:06:39','0000-00-00 00:00:00',NULL,NULL),(45,1,'What is the minimal form of the Karnaugh map shown below? Assume that X denotes a don’t\r\ncare term. ','[\"b\'d\' \",\"  b\'d\' + b\'c\' \",\"b\'d\' + ab\'c\'d  \",\" b\'d\' + b\'c\' + c\'d\' \"]  ','1','',NULL,'2016-09-12 13:13:15','0000-00-00 00:00:00',NULL,NULL),(46,1,'Let G be a weighted graph with edge weights greater than one and G’ be the graph\r\nconstructed by squaring the weights of edges in G. Let T and T’ be the minimum spanning\r\ntrees of G and G’ respectively, with total weights t and t’. Which of the following statements\r\nis TRUE? ','[\"T’ = T with total weight t’ = t<sup>2</sup> \",\"T’ = T with total weight t’ < t<sup>2</sup>\",\"T’ ? T but total weight t’ = t<sup>2</sup>\r\n \",\"None of these \"]  ','3','Graph G is counter example for options (B) and (C) and Graph G1 is counter example for\r\noption (A)',NULL,'2016-09-12 13:16:18','0000-00-00 00:00:00',NULL,NULL),(47,1,'The bisection method is applied to compute a zero of the function f(x) = x<sup>4</sup> -x<sup>3</sup>- x<sup>2</sup>– 4 in the interval [1,9]. The method converges to a solution after ______ iterations. ','[\"1\",\"3\",\"5\",\"7\"]  ','1','f(x)= x<sup>4</sup> -x<sup>3</sup>- x<sup>2</sup>– 4\r\nfind , f(0) , f(1), f(3)',NULL,'2016-09-12 13:20:21','0000-00-00 00:00:00',NULL,NULL),(48,1,'Which of the following graph is isomorphic to \r\n<img src=\"http://mydailyprep.com/question-images/2012-46.png\"/>','[\"A\",\"B\",\"C\",\"D\"]  ','1','The graph in option (A) has a 3 length cycle whereas the original graph does not have a 3\r\nlength cycle <br/>\r\n The graph in option (C) has vertex with degree 3 whereas the original graph does not have a\r\nvertex with degree 3  <br/>\r\nThe graph in option (D) has a 4 length cycle whereas the original graph does not have a 4\r\nlength cycle  <br/>',NULL,'2016-09-12 13:22:31','0000-00-00 00:00:00',NULL,NULL),(49,1,'Consider the following transactions with data items P and Q initialized to zero: <br/>\r\nT1 : read (P) ; <br/>\r\n read (Q) ; <br/>\r\n if P = 0 then Q : = Q + 1 ; <br/>\r\n write (Q). <br/>\r\nT2 : read (Q) ; <br/>\r\n read (P) <br/>\r\n if Q = 0 then P : = P + 1 ; <br/>\r\n write (P). <br/>\r\n Any non-serial interleaving of T1 and T2 for concurrent execution leads to ','[\"a serializable schedule \",\"a schedule that is not conflict serializable\",\"a conflict serializable schedule \",\"a schedule for which precedence graph cannot be drawn\"]  ','1','Let S be a non-serial schedule, without loss of generality assume that T1 has started earlier\r\nthan T2. The first instruction of T1 is read(P) and the last instruction of T2 is write(P), so the precedence graph for S has an edge from T1 to T2, now since S is a non-serial schedule the first instruction of T2(read(Q)) should be executed before last instruction of T1(write(Q)) and since read and write are conflicting operations, the precedence graph for S also contains an edge from T2 to T1,  <br/>So we will have a cycle in the precedence graph which implies that any non serial schedule with T1 as the earliest transaction will not be conflict serializable. <br/>\r\n In a similar way we can show that if T2 is the earliest transaction then also the schedule is not conflict serializable. ',NULL,'2016-09-12 13:24:41','0000-00-00 00:00:00',NULL,NULL),(50,1,'<h4>Common Data Questions: 48 & 49</h4>\r\n\r\nConsider the following relations A, B and C:  <br/>\r\n<img src\"http://mydailyprep.com/question-images/2012-48.png\" /><br/>\r\nHow many tuples does the result of the following SQL query contain? <br/>\r\nSELECT A.Id\r\n FROM A\r\n WHERE A.Age > ALL(SELECT B.Age\r\n FROM B\r\n WHERE B.Name = ‘Arun’)  <br/>','[\"4\",\"3\",\"0\",\"1\"]  ','1','As the result of subquery is an empty table, ‘>ALL’ comparison is true . Therefore, all the\r\nthree row id’s of A will be selected from table A. ',NULL,'2016-09-12 13:30:32','0000-00-00 00:00:00',NULL,NULL),(51,1,'<h4>Common Data Questions: 50 & 51 </h4>\r\nConsider the following C code segment: <br/>\r\nint a, b, c = 0; <br/>\r\nvoid prtFun(void);  <br/>\r\nmain( ) <br/>\r\n{ static int a = 1; /* Line 1 */ <br/>\r\n prtFun( ); <br/>\r\n a + = 1; <br/>\r\n prtFun( ) <br/>\r\n printf(“\\n %d %d “, a, b); <br/>\r\n} <br/>\r\nvoid prtFun(void) <br/>\r\n{ static int a=2; /* Line 2 */ <br/>\r\n int b=1; <br/>\r\n a+=++b; <br/>\r\n printf(“\\n %d %d “, a, b); <br/>\r\n}  <br/>\r\n\r\nWhat output will be generated by the given code segment if:\r\n Line 1 is replaced by auto int a = 1;\r\n Line 2 is replaced by register int a = 2; \r\n','[\"3  &nbsp; &nbsp;1 <br/>\r\n4 &nbsp; &nbsp;1 <br/>\r\n4 &nbsp; &nbsp;2\r\n \",\"4  &nbsp; &nbsp;2 <br/>\r\n6 &nbsp; &nbsp;1 <br/>\r\n6 &nbsp; &nbsp;1\",\r\n\"4  &nbsp; &nbsp;2 <br/>\r\n6&nbsp; &nbsp;2 <br/>\r\n2 &nbsp; &nbsp;0\",\"\r\n4 &nbsp; &nbsp;2 <br/>\r\n4 &nbsp; &nbsp;2 <br/>\r\n2 &nbsp; &nbsp;0\"]  ','3','Static local variables: Scope is limited to function/block but life time is entire program.<br/>\r\n Automatic local variables:\r\n Storage allocated on function entry and automatically deleted or freed when the function is\r\nexited.<br/>\r\n Register variables: Same as automatic variables except that the register variables will not\r\nhave addresses Hence may not take the address of a register variable. ',NULL,'2016-09-12 13:39:25','0000-00-00 00:00:00',NULL,NULL),(52,1,'What output will be generated by the given code segment? ','[\"3  &nbsp; &nbsp;1 <br/>\r\n4 &nbsp; &nbsp;1 <br/>\r\n4 &nbsp; &nbsp;2\r\n \",\"4  &nbsp; &nbsp;2 <br/>\r\n6 &nbsp; &nbsp;1 <br/>\r\n6 &nbsp; &nbsp;1\",\r\n\"4  &nbsp; &nbsp;2 <br/>\r\n6&nbsp; &nbsp;2 <br/>\r\n2 &nbsp; &nbsp;0\",\"\r\n3 &nbsp; &nbsp;1 <br/>\r\n5 &nbsp; &nbsp;2 <br/>\r\n5 &nbsp; &nbsp;2\"]  ','2','',NULL,'2016-09-12 13:43:01','0000-00-00 00:00:00',NULL,NULL),(53,1,'<h4>Statement for Linked Answer Questions: 52 & 53 </h4>\r\nA computer has a 256 KByte, 4-way set associative, write back data cache with block size of\r\n32 Bytes. The processor sends 32 bit addresses to the cache controller. Each cache tag\r\ndirectory entry contains, in addition to address tag, 2 valid bits, 1 modified bit and 1\r\nreplacement bit. <br/>\r\n\r\nThe number of bits in the tag field of an address is:','[\"11\",\"14\",\"16\",\"27\"]  ','2','',NULL,'2016-09-12 13:45:20','0000-00-00 00:00:00',NULL,NULL),(54,1,'The size of the cache tag directory is ','[\"160 Kbits \",\"136 Kbits \",\"40 Kbits\",\"32 Kbits\"]','0','TAG controller maintains 16 + 4 = 20 bits for every block\r\n Hence, size of cache tag directory = 20 X 2<sup>13 </sup> bits =160 K bits            \r\n        ',NULL,'2016-09-12 13:47:03','0000-00-00 00:00:00',NULL,NULL),(55,1,'<h4>Statement for Linked Answer Questions: 54 & 55  </h4>\r\nFor the grammar below, a partial LL(1) parsing table is also presented along with the\r\ngrammar. Entries that need to be filled are indicated as E1, E2, and E3. ? is the empty\r\nstring, $ indicates end of input, and | separates alternate right hand sides of productions.\r\nS  ? aAbB |bAaB| ? <br/>\r\nA  ? S <br/>\r\nB ? S <br/>\r\n<img src=\"http://mydailyprep.com/question-images/2012-54.png\" /><br/>\r\n\r\nThe First and Follow sets for the non-terminals A and B are :\r\n\r\n(A) FIRST(A) = {a, b, ? } = FIRST (B)\r\n FOLLOW(A) = {a, b}\r\n FOLLOW(B) = {a, b, $} <br/>\r\n (B) FIRST(A) = {a, b, $}\r\n FIRST(B) = {a, b, ? }\r\n FOLLOW(A) = {a, b}\r\n FOLLOW(B) ={$} <br/>\r\n (C) FIRST(A) = {a, b, ? } = FIRST(B)\r\n FIRST(A) = {a, b}\r\n FOLLOW(B) = ? <br/>\r\n (D) FIRST(A) = {a, b,} = FIRST(B)\r\n FIRST(A) = {a, b}\r\n FOLLOW(B) ={a, b} ','[\"A\",\"B\",\"C\",\"D\"]  ','0','',NULL,'2016-09-12 13:54:00','0000-00-00 00:00:00',NULL,NULL),(56,1,'<h4>Statement for Linked Answer Questions: 54 & 55  </h4>\r\nFor the grammar below, a partial LL(1) parsing table is also presented along with the\r\ngrammar. Entries that need to be filled are indicated as E1, E2, and E3. ? is the empty\r\nstring, $ indicates end of input, and | separates alternate right hand sides of productions.\r\nS  ? aAbB |bAaB| ? <br/>\r\nA  ? S <br/>\r\nB ? S <br/>\r\n<img src=\"http://mydailyprep.com/question-images/2012-54.png\" /><br/>\r\n\r\nThe First and Follow sets for the non-terminals A and B are :\r\n\r\n(A) FIRST(A) = {a, b, ? } = FIRST (B)\r\n FOLLOW(A) = {a, b}\r\n FOLLOW(B) = {a, b, $} <br/>\r\n (B) FIRST(A) = {a, b, $}\r\n FIRST(B) = {a, b, ? }\r\n FOLLOW(A) = {a, b}\r\n FOLLOW(B) ={$} <br/>\r\n (C) FIRST(A) = {a, b, ? } = FIRST(B)\r\n FIRST(A) = {a, b}\r\n FOLLOW(B) = ? <br/>\r\n (D) FIRST(A) = {a, b,} = FIRST(B)\r\n FIRST(A) = {a, b}\r\n FOLLOW(B) ={a, b} ','[\"A\",\"B\",\"C\",\"D\"]  ','0','',NULL,'2016-09-12 13:56:00','0000-00-00 00:00:00',NULL,NULL),(57,1,'The cost function for a product in a firm is given by 5q<sup>2</sup> , where q is the amount of production. The firm can sell the product at a market price of Rs.50 per unit. The number of units to be produced by the firm such that the profit is maximized is','[\"5\",\"10\",\"15\",\"25\"]','0','             \r\n        ',NULL,'2016-09-12 13:57:37','0000-00-00 00:00:00',NULL,NULL),(58,1,'Choose the most appropriate alternative from the options given below to complete the\r\nfollowing sentence: <br/>\r\nSuresh’s dog is the one ________ was hurt in the stampede. ','[\"that\",\"which\",\"who\",\"whom\"]','0','            \r\n        ',NULL,'2016-09-12 13:58:30','0000-00-00 00:00:00',NULL,NULL),(59,1,'Choose the grammatically INCORRECT sentence: <br/>\r\n(A) They gave us the money back less the service charges of Three Hundred rupees. <br/>\r\n (B) This country\'s expenditure is not less than that of Bangladesh. <br/>\r\n (C) The committee initially asked for a funding of Fifty Lakh rupees, but later settled for a\r\nlesser sum. <br/>\r\n (D) This country\'s expenditure on educational reforms is very less ','[\"A\",\"B\",\"C\",\"D\"]  ','3','',NULL,'2016-09-12 13:59:27','0000-00-00 00:00:00',NULL,NULL),(60,1,'Which one of the following options is the closest in meaning to the word given below?<br/>\r\n<b>Mitigate </b>','[\"Diminish\",\"Divulge\",\"Dedicate \",\"Denote\"]  ','0','',NULL,'2016-09-12 14:00:35','0000-00-00 00:00:00',NULL,NULL),(61,1,'Choose the most appropriate alternative from the options given below to complete the\r\nfollowing sentence:  <br/>\r\n<b> Despite several __________ the mission succeeded in its attempt to resolve the conflict.</b>\r\n','[\"attempts \",\"setbacks \",\"meetings \",\"delegations \"]  ','1','',NULL,'2016-09-12 14:02:48','0000-00-00 00:00:00',NULL,NULL),(62,1,'Wanted Temporary, Part-time persons for the post of Field Interviewer to conduct personal\r\ninterviews to collect and collate economic data. Requirements: High School-pass, must be\r\navailable for Day, Evening and Saturday work. Transportation paid, expenses reimbursed.\r\nWhich one of the following is the best inference from the above advertisement? <br/>\r\n(A) Gender-discriminatory <br/> (B) Xenophobic<br/>\r\n (C) Not designed to make the post attractive<br/> (D) Not gender-discriminatory ','[\"A \",B\",\"C \",\"D\"]  ','2','',NULL,'2016-09-12 14:03:47','0000-00-00 00:00:00',NULL,NULL),(63,1,'Given the sequence of terms, AD CG FK JP, the next term is','[\"OV \",OW \",\"PV \",\"PW\"]  ','0','',NULL,'2016-09-12 14:04:38','0000-00-00 00:00:00',NULL,NULL),(64,1,'An automobile plant contracted to buy shock absorbers from two suppliers X and Y. X supplies\r\n60% and Y supplies 40% of the shock absorbers. All shock absorbers are subjected to a quality test.\r\nThe ones that pass the quality test are considered reliable Of X\'s shock absorbers, 96% are reliable.\r\nOf Y\'s shock absorbers, 72% are reliable. <br/>\r\n The probability that a randomly chosen shock absorber, which is found to be reliable, is made by Y is  ','[\"0.288  \",\"0.334 \",\" 0.667 \",\"0.720 \"]  ','1','            \r\n        ',NULL,'2016-09-12 14:07:26','2016-09-12 14:10:42',NULL,NULL),(65,1,'A political party orders an arch for the entrance to the ground in which the annual convention is being held. The profile of the arch follows the equation y=2x - 0.1x<sup>2</sup> where y is the height of\r\nthe arch in meters. \r\n<br/>\r\nThe maximum possible height of the arch is ','[\"8 meters \",\"10 meters  \",\" 12 meters  \",\"14 meters \"]  ','1','',NULL,'2016-09-12 14:09:59','0000-00-00 00:00:00',NULL,NULL);
/*!40000 ALTER TABLE `test_info` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER test_info_update_entry BEFORE UPDATE ON test_info FOR EACH ROW SET NEW.modify_date = CURRENT_TIMESTAMP */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `test_types`
--

DROP TABLE IF EXISTS `test_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` varchar(100) DEFAULT NULL,
  `modified_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test_types`
--

LOCK TABLES `test_types` WRITE;
/*!40000 ALTER TABLE `test_types` DISABLE KEYS */;
INSERT INTO `test_types` VALUES (2,'Mock Test','2016-09-08 10:25:40','0000-00-00 00:00:00',NULL,NULL),(3,'Practice Test','2016-09-08 10:25:40','0000-00-00 00:00:00',NULL,NULL),(1,'previous','2016-09-08 10:25:40','2016-09-08 13:06:59',NULL,NULL);
/*!40000 ALTER TABLE `test_types` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER test_types_update_entry BEFORE UPDATE ON test_types FOR EACH ROW SET NEW.modify_date = CURRENT_TIMESTAMP */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `tests`
--

DROP TABLE IF EXISTS `tests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `total_count` int(11) DEFAULT NULL,
  `type_id` int(11) NOT NULL,
  `exam_level` varchar(100) DEFAULT NULL,
  `exam_level_id` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` varchar(100) DEFAULT NULL,
  `modified_by` varchar(100) DEFAULT NULL,
  `instructions` text,
  PRIMARY KEY (`name`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tests`
--

LOCK TABLES `tests` WRITE;
/*!40000 ALTER TABLE `tests` DISABLE KEYS */;
INSERT INTO `tests` VALUES (1,'GATE CSE-2012 Paper',60,1,'exam',1,'2016-09-08 10:50:45','2016-09-12 14:10:54',NULL,NULL,'1. asdjfhadjsf asdkfj lasdfj lasdfjlas dfkhfjaskhf dsjf asdjhf lasdfaf asdfkjhas dlflasdjf hladskjfhladfjshladfjs h.<br>1. asdjfhadjsf asdkfj lasdfj lasdfjlas dfkhfjaskhf dsjf asdjhf lasdfaf asdfkjhas dlflasdjf hladskjfhladfjshladfjs h.<br>1. asdjfhadjsf asdkfj lasdfj lasdfjlas dfkhfjaskhf dsjf asdjhf lasdfaf asdfkjhas dlflasdjf hladskjfhladfjshladfjs h.<br>1. asdjfhadjsf asdkfj lasdfj lasdfjlas dfkhfjaskhf dsjf asdjhf lasdfaf asdfkjhas dlflasdjf hladskjfhladfjshladfjs h.<br>1. asdjfhadjsf asdkfj lasdfj lasdfjlas dfkhfjaskhf dsjf asdjhf lasdfaf asdfkjhas dlflasdjf hladskjfhladfjshladfjs h.<br>1. asdjfhadjsf asdkfj lasdfj lasdfjlas dfkhfjaskhf dsjf asdjhf lasdfaf asdfkjhas dlflasdjf hladskjfhladfjshladfjs h.'),(2,'GATE CSE-2013 Paper',60,1,'exam',1,'2016-09-08 10:50:55','2016-09-12 14:10:54',NULL,NULL,'1. asdjfhadjsf asdkfj lasdfj lasdfjlas dfkhfjaskhf dsjf asdjhf lasdfaf asdfkjhas dlflasdjf hladskjfhladfjshladfjs h.<br>1. asdjfhadjsf asdkfj lasdfj lasdfjlas dfkhfjaskhf dsjf asdjhf lasdfaf asdfkjhas dlflasdjf hladskjfhladfjshladfjs h.<br>1. asdjfhadjsf asdkfj lasdfj lasdfjlas dfkhfjaskhf dsjf asdjhf lasdfaf asdfkjhas dlflasdjf hladskjfhladfjshladfjs h.<br>1. asdjfhadjsf asdkfj lasdfj lasdfjlas dfkhfjaskhf dsjf asdjhf lasdfaf asdfkjhas dlflasdjf hladskjfhladfjshladfjs h.<br>1. asdjfhadjsf asdkfj lasdfj lasdfjlas dfkhfjaskhf dsjf asdjhf lasdfaf asdfkjhas dlflasdjf hladskjfhladfjshladfjs h.<br>1. asdjfhadjsf asdkfj lasdfj lasdfjlas dfkhfjaskhf dsjf asdjhf lasdfaf asdfkjhas dlflasdjf hladskjfhladfjshladfjs h.');
/*!40000 ALTER TABLE `tests` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER test_update_entry BEFORE UPDATE ON tests FOR EACH ROW SET NEW.modify_date = CURRENT_TIMESTAMP */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `topic_master`
--

DROP TABLE IF EXISTS `topic_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `topic_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `abbreviation` varchar(100) DEFAULT NULL,
  `chapter_id` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` varchar(100) DEFAULT NULL,
  `modified_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `id` (`id`),
  KEY `chap_id` (`chapter_id`),
  CONSTRAINT `chap_id` FOREIGN KEY (`chapter_id`) REFERENCES `chapter_master` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `topic_master`
--

LOCK TABLES `topic_master` WRITE;
/*!40000 ALTER TABLE `topic_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `topic_master` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER topic_master_update_entry BEFORE UPDATE ON topic_master FOR EACH ROW SET NEW.modify_date = CURRENT_TIMESTAMP */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(10) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `salt` varchar(250) DEFAULT NULL,
  `saltid` varchar(250) DEFAULT NULL,
  `start_time` varchar(250) DEFAULT NULL,
  `reg_source` varchar(100) DEFAULT NULL,
  `reg_platform` varchar(100) DEFAULT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` varchar(100) DEFAULT NULL,
  `modified_by` varchar(100) DEFAULT NULL,
  `exam_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`email`),
  UNIQUE KEY `user_id` (`id`),
  UNIQUE KEY `phone` (`phone`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (3,'dinesh','dinesh.dogra@numetriclabz.com','1232232323','efe6398127928f1b2e9ef3207fb82663','0','514779957d3f3964d9925.13094712','a532eb1cb2014a192dd73d39fcb5ffed','1473508246',NULL,NULL,'2016-09-10 11:50:46','2016-09-12 11:40:37',NULL,NULL,1),(1,'Udit','udit@numetriclabz.com','1231231230','d8578edf8458ce06fbc5bb76a58c5ca4','1','250219857cff96a7e4b92.44728558','4b049aee7740349a3b245c4c13fdc7c3','1473247594',NULL,NULL,'2016-09-07 11:26:34','2016-09-12 11:40:37',NULL,NULL,1),(2,'pankaj','vikrant.chauhan@numetriclabz.com','1231231231','d8578edf8458ce06fbc5bb76a58c5ca4','1','384543957d24930017488.46148080','bc8ed85d07ce3f3af80106c23f9949d3','1473399088',NULL,NULL,'2016-09-09 05:31:28','2016-09-12 11:40:37',NULL,NULL,1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER user_update_entry BEFORE UPDATE ON user FOR EACH ROW SET NEW.modify_date = CURRENT_TIMESTAMP */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `user_test_details`
--

DROP TABLE IF EXISTS `user_test_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_test_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `utr_id` int(11) NOT NULL,
  `state` varchar(255) DEFAULT NULL,
  `time_taken` int(11) DEFAULT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` varchar(100) DEFAULT NULL,
  `modified_by` varchar(100) DEFAULT NULL,
  `question_id` int(11) DEFAULT NULL,
  `answer_id` int(11) DEFAULT NULL,
  `is_Correct` tinyint(1) DEFAULT NULL,
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `utr_ques` (`utr_id`,`question_id`),
  CONSTRAINT `utr_utd_id` FOREIGN KEY (`utr_id`) REFERENCES `user_test_report` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_test_details`
--

LOCK TABLES `user_test_details` WRITE;
/*!40000 ALTER TABLE `user_test_details` DISABLE KEYS */;
INSERT INTO `user_test_details` VALUES (1,1,NULL,NULL,'2016-09-12 10:05:26','2016-09-12 10:06:44',NULL,NULL,1,1,1),(4,1,NULL,NULL,'2016-09-12 10:07:11','0000-00-00 00:00:00',NULL,NULL,2,2,0),(5,1,NULL,NULL,'2016-09-12 10:07:22','0000-00-00 00:00:00',NULL,NULL,4,1,1),(6,1,NULL,NULL,'2016-09-12 14:18:16','2016-09-12 14:18:17',NULL,NULL,13,3,0),(8,1,NULL,NULL,'2016-09-12 14:18:21','0000-00-00 00:00:00',NULL,NULL,16,2,1);
/*!40000 ALTER TABLE `user_test_details` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER utd_update_entry BEFORE UPDATE ON user_test_details FOR EACH ROW SET NEW.modify_date = CURRENT_TIMESTAMP */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `user_test_report`
--

DROP TABLE IF EXISTS `user_test_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_test_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `test_id` int(11) NOT NULL,
  `test_status` varchar(100) DEFAULT NULL,
  `ques_attempted` int(11) DEFAULT NULL,
  `time_taken` int(11) DEFAULT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` varchar(100) DEFAULT NULL,
  `modified_by` varchar(100) DEFAULT NULL,
  `total_count` int(11) DEFAULT NULL,
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `composite_test_user` (`user_id`,`test_id`),
  KEY `test_utr_id` (`test_id`),
  CONSTRAINT `test_utr_id` FOREIGN KEY (`test_id`) REFERENCES `tests` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_utr_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=407 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_test_report`
--

LOCK TABLES `user_test_report` WRITE;
/*!40000 ALTER TABLE `user_test_report` DISABLE KEYS */;
INSERT INTO `user_test_report` VALUES (1,1,1,'resumed',10,0,'2016-09-12 09:01:01','2016-09-13 04:37:01',NULL,NULL,NULL),(348,1,2,'resumed',0,0,'2016-09-12 14:15:23','2016-09-12 14:15:26',NULL,NULL,NULL);
/*!40000 ALTER TABLE `user_test_report` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER utr_update_entry BEFORE UPDATE ON user_test_report FOR EACH ROW SET NEW.modify_date = CURRENT_TIMESTAMP */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-09-13 10:22:45
