<div class="row placeholders" ng-class="{'noactive' : check}">                        
                
    <div class="instruction-left-sidebar">
        <div class="question-year1">
	                    
	        <h4 class="dp-inst">Instructions</h4>
	        <p ng-bind-html="info['instructions']"></p>
            	
    	</div>

	    <div class="instruction-btn-block">
	                		
	        <div class="inner-instruction-btn-block">

	            <div class="previous-btn-box">
	                
	                <a href="/previous-years/cse">
	                	&larr; &nbsp;go back to my test
	                </a>
	            </div>

		        <div class="start-test-btn">
		                			
		            <a ng-href="/previous-years/cse/{{info['test_id']}}#/test"> 
		                
		                <button class="dp-start-test" ng-click="search(info['test_id'], info['user_id'], 0); totalQ(	info['test_id'])"id="start_test"> Start Test</button>
		            </a>
		                		
		        </div>
	   		
	   		</div>
	                	
	    </div>
                
    </div>

    <div class="instruction-right-sidebar">
    	<div class="user-details">
    		<div class="user-img">
    			<img id="userImage" src="/images/profile.png" alt="user-profile">
    		</div>
    		<div class="user-name">
    			<span id="userName"></span>
    		</div>

    	</div>
    </div>

</div>
